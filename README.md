# dr-grep-nodejs

## Prerequisites

1. Install nvm to manage multiple node version: https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04
2. nvm install v6.3.0
3. Install any of your favourite editor (Sublime Text 3 with typescript plugin)
4. Install imagemagick. command for centos
  sudo yum install -y gcc php-devel php-pear
  sudo yum install -y ImageMagick ImageMagick-devel
5. Install mysql
   sudo yum install mysql-server
   sudo /sbin/service mysqld start
   sudo /usr/bin/mysql_secure_installation

## Import database

1. login into database
  mysql -u [username] -p
2. create database
  CREATE DATABASE pin_drgrep_db;
3. exit
4. import the database using the following cmmand
  mysql -u [username] -p pin_drgrep_db < db/schema.sql

## To install typescript package

1. In Sublime editor open the Command Pallete (ctrl+shift+P or cmd+shift+P).
2. Type “Install Package” and hit return.
3. Type “TypeScript” and hit return.

## Build

1. npm install
2. npm run build

## Run the server

npm start

## Debug the application

node --inspect  bin/www

## API Documentation

http://localhost:8080/api
