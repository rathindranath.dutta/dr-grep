-- MySQL dump 10.13  Distrib 5.5.51, for Linux (x86_64)
--
-- Host: drgrep.c5tmebcr3bkt.ap-south-1.rds.amazonaws.com    Database: drgrep
-- ------------------------------------------------------
-- Server version	5.5.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `user_id` int(100) NOT NULL,
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `brand_desc` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `guid` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brand_prod_entry`
--

DROP TABLE IF EXISTS `brand_prod_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand_prod_entry` (
  `prod_id` int(100) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `cat_name` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `up_cat` varchar(25) COLLATE latin1_general_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_first_name` varchar(50) NOT NULL,
  `contact_last_name` varchar(50) NOT NULL,
  `contact_phone_number` varchar(15) NOT NULL,
  `contact_address` varchar(50) NOT NULL,
  `contact_city` varchar(50) NOT NULL,
  `contact_zip_cd` varchar(15) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `contact_state` varchar(10) NOT NULL,
  `is_contact_from_shop` varchar(2) NOT NULL,
  `is_contact_from_website` varchar(2) NOT NULL,
  `is_contact_from_drgrep` varchar(2) NOT NULL,
  `shop_id` int(10) NOT NULL,
  `contact_add_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `contact_email_address` varchar(50) NOT NULL,
  UNIQUE KEY `cntact` (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_title` varchar(50) DEFAULT NULL,
  `discount_perc_off_ind` varchar(2) DEFAULT NULL,
  `discount_perc_off_amt` varchar(10) DEFAULT NULL,
  `discount_money_off_ind` int(2) DEFAULT NULL,
  `discount_money_off_amt` varchar(10) DEFAULT NULL,
  `discount_promo_code_ind` varchar(2) DEFAULT NULL,
  `discount_promo_code` varchar(20) DEFAULT NULL,
  `discount_is_active` varchar(2) DEFAULT NULL,
  `discount_add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `discount_lat` varchar(255) DEFAULT NULL,
  `discount_long` varchar(255) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  KEY `discount_id` (`discount_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount_img`
--

DROP TABLE IF EXISTS `discount_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_img` (
  `discount_id` int(11) NOT NULL,
  `discount_img_link` varchar(255) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount_prod_entries`
--

DROP TABLE IF EXISTS `discount_prod_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_prod_entries` (
  `discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount_prod_entries_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount_prod_entry`
--

DROP TABLE IF EXISTS `discount_prod_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount_prod_entry` (
  `prod_id` int(100) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prod_addr`
--

DROP TABLE IF EXISTS `prod_addr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_addr` (
  `prod_addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_addr_lat` varchar(77) COLLATE latin1_general_ci NOT NULL,
  `prod_addr_long` int(77) NOT NULL,
  `prod_addr_line1` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `prod_addr_pin` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `prod_addr_city` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `prod_addr_state` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `prod_addr_country` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`prod_addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prod_img`
--

DROP TABLE IF EXISTS `prod_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_img` (
  `prod_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `prod_img_link` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`prod_img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=934 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prod_variation_entries`
--

DROP TABLE IF EXISTS `prod_variation_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_variation_entries` (
  `prod_id` int(15) DEFAULT NULL,
  `variation_id` int(10) NOT NULL AUTO_INCREMENT,
  `variation_name` varchar(20) DEFAULT NULL,
  `variation_quantity` int(5) DEFAULT NULL,
  `variation_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  KEY `variation_id` (`variation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prod_wish_list`
--

DROP TABLE IF EXISTS `prod_wish_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_wish_list` (
  `wish_list_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wish_list_name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `prod_id` int(15) NOT NULL,
  `is_prod_in_wishlist` varchar(2) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(3000) COLLATE latin1_general_ci NOT NULL,
  `price` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `category` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `upc` int(20) NOT NULL,
  `guid` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `is_new` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `longitude` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `update_time` datetime NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=570 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `shop_id` int(50) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `shop_name` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_desc` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shop_contact_email` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_phone_no` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_online_weblink` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_active_ind` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`shop_id`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_discount_entry`
--

DROP TABLE IF EXISTS `shop_discount_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_discount_entry` (
  `discount_id` int(11) NOT NULL,
  `shop_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_prod_entries`
--

DROP TABLE IF EXISTS `shop_prod_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_prod_entries` (
  `shop_id` int(50) DEFAULT NULL,
  `prod_id` int(50) DEFAULT NULL,
  `is_prod_in_store` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_shipping_entries`
--

DROP TABLE IF EXISTS `shop_shipping_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_shipping_entries` (
  `shop_id` int(10) DEFAULT NULL,
  `shop_shipping_id` int(10) NOT NULL AUTO_INCREMENT,
  `shop_shipping_name` varchar(40) DEFAULT NULL,
  `shop_shipping_start_time` time DEFAULT NULL,
  `shop_shipping_end_time` time DEFAULT NULL,
  `shop_shipping_flat_rate_ind` int(2) DEFAULT NULL,
  `shop_shipping_flat_rate_amt` varchar(5) DEFAULT NULL,
  `shop_shipping_is_active` varchar(2) DEFAULT NULL,
  KEY `shop_shipping_id` (`shop_shipping_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shop_tax_info`
--

DROP TABLE IF EXISTS `shop_tax_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_tax_info` (
  `shop_id` int(11) DEFAULT NULL,
  `tax_amt_in_prcnt` varchar(5) DEFAULT NULL,
  `tax_is_active_ind` varchar(2) DEFAULT NULL,
  `tax_include_all_price` varchar(2) DEFAULT NULL,
  `shop_tax_add_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(100) NOT NULL AUTO_INCREMENT COMMENT 'primary',
  `user_ext_id` varchar(30) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email_id` varchar(100) NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_sex` varchar(10) NOT NULL,
  `user_is_seller` varchar(5) NOT NULL,
  `user_login_source` varchar(20) NOT NULL,
  `user_img_link` varchar(255) DEFAULT NULL COMMENT 'user''s profile img',
  `user_add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_update_time` timestamp NULL DEFAULT NULL,
  `user_is_active` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_addr`
--

DROP TABLE IF EXISTS `user_addr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_addr` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_addr_lat` varchar(77) COLLATE latin1_general_ci NOT NULL,
  `user_addr_long` varchar(77) COLLATE latin1_general_ci NOT NULL,
  `system_gen` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `user_addr_line1` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `user_addr_pin` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `user_addr_city` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `user_addr_state` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `user_addr_country` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `user_addr_add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_addr_update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_discount_entries`
--

DROP TABLE IF EXISTS `user_discount_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_discount_entries` (
  `seller_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `shop_id` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_msg_node`
--

DROP TABLE IF EXISTS `user_msg_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_msg_node` (
  `parent_node` int(11) NOT NULL,
  `user_node` int(11) NOT NULL,
  `user_from_id` int(11) NOT NULL,
  `user_to_id` int(11) NOT NULL,
  PRIMARY KEY (`parent_node`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_shop_entries`
--

DROP TABLE IF EXISTS `user_shop_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shop_entries` (
  `seller_id` int(100) DEFAULT NULL,
  `shop_id` int(50) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`shop_id`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_social_info`
--

DROP TABLE IF EXISTS `user_social_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_social_info` (
  `user_id` int(11) NOT NULL,
  `user_facebook_id` int(11) NOT NULL,
  `user_facebook_name` varchar(40) NOT NULL,
  `user_facebook_pg1` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-22 16:22:52


CREATE TABLE IF NOT EXISTS `token` (
  `token_id` varchar(250) NOT NULL,
  `refresh_token` varchar(250) NOT NULL,
  `expires_on` timestamp NOT NULL,
  `user_id` int(50) NOT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
