#!/bin/sh

yum -y install git
yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_6.x | bash -
yum -y install nodejs
npm install -g grunt

curl -s https://packagecloud.io/install/repositories/imeyer/runit/script.rpm.sh | bash
yum -y install runit

#printf "NETWORKING=yes\n" >> /etc/sysconfig/network
#/sbin/service mysqld start


yum install -y gcc php-devel php-pear
yum install -y ImageMagick ImageMagick-devel
