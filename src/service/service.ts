/// <reference path="../_all.d.ts" />
"use strict";


/**
 * Service class
 */
export abstract class Service {

  /**
   * Constructor.
   *
   * @class Service
   * @constructor
   */
  constructor() {
    // Initialize
  }
}
