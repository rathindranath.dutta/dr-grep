/// <reference path="../_all.d.ts" />
"use strict";

const nodemailer = require("nodemailer");
var conf = require("../config/config.js");

/**
 * Service class
 */
export class EmailService {

  public send(to: string, body: string, sub: string): boolean {
    let transporter = nodemailer.createTransport({
        service: "Gmail",
        auth: {
            type: "OAuth2",
            user: conf.get("smtp.auth.user"),
            clientId: "1063147513998-76o538kg4fcksp7fq7q2j8fomd2l266a.apps.googleusercontent.com",
            clientSecret: "7MyuYsg32SwTLP_OSKjB516L",
            refreshToken: conf.get("smtp.auth.refreshToken")
        }
    });
    transporter.sendMail({
        from: conf.get("smtp.from"),
        to: to,
        subject: sub,
        text: body
    });
    return true;
  }
}