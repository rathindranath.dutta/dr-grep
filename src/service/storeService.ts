/// <reference path= "../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import storeModule = require("../models/store");
import storeDaoModule = require("../dao/storeDao");

export class StoreService extends serviceModule.Service {

  private _storeDao: storeDaoModule.StoreDao;

  /**
   * Constructor.
   *
   * @class StoreService
   * @constructor
   */
   constructor(storeDao: storeDaoModule.StoreDao) {
     super();
     this._storeDao = storeDao;
   }

  /**
   * List all stores
   */
  public list(callback: any): void {
  	return this._storeDao.list(callback);
  }

  /**
   * Get a store details
   */
  public get(id: number, callback: any): void {
  	return this._storeDao.get(id, callback);
  }

  /**
   * Create a store
   */
  public create(store: storeModule.Store, callback: any): void {
  	return this._storeDao.create(store, callback);
  }

  /**
   * Update a store
   */
  public update(store: storeModule.Store, callback: any): void {
  	return this._storeDao.update(store, callback);
  }

  /**
   * Delete a store
   */
  public delete(id: number, callback: any): void {
    return this._storeDao.delete(id, callback);
  }

}
