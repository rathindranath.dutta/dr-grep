/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import iqpDaoModule = require("../dao/iqpDao");
import iqpModule = require("../models/iqp");

/**
 * Service class
 */
export class IQPService extends serviceModule.Service {

    private _iqpDao: iqpDaoModule.IqpDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(iqpDao: iqpDaoModule.IqpDao) {
        super();
        this._iqpDao = iqpDao;
    }

    /**
     * Create a brand
     */
    public getIQPDetailsByBrand(brandId: number, callback: any): void {
        return this._iqpDao.getIQPDetailsByBrand(brandId, callback);
    }

    public updateIQPStatus(iqpId: number, brandId: number, indicator: string, callback: any): void {
        return this._iqpDao.updateIQPStatus(iqpId, brandId, indicator, callback);
    }

}