/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import brandDaoModule = require("../dao/brandDao");
import brandModule = require("../models/brand");

/**
 * Service class
 */
export class BrandService extends serviceModule.Service {

  private _brandDao: brandDaoModule.BrandDao;
  /**
   * Constructor.
   *
   * @class Service
   * @constructor
   */
  constructor(brandDao: brandDaoModule.BrandDao) {
    super();
    this._brandDao = brandDao;
  }

  /**
   * Create a brand
   */
  public create(brand: brandModule.Brand, callback: any): void {
    return this._brandDao.create(brand, callback);
  }

  /**
   * Delete a brand
   */
  public delete(id: number, callback: any): void {
    return this._brandDao.delete(id, callback);
  }

  /**
   * Get a brand details
   */
  public get(id: number, callback: any): void {
    return this._brandDao.get(id, callback);
  }
  /**
   * Get a brand details by userId
   */
  public getBrandByUserEmail(userEmail: string, callback: any): void {
    return this._brandDao.getBrandByUserEmail(userEmail, callback);
  }

  /**
   * Get a brand by guid  updateBrand
   */
  public getByGuid(guid: string, callback: any): void {
    return this._brandDao.getByGuid(guid, callback);
  }

  public updateBrand(brand: brandModule.Brand, callback: any): void {
    return this._brandDao.updateBrand(brand, callback);
  }

}