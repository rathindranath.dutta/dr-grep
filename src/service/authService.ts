/// <reference path="../_all.d.ts" />
"use strict";

import httpServiceModule = require("./httpService");
import emailServiceModule = require("./emailService");
import googleResponse = require("../models/googleResponse");
import facebookResponse = require("../models/facebookResponse");
import response = require("../models/response");
import serviceModule = require("./service");
import authDaoModule = require("../dao/authDao");
import tokenModule = require("../models/token");
import userModule = require("../models/user");

/**
 * Service class
 */
export class AuthService extends serviceModule.Service {

  private _httpService: httpServiceModule.HttpService;
  private _authDao: authDaoModule.AuthDao;
  private _emailService: emailServiceModule.EmailService;
  /**;
   * Constructor.
   *
   * @class Service
   * @constructor
   */
  constructor(authDao: authDaoModule.AuthDao) {
    super();
    this._authDao = authDao;
    this._httpService = new httpServiceModule.HttpService();
    this._emailService = new emailServiceModule.EmailService();
  }

  /**
   * Validate token by id_token
   */

  public validateToken(idToken: string, authProvider: string, callback: any): void {
    let apiUrl: string;
    let responseClass: any;
    if (authProvider === "google") {
      apiUrl = "https://www.googleapis.com/oauth2/v3/tokeninfo";
      responseClass = googleResponse.GoogleResponse;
      this._httpService.doPost
        (apiUrl, { id_token: idToken }, responseClass, function (resp: response.Response) {
          callback(resp.isValid());
        });
    } else if (authProvider === "facebook") {
      apiUrl = "https://graph.facebook.com/debug_token?input_token="
        + idToken + "&access_token=359735084358244|db2adc504bd31687ec47f82618d71fd4";
      responseClass = facebookResponse.FacebookResponse;
      this._httpService.doGet
        (apiUrl, {}, responseClass, function (resp: response.Response) {
          callback(resp.isValid());
        });
    }
  }

  /**
   * Find token by id
   */
  public getTokenInfo(tokenId: string, callback: any): void {
    return this._authDao.getTokenInfo(tokenId, callback);

  }

  /**
   * Find token by refresh id
   */

  public refreshToken(refreshId: string, callback: any): void {
    return this._authDao.getTokenInfoByRefreshId(refreshId,
      function (tokenObj: tokenModule.Token, error: any) {
        if (tokenObj == null) {
          return callback(false, null, error);
        }
        let expiresOn: Date = new Date();
        expiresOn.setMinutes(expiresOn.getMinutes() + 20);
        tokenObj.expiresOn = expiresOn;
        this._authDao.updateToken(tokenObj, function (isUpdated: boolean, error: any) {
          callback(isUpdated, tokenObj, error);
        });
      }.bind(this));
  }

  /**
   * Find user by email
   */
  public getUserByEmail(email: string, callback: any): void {
    return this._authDao.getUserByEmail(email, callback);
  }
  /**
   * send email
   */
  public sendEmail(to: string, body: any, sub: string): boolean {
    return this._emailService.send(to, body, sub);
  }

  /**
   * Update a user
   */
  public updateUser(user: userModule.User, callback: any): void {
    return this._authDao.updateUser(user, callback);
  }

  /**
   * Find user by its Id
   */
  public getUserById(id: string, callback: any): void {
    return this._authDao.getUserById(id, callback);
  }

  /**
   * save User
   */
  public saveUser(user: userModule.User, callback: any): void {
    return this._authDao.saveUser(user, callback);
  }

  /**
   * save Token
   */
  public saveToken(token: tokenModule.Token, callback: any): void {
    return this._authDao.saveToken(token, callback);
  }

  /**
   * save Token
   */
  public deleteToken(tokenId: string, callback: any): void {
    return this._authDao.deleteToken(tokenId, callback);
  }

}