/// <reference path="../_all.d.ts" />
"use strict";

import * as request from "request";
import http = require("http");
import * as response from "../models/response";
import querystring = require("querystring");

/**
 * Service class
 */
export class HttpService {

  /**
   * post call
   */

  public doPost(url: string, params: any, responseType: any, callback: any): void {
    request.post(url, {form: params},
    function(err: any, response: http.IncomingMessage, body: any) {
      var resp = JSON.parse(body);
      let returnObj = Object.create(responseType.prototype);
      var customResp = Object.assign(returnObj, resp);
      callback(customResp);
    }.bind(this));
  }

  public doGet(url: string, params: any, responseType: any, callback: any): void {
    let modifiedUrl: string;
    modifiedUrl = this.isEmpty(params) ?  url : url + "?" + querystring.stringify(params);
    request.get(modifiedUrl,
    function(err: any, response: http.IncomingMessage, body: any) {
      var resp = JSON.parse(body);
      let returnObj = Object.create(responseType.prototype);
      var customResp = Object.assign(returnObj, resp);
      callback(customResp);
    }.bind(this));
  }

  private isEmpty(obj: Object) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }
}