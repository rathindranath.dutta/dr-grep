/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import contactDaoModule = require("../dao/contactDao");
import contactModule = require("../models/contact");

/**
 * Service class
 */
export class ContactService extends serviceModule.Service {

    private _contactDao: contactDaoModule.ContactDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(contactDao: contactDaoModule.ContactDao) {
        super();
        this._contactDao = contactDao;
    }

    /**
     * create a contact
     */
    public create(contact: contactModule.Contact, callback: any): void {
        return this._contactDao.create(contact, callback);
    }

    /**
     * update a contact
     */
    public update(contact: contactModule.Contact, callback: any): void {
        return this._contactDao.update(contact, callback);
    }
}