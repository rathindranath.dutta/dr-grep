/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import arapDaoModule = require("../dao/arapDao");
import iqpModule = require("../models/iqp");
/**
 * Service class
 */
export class ARAPService extends serviceModule.Service {

    private _arapDao: arapDaoModule.ARAPDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(arapDao: arapDaoModule.ARAPDao) {
        super();
        this._arapDao = arapDao;
    }

    /**
     * Create a brand
     */
    public createEditInvoice(accountReceivable: any, sentInd: string, callback: any): void {
        return this._arapDao.createEditInvoice(accountReceivable, sentInd, callback);
    }
    //getArapDetailsByBrand
    public getAllArapDetails(brandId: number, type: string, account: string, isDiscountAvailable: string, callback: any): void {
        return this._arapDao.getAllArapDetails(brandId, type, account, isDiscountAvailable, callback);
    }
}