/// <reference path= "../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import prodModule = require("../models/product");
import imageModule = require("../models/image");
import variationModule = require("../models/variation");
import prodDaoModule = require("../dao/productDao");
import imageDaoModule = require("../dao/imageDao");
import typedjson = require("typedjson");

/**
 * Service for Product & Variation
 */
export class ProductService extends serviceModule.Service {

  private _productDao: prodDaoModule.ProductDao;
  private _imageDao: imageDaoModule.ImageDao;

  /**
   * Constructor.
   *
   * @class ProductService
   * @constructor
   */
  constructor(productDao: prodDaoModule.ProductDao, imageDao: imageDaoModule.ImageDao) {
    super();
    this._productDao = productDao;
    this._imageDao = imageDao;
  }

  /**
   * List all products
   */
  public list(page: number, callback: any): void {
    return this._productDao.list(page, callback);
  }
  //getProductsByUserId
  /**
   * List all products
   */
  public getProductsByUserEmail(userEmail: string, callback: any): void {
    return this._productDao.getProductsByUserEmail(userEmail, callback);
  }

  /**
   * Get a product details
   */
  public get(id: number, callback: any): void {
    this._imageDao.get(id, function (images: Array<imageModule.Image>, error: any) {
      if (images.length > 0) {
        this._productDao.get(id, function (product: prodModule.Product, error: any) {
          for (let image of images) {
            product.images.push(image);
          }
          callback(product, error);
        });
      }
    });
  }

  /**
   * Get a product details
   */
  public getProductIdsByGuids(guids: Array<string>, callback: any): void {
    this._productDao.getProductIdsByGuids(guids, callback);
  }

  /**
   * Get a product details
   */
  public getProductIdsByUpcs(upcs: Array<string>, userId: number, callback: any): void {
    this._productDao.getProductIdsByUpcs(upcs, userId, callback);
  }

  /**
   * Get product(s) by brand
   */
  public getProductsByBrand(brandId: number, callback: any): void {
    this._productDao.getProductsByBrand(brandId, callback);
  }

  /**
   * attach Products to a brand
   */
  public attachProductsToBrand(products: any[], callback: any): void {
    this._productDao.attachProducts(products, callback);
  }

  /**
   * Create a product
   */
  public create(product: prodModule.Product, callback: any): void {
    let images: Array<any> = product.images;
    product.images = undefined;
    return this._productDao.create(product, (id: number, error: any) => {
      if (id) {
        for (let i: number = 0; i < images.length; i++) {
          images[i]["product_id"] = id;
          images[i] = typedjson.TypedJSON.parse(JSON.stringify(images[i]), imageModule.Image);
        }
        if (images !== undefined && images !== null && images.length > 0) {
          this.createManyProductImages(images, (data: any, error: any) => {
            callback(id, error);
          });
        } else {
          callback(id, error);
        }
      }
    });
  }

  /**
   * Create many products at a time
   */
  public createMany(products: Array<prodModule.Product>, callback: any): void {
    return this._productDao.createMany(products, callback);
  }

  /**
   * Create many products at a time
   */
  public createManyProductImages(images: Array<imageModule.Image>, callback: any): void {
    return this._imageDao.createManyProductImages(images, callback);
  }

  /**
   * Update a product
   */
  public update(product: prodModule.Product, callback: any): void {
    return this._productDao.update(product, callback);
  }

  /**
   * Delete a product
   */
  public delete(id: number, callback: any): void {
    return this._productDao.delete(id, callback);
  }

  /**
   * Get all variations of a product
   */
  public getVariations(id: number, callback: any): void {
    return this._productDao.getVariations(id, callback);
  }

  /**
   * Get a variation of a product
   */
  public getVariation(id: number, variationId: number, callback: any): void {
    return this._productDao.getVariation(id, variationId, callback);
  }

  /**
   * Add variations
   */
  public addVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void {
    return this._productDao.addVariations(id, variations, callback);
  }

  /**
   * Remove variations
   */
  public removeVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void {
    return this._productDao.removeVariations(id, variations, callback);
  }

  /**
   * Update variations
   */
  public updateVariations(variations: Array<variationModule.Variation>, callback: any): void {
    return this._productDao.updateVariations(variations, callback);
  }

}
