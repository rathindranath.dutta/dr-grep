/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import stockModule = require("../dao/stockDao");

/**
 * Service class
 */
export class StockService extends serviceModule.Service {

    private _stockDao: stockModule.StockDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(stockDao: stockModule.StockDao) {
        super();
        this._stockDao = stockDao;
    }

    /**
     * Create a brand
     */
    public getStockDetails(brandId: number, callback: any): void {
        return this._stockDao.getStockDetails(brandId, callback);
    }
    //getArapDetailsByBrand
    public getInOutStockByProduct(productId: number, callback: any): void {
        return this._stockDao.getInOutStockByProduct(productId, callback);
    }

    /**
     * Add stock for a product
     */
    public addStock(stock: any, isProductAddtoStock: number, callback: any): void {
        return this._stockDao.addStock(stock, isProductAddtoStock, callback);
    }

    /**
     * Update Stock for a product //updateStock
     */
    public updateStock(stock: any, callback: any): void {
        return this._stockDao.updateStock(stock, callback);
    }
}