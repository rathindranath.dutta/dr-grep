/// <reference path="../_all.d.ts" />
"use strict";

import storeServiceModule = require("../service/storeService");
import productServiceModule = require("../service/productService");
import brandServiceModule = require("../service/brandService");
import serviceModule = require("./service");
import authDaoModule = require("../dao/authDao");
import tokenModule = require("../models/token");
import userModule = require("../models/user");
import brandModule = require("../models/brand");
import productModule = require("../models/product");
import imageModule = require("../models/image");
import typedjson = require("typedjson");

/**
 * Service class
 */
export class BulkuploadService extends serviceModule.Service {

  private _storeService: storeServiceModule.StoreService;
  private _productService: productServiceModule.ProductService;
  private _brandService: brandServiceModule.BrandService;

  /**
   * Constructor.
   *
   * @class Service
   * @constructor
   */
  constructor(productService: productServiceModule.ProductService,
    brandService: brandServiceModule.BrandService, storeService: storeServiceModule.StoreService) {
    super();
    this._productService = productService;
    this._brandService = brandService;
    this._storeService = storeService;
  }

  /**
   * Validate token by id_token
   */

  public uploadBrand(brands: any, currentUserId: number, callback: any): void {
    let addedBrands = {};
    const brandName = "brandName";
    const brandDesc = "brandDesc";
    const brandGuid = "guid";
    let numberOfRows = 0;
    brands.forEach((data) => {
      if (data[brandName] === "Brand Name" || addedBrands.hasOwnProperty(data[brandGuid])) {
        return;
      }
      addedBrands[data[brandGuid]] = "";
      if ("" === data[brandGuid]) {
        return callback("");
      }
      this._brandService.getByGuid(data[brandGuid], (brand: brandModule.Brand, error: any) => {
        if (brand == null) {
          let brand: brandModule.Brand = new brandModule.Brand();
          brand.setProperties(null, currentUserId, data[brandName], data[brandDesc],
            data[brandGuid], 1, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", null, "", "", "", "", "", "", "", null, null, "", "", null);
          this._brandService.create(brand, (savedBrandId: number, error: any) => {
            addedBrands[data[brandGuid]] = savedBrandId;
            numberOfRows += 1;
            if (numberOfRows === Object.keys(addedBrands).length) {
              callback(addedBrands);
            }
          });
        } else {
          addedBrands[data[brandGuid]] = brand.brandId;
          numberOfRows += 1;
          if (numberOfRows === Object.keys(addedBrands).length) {
            callback(addedBrands);
          }
        }
      });
    });
  }

  public uploadProducts(products: any, brandguid: string, brandId: number,
    currentUserId: number, callback: any): void {
    let addedBrands = {};
    const title = "title";
    const description = "description";
    const userId = "user_id";
    const brandIdColumn = "brand_id";
    const guid = "guid";
    const upc = "upc";
    let numberOfRows = 0;
    let productModels = new Array<productModule.Product>();
    products.forEach((data) => {
      if (data[title] === "Product Name") {
        return;
      }
      data[userId] = currentUserId;
      data[brandIdColumn] = brandId;
      data[guid] = currentUserId + "-" + data[upc];
      let product = typedjson.TypedJSON.parse(JSON.stringify(data), productModule.Product);
      productModels.push(product);
    });
    this._productService.createMany(productModels, callback);
  }

  public updateProductImages(prodUpcImages: Array<Object>, currentUserId: number, callback: any): void {
    let upcs = new Array<string>();
    const upcSymbol = "upc";
    const urlSymbol = "url";
    const prodUpcSymbol = "productUpc";
    prodUpcImages.forEach((prodUpcImage) => {
      upcs.push("'" + prodUpcImage[prodUpcSymbol] + "'");
    });
    this._productService.getProductIdsByUpcs(upcs, currentUserId, (data: any, error: any) => {
      let productImages = new Array<imageModule.Image>();
      prodUpcImages.forEach((prodUpcImage) => {
        let productGuid: string = currentUserId + "-" + String(prodUpcImage[prodUpcSymbol]);
        let img: imageModule.Image = new imageModule.Image();
        img.setProperties(null, data[productGuid], prodUpcImage[urlSymbol]);
        productImages.push(img);
      });
      this._productService.createManyProductImages(productImages, callback);
    });
  }
}