/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import connectionDaoModule = require("../dao/connectionDao");
import connectionModule = require("../models/connection");
// import iqpModule = require("../models/iqp");

/**
 * Service class
 */
export class ConnectionService extends serviceModule.Service {

    private _connectionDao: connectionDaoModule.ConnectionDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(connectionDao: connectionDaoModule.ConnectionDao) {
        super();
        this._connectionDao = connectionDao;
    }

    /**
     * get brands and users connected with the brand
     */
    public getConnectedEntities(brandId: number, callback: any): void {
        return this._connectionDao.getConnetedEntities(brandId, callback);
    }

    /**
     * create a connection
     */
    public create(connection: connectionModule.Connection, callback: any): void {
        return this._connectionDao.create(connection, callback);
    }

    /**
     * update a connection
     */
    public update(connection: connectionModule.Connection, callback: any): void {
        return this._connectionDao.update(connection, callback);
    }
}