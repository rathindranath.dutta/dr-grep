/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import updateUserDaoModule = require("../dao/updateUserDao");
import userModule = require("../models/user");

/**
 * Service class
 */
export class UpdateUserService extends serviceModule.Service {

  private _updateUserDao: updateUserDaoModule.UpdateUserDao;
  /**
   * Constructor.
   *
   * @class Service
   * @constructor
   */
  constructor(updateUserDao: updateUserDaoModule.UpdateUserDao) {
    super();
    this._updateUserDao = updateUserDao;
  }

  /**
   * Find user by email
   */
  public getUserByEmail(email: string, callback: any): void {
    return this._updateUserDao.getUserByEmail(email, callback);
  }

  /**
   * save User
   */
  public saveUser(user: userModule.User, callback: any): void {
    return this._updateUserDao.saveUser(user, callback);
  }

}