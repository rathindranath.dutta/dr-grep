/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import ReportDaoModule = require("../dao/reportDao");

/**
 * Service class
 */
export class ReportService extends serviceModule.Service {

    private _reportDao: ReportDaoModule.ReportDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(reportDao: ReportDaoModule.ReportDao) {
        super();
        this._reportDao = reportDao;
    }

    /**
     * get brands and users connected with the brand
     */
    public getGstr1Rpt(brandId: number, issueMonth: number, issueYear: number, callback: any): void {
        return this._reportDao.getGstr1Rpt(brandId, issueMonth, issueYear, callback);
    }
}