/// <reference path="../_all.d.ts" />
"use strict";

var winston = require("winston");
var conf = require("../config/config.js");

/**
 * Service class
 */
export class Logger {

  private static _logger: any = new winston.Logger({
    level: conf.get("logging.level"),
    transports: [
      new (winston.transports.Console)({"timestamp" : true}),
      new (winston.transports.File)({ filename: "development.log" })
    ]
  });

  public static getLogger(): any {
    return this._logger;
  }

}