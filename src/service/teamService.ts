/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import teamDaoModule = require("../dao/teamDao");
import teamModule = require("../models/team");

/**
 * Service class
 */
export class TeamService extends serviceModule.Service {

    private _teamDao: teamDaoModule.TeamDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(teamDao: teamDaoModule.TeamDao) {
        super();
        this._teamDao = teamDao;
    }

    /**
     * Create a brand
     */
    public addUserToTeam(team: teamModule.Team, callback: any): void {
        return this._teamDao.addUserToTeam(team, callback);
    }

    public getTeamMembers(brandId: number, callback: any): void {
        return this._teamDao.getTeamMembers(brandId, callback);
    }

    public insertRule(ruleObject: any, callback: any): void {
        return this._teamDao.insertRule(ruleObject, callback);
    }

    public getRuleSets(brandId: number, callback: any): void {
        return this._teamDao.getRuleSets(brandId, callback);
    }
}