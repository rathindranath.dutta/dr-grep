/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import ddDaoModule = require("../dao/dynamicDiscountigDao");
import iqpModule = require("../models/iqp");

/**
 * Service class
 */
export class DynamicDiscountingService extends serviceModule.Service {

    private _ddDao: ddDaoModule.DynamicDiscountngDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(ddDao: ddDaoModule.DynamicDiscountngDao) {
        super();
        this._ddDao = ddDao;
    }

    /**
     * make early payment
     */
    public earlyPayment(dynamicDiscounting: any, callback: any): void {
        return this._ddDao.earlyPayment(dynamicDiscounting, callback);
    }

    public actionOnDiscount(dynamicDiscounting: any, callback: any): void {
        return this._ddDao.actionOnDiscount(dynamicDiscounting, callback);
    }
}