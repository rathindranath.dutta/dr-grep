/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import workflowModule = require("../dao/workflowDao");

/**
 * Service class
 */
export class WorkflowService extends serviceModule.Service {

    private _workflowDao: workflowModule.WorkflowDao;
    /**
     * Constructor.
     *
     * @class Service
     * @constructor
     */
    constructor(workflowDao: workflowModule.WorkflowDao) {
        super();
        this._workflowDao = workflowDao;
    }

    /**
     * Create a brand
     */
    public insertUpdateWorkflowFeedData(action: string, workflowFeed: any, callback: any): void {
        return this._workflowDao.insertUpdateWorkflowFeedData(action, workflowFeed, callback);
    }

    public getToDoList(brandId: number, userId: number, callback: any): void {
        return this._workflowDao.getToDoList(brandId, userId, callback);
    }
}