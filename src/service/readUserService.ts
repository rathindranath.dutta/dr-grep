/// <reference path="../_all.d.ts" />
"use strict";

import serviceModule = require("./service");
import readUserDaoModule = require("../dao/readUserDao");
import userModule = require("../models/user");

/**
 * Service class
 */
export class ReadUserService extends serviceModule.Service {

  private _readUserDao: readUserDaoModule.ReadUserDao;
  /**
   * Constructor.
   *
   * @class ReadUserService
   * @constructor
   */
  constructor(readUserDao: readUserDaoModule.ReadUserDao) {
    super();
    this._readUserDao = readUserDao;
  }

  /**
   * Find user by email
   */
  public getUserByEmail(email: string, callback: any): void {
    return this._readUserDao.getUserByEmail(email, callback);
  }
}