/// <reference path="../_all.d.ts" />
"use strict";

//import iqpModule = require("../models/iqp");
import connectionModule = require("../models/connection");

export abstract class ConnectionDao {

    /**
     * get connected brand/ user
     */
    public abstract getConnetedEntities(brandId: number, callback: any): void;

    /**
    * create a connection
    */
    public abstract create(connection: connectionModule.Connection, callback: any): void;

    /**
    * update a connection
    */
    public abstract update(connection: connectionModule.Connection, callback: any): void;

}
