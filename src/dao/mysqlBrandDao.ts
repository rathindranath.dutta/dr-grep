/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import brandDaoModule = require("./brandDao");
import brandModule = require("../models/brand");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");

export class MySqlBrandDao extends brandDaoModule.BrandDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class BrandDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  /**
   * Create a brand
   */
  public create(brand: brandModule.Brand, callback: any): void {
    this._mysqlDao.createObject<brandModule.Brand>(brand,
      function (id: number, error: any) {
        callback(id, error);
      });
  }

  /**
  * Delete a brand
  */
  public delete(id: number, callback: any): void {
    this._mysqlDao.deleteObject<brandModule.Brand>(new brandModule.Brand(), <string><any>id,
      function (isDeleted: boolean, error: any) {
        callback(isDeleted, error);
      });
  }

  /**
  * Get a brand details
  */

  public get(id: number, callback: any): void {
    let param: any = { brand_id: id };
    this.getBrand("", param, callback);
  }

  public getBrandByUserEmail(userEmail: string, callback: any): void {
    let param: any = { user_email: userEmail };
    let query: string = `select 
    b.user_id as '_userId', 
    b.brand_id as '_brandId', 
    IFNULL(b.brand_name,"") as '_brandName', 
    b.brand_desc as '_brandDesc', 
    b.guid as '_guid', 
    b.is_active as '_isActive', 
    IFNULL(b.brand_about_us,"") as '_brandAboutUs',
    IFNULL(b.brand_localtion,"") as '_brandLocation', 
    IFNULL(b.brand_website,"") as '_brandWebsite', 
    IFNULL(b.brand_registration_number,"") as '_brandRegistrationNumber',
    IFNULL(b.brand_addr_country,"") as '_brandAddrCountry',
    IFNULL(b.brand_addr_city,"") as '_brandAddrCity',
    IFNULL(b.brand_addr_post_code,"") as '_brandAddrPostCode', 
    IFNULL(b.brand_type,"") as '_brandType',
    IFNULL(brand_industry,"") as '_brandIndustry',
    IFNULL(b.brand_contact_phone_number,0) as '_brandContactPhoneNumber',
    IFNULL(b.brand_contact_email_address,"") as '_brandContactEmailAddress',
    IFNULL(b.brand_lat,"") as '_brandLat',
    IFNULL(b.brand_long,"") as '_brandLong',
    IFNULL(b.brand_tax_id,"") as '_brandTaxId',
    IFNULL(b.facebook_profile_link,"") as '_facebookProfileLink',
    IFNULL(b.linkedin_profile_link,"") as '_linkedinProfileLink',
    IFNULL(b.twitter_profile_link,"") as '_twitterProfileLink',
    IFNULL(b.amazon_profile_link,"") as '_amazonProfileLink',
    IFNULL(b.brand_pan_number,"") as '_brandPanNumber',
    IFNULL(b.brand_bank_accnt_number,"") as '_brandBankAccountNumber',
    IFNULL(b.brand_billing_to_fullname,"") as '_brandBillingToFullname',
    IFNULL(b.brand_billing_to_email,"") as '_brandbillingToEmail',
    IFNULL(b.brand_addr_line1,"") as '_brandAddrLine1',
    IFNULL(b.brand_addr_line2,"") as '_brandAddrLine2',
    IFNULL(b.brand_addr_state,"") as '_brandAddrState',
    IFNULL(b.brand_GSTIN,"") as '_brandGSTIN',
    IFNULL(b.brand_state_cd,"") as '_brandStateCd',
    IFNULL(b.brand_shipping_info_as_billing_ind,0) as '_brandShippingInfoAsBillingInd',
    IFNULL(b.brand_shipping_to_fullname,"") as '_brandShippingToFullname',
    IFNULL(b.brand_shipping_to_email,"") as '_brandShippingToEmail',
    IFNULL(b.brand_shipping_address_line1,"") as '_brandShippingAddressLine1',
    IFNULL(b.brand_shipping_address_line2,"") as '_brandShippingAddressLine2',
    IFNULL(b.brand_shipping_city,"") as '_brandShippingCity',
    IFNULL(b.brand_shipping_state,"") as '_brandShippingState',
    IFNULL(b.brand_shipping_zip_cd,"") as '_brandShippingZipCd',
    IFNULL(b.brand_shipping_country,"") as '_brandShippingCountry',
    IFNULL(b.period_in_business,0) as '_periodInBusiness',
    IFNULL(b.area_covered,"") as '_areaCovered',
    IFNULL(b.annual_turn_over,"") as '_annualTurnCover',
    IFNULL(b.NMMC_cess_number,0) as '_nmmcCessNumber',
    IFNULL(b.brand_CIN,0) as '_brandCin',
    IFNULL(b.brand_yoy_revenue,0) as '_brandYoyRevenue',
    IFNULL(b.brand_bank_name,"") as '_brandBankName',
    IFNULL(b.brand_bank_branch_name,"") as '_brandBankBranchName',
    IFNULL(b.brand_bank_accnout_number,0) as '_brandBankAccountNum',
    IFNULL(b.brand_bank_IFSC_code,"") as '_brandBankIfscCode',
    IFNULL(b.brand_drug_lic_number,"") as '_brandDrugLicNum',
    IFNULL(b.brand_FSSAI_number,"") as '_brandFssiNum',
    IFNULL(b.brand_ownnership,"") as '_brandOwnership',
    IFNULL(b.brand_size,"") as '_brandSize',

    p.product_id as '_products:_id', 
    p.user_id as '_products:_userId', 
    p.brand_id as '_products:_brandId', 
    p.title as '_products:_title', 
    p.description as '_products:_desc',
    p.price as '_products:_price', 
    p.category as '_products:_category', 
    p.upc as '_products:_upc', 
    p.guid as '_products:_guid', 
    p.is_new  as '_products:_isNew', 
    p.add_time as'_products:_addTime', 
    p.latitude as '_products:_lat', 
    p.longitude as '_products:_long', 
    p.update_time as '_products:_updateTime',
    p.is_active as '_products:_isActive', 
    i.prod_img_id as '_products:_images:_id', 
    i.product_id as '_products:_images:_productId',
    i.prod_img_link as '_products:_images:_link' 
    from brand b left join product p
    on b.brand_id = p.brand_id left join prod_img i on p.product_id = i.product_id
    inner join user u on u.user_id = b.user_id where u.user_email_id`;
    this.getBrand(query, param, callback);
  }

  private getBrand(query: string, param: any, callback: any): void {
    if (query !== null && query !== "") {
      this._mysqlDao.getComplexObject(new brandModule.Brand(), query, param,
        function (result: any, error: any) {
          if (result.length > 0) {
            let tree = new Treeize();
            tree.grow(result);
            let brand: brandModule.Brand = tree.getData();
            callback(brand, error);
          } else {
            callback(null, error);
          }
        });
    } else {
      this._mysqlDao.getObject(new brandModule.Brand(), param,
        function (result: any, error: any) {
          if (result.length > 0) {
            let brand: brandModule.Brand = typedjson.
              TypedJSON.parse(JSON.stringify(result[0]), brandModule.Brand);
            callback(brand, error);
          } else {
            callback(null, error);
          }
        });
    }
  }

  /**
  * Get a brand by guid
  */
  public getByGuid(guid: string, callback: any): void {
    let param: any = { guid: guid };
    this.getBrand("", param, callback);
  }

  public updateBrand(brandData: brandModule.Brand, callback: any): void {
    let brandObj = this.prepareBrandObj(brandData);
    let isModified: boolean = false;
    let tableInfo: any = {
      "_tableName": "brand",
      "brand_id": brandData["_brandId"]
    };
    this._mysqlDao.updateMultiplePropObj(tableInfo, brandObj[0], (id: number, error: any) => {
      isModified = true;
      callback(null, isModified);
    });
  }

  private prepareBrandObj(brand: brandModule.Brand): brandModule.Brand[] {
    let brandObj: any[] = [{
      "brand_name": brand["_brandName"],
      "brand_desc": brand["_brandDesc"],
      "guid": brand["_guid"],
      "is_active": brand["_isActive"],
      "brand_about_us": brand["_brandAboutUs"],
      "brand_localtion": brand["_brandLocation"],
      "brand_website": brand["_brandWebsite"],
      "brand_registration_number": brand["_brandRegistrationNumber"],
      "brand_addr_country": brand["_brandAddrCountry"],
      "brand_addr_city": brand["_brandAddrCity"],
      "brand_addr_post_code": brand["_brandAddrPostCode"],
      "brand_type": brand["_brandType"],
      "brand_industry": brand["_brandIndustry"],
      "brand_contact_phone_number": brand["_brandContactPhoneNumber"],
      "brand_contact_email_address": brand["_brandContactEmailAddress"],
      "brand_lat": brand["_brandLat"],
      "brand_long": brand["_brandLong"],
      "brand_tax_id": brand["_brandTaxId"],
      "facebook_profile_link": brand["_facebookProfileLink"],
      "linkedin_profile_link": brand["_linkedinProfileLink"],
      "twitter_profile_link": brand["_twitterProfileLink"],
      "amazon_profile_link": brand["_amazonProfileLink"],
      "brand_pan_number": brand["_brandPanNumber"],
      "brand_bank_accnt_number": brand["_brandBankAccountNumber"],
      "brand_billing_to_fullname": brand["_brandBillingToFullname"],
      "brand_billing_to_email": brand["_brandbillingToEmail"],
      "brand_addr_line1": brand["_brandAddrLine1"],
      "brand_addr_line2": brand["_brandAddrLine2"],
      "brand_addr_state": brand["_brandAddrState"],
      "brand_GSTIN": brand["_brandGSTIN"],
      "brand_state_cd": brand["_brandStateCd"],
      "brand_shipping_info_as_billing_ind": brand["_brandShippingInfoAsBillingInd"],
      "brand_shipping_to_fullname": brand["_brandShippingToFullname"],
      "brand_shipping_to_email": brand["_brandShippingToEmail"],
      "brand_shipping_address_line1": brand["_brandShippingAddressLine1"],
      "brand_shipping_address_line2": brand["_brandShippingAddressLine2"],
      "brand_shipping_city": brand["_brandShippingCity"],
      "brand_shipping_state": brand["_brandShippingState"],
      "brand_shipping_zip_cd": brand["_brandShippingZipCd"],
      "brand_shipping_country": brand["_brandShippingCountry"],
      "period_in_business": brand["_periodInBusiness"],
      "area_covered": brand["_areaCovered"],
      "annual_turn_over": brand["_annualTurnCover"],
      "NMMC_cess_number": brand["_nmmcCessNumber"],
      "brand_CIN": brand["_brandCin"],
      "brand_yoy_revenue": brand["_brandYoyRevenue"],
      "brand_bank_name": brand["_brandBankName"],
      "brand_bank_branch_name": brand["_brandBankBranchName"],
      "brand_bank_accnout_number": brand["_brandBankAccountNum"],
      "brand_bank_IFSC_code": brand["_brandBankIfscCode"],
      "brand_drug_lic_number": brand["_brandDrugLicNum"],
      "brand_FSSAI_number": brand["_brandFssiNum"],
      "brand_ownnership": brand["_brandOwnership"],
      "brand_size": brand["_brandSize"]
    }];
    return brandObj;
  }
}
