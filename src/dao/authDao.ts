/// <reference path="../_all.d.ts" />
"use strict";

import tokenModule = require("../models/token");
import userModule = require("../models/user");

export abstract class AuthDao {

  public abstract getTokenInfo(id: string, callback: any): void;

  public abstract getUserById(id: any, callback: any);

  public abstract getTokenInfoByRefreshId(refreshId: string, callback: any): void;

  public abstract saveUser(user: userModule.User, callback: any): void;

  public abstract saveToken(token: tokenModule.Token, callback: any): void;

  public abstract updateToken(token: tokenModule.Token, callback: any): void;

  public abstract updateUser(user: userModule.User, callback: any): void;

  public abstract getUserByEmail(emailId: string, callback: any): void;

  public abstract getUserByToken(token: string, callback: any): void;

  public abstract deleteToken(tokenId: string, callback: any): void;
}