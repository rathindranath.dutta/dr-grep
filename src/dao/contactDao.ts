/// <reference path="../_all.d.ts" />
"use strict";

import contactModule = require("../models/contact");

export abstract class ContactDao {

    /**
    * create a contact
    */
    public abstract create(connection: contactModule.Contact, callback: any): void;

    /**
    * update a contact
    */
    public abstract update(connection: contactModule.Contact, callback: any): void;

}
