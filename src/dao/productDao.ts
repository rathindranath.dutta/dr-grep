/// <reference path="../_all.d.ts" />
"use strict";

import prodModule = require("../models/product");
import variationModule = require("../models/variation");

export abstract class ProductDao {

  /**
   * List all products
   */
  public abstract list(page: number, callback: any): void;

  /**
   * Get a product details
   */
  public abstract get(id: number, callback: any): void;

  /**
   * Get a product details
   */
  public abstract attachProducts(products: any[], callback: any): void;

  /**
   * Get a product details
   */
  public abstract getProductsByUserEmail(userEmail: string, callback: any): void;
  /**
   * Get product ids by guids
   */
  public abstract getProductIdsByGuids(guids: Array<string>, callback: any): void;

  /**
   * Get product ids by upcs and user_id
   */
  public abstract getProductIdsByUpcs(upcs: Array<string>, userId: number, callback: any): void

  /**
   * Get product(s) by brand
   */
  public abstract getProductsByBrand(brandId: number, callback: any): void;

  /**
   * Create a product
   */
  public abstract create(product: prodModule.Product, callback: any): void;

  /**
   * Create many products at a time
   */
  public abstract createMany(product: Array<prodModule.Product>, callback: any): void;

  /**
   * Update a product
   */
  public abstract update(product: prodModule.Product, callback: any): void;

  /**
   * Delete a product
   */
  public abstract delete(id: number, callback: any): void;

  /**
   * Get all variations of a product
   */
  public abstract getVariations(id: number, callback: any): void;

  /**
   * Get a variation of a product
   */
  public abstract getVariation(id: number, variationId: number, callback: any): void;

  /**
   * Add variations
   */
  public abstract addVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void;

  /**
   * Remove variations
   */
  public abstract removeVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void;

  /**
   * Update variations
   */
  public abstract updateVariations(variations: Array<variationModule.Variation>, callback: any): void;

}
