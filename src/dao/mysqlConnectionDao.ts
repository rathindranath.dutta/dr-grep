/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import connectionDaoModule = require("./connectionDao");
//import iqpModule = require("../models/iqp");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import connectionModule = require("../models/connection");
// import async = require("async");

export class MySqlConnectionDao extends connectionDaoModule.ConnectionDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /**
    * Get IQP details by brand
    */

    public getConnetedEntities(brandId: number, callback: any): void {
        let spName: string = "sp_getConnections";
        let params: Array<number> = [brandId];
        this._mysqlDao.execSelectSP(spName, params, callback);
    }

    /**
    * create a connection
    */

    public create(connection: connectionModule.Connection, callback: any): void {
        this._mysqlDao.createObject<connectionModule.Connection>(connection,
            function (id: number, error: any) {
                callback(id, error);
            });
    }

    /**
    * update a connection
    */

    public update(connection: connectionModule.Connection, callback: any): void {
        let jsonConnection = JSON.parse(typedjson.TypedJSON.stringify(connection));
        this._mysqlDao.updateObject<connectionModule.Connection>(new connectionModule.Connection(),
            jsonConnection, function (id: number, error: any) {
                callback(id, error);
            });
    }

}
