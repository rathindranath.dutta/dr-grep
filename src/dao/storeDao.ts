/// <reference path="../_all.d.ts" />
"use strict";

import storeModule = require("../models/store");

export abstract class StoreDao {

  /**
  * List all stores
  */
  public abstract list(callback: any): void;

  /**
  * Get a store details
  */
  public abstract get(id: number, callback: any): void;

  /**
  * Create a store
  */
  public abstract create(store: storeModule.Store, callback: any): void;

  /**
  * Update a store
  */
  public abstract update(store: storeModule.Store, callback: any): void;

  /**
  * Delete a store
  */
  public abstract delete(id: number, callback: any): void;

}
