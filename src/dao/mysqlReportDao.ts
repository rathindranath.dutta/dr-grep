/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import reportDaoModule = require("./reportDao");
// import async = require("async");

export class MySqlReportDao extends reportDaoModule.ReportDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /**
    * Get IQP details by brand
    */

    public getGstr1Rpt(brandId: number, issueMonth: number, issueYear: number, callback: any): void {
        let spName: string = "sp_getGstr1_r";
        let params: Array<any> = [brandId, issueMonth, issueYear];
        this._mysqlDao.execSelectSP(spName, params, callback);
    }
}