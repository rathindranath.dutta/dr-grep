/// <reference path="../_all.d.ts" />
"use strict";
import teamModule = require("../models/team");
export abstract class WorkflowDao {

    /**
     * add user to a team
     */
    public abstract insertUpdateWorkflowFeedData(action: string, workflowFeed: any, callback: any): void;
    public abstract getToDoList(brandId: number, userId: number, callback: any): void;
}
