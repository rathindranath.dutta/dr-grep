/// <reference path="../_all.d.ts" />
"use strict";
import teamModule = require("../models/team");
export abstract class TeamDao {

    /**
     * add user to a team
     */
    public abstract addUserToTeam(team: teamModule.Team, callback: any): void;

    public abstract getTeamMembers(brandId: number, callback: any): void;

    public abstract insertRule(ruleObject: any, callback: any): void;

    public abstract getRuleSets(brandId: number, callback: any): void;
}
