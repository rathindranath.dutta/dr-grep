/// <reference path="../_all.d.ts" />
"use strict";
import mysql = require("mysql");
var conf = require("../config/config.js");
import LoggerModule = require("../service/logger");


export namespace ConnectionPool {

  LoggerModule.Logger.getLogger().log("info", "creating connection pool is one time job...");
  LoggerModule.Logger.getLogger().log("info", "db host is :", process.env.DATABASE_HOST || conf.get("database.host"));
  var _pool: mysql.IPool = mysql.createPool({
    acquireTimeout  : 60000,
    connectTimeout  : conf.get("database.connectionTimeout"),
    connectionLimit : conf.get("database.connectionLimit"),
    host            : process.env.DATABASE_HOST || conf.get("database.host"),
    user            : process.env.DATABASE_USER || conf.get("database.user"),
    password        : process.env.DATABASE_PASSWORD || conf.get("database.password"),
    database        : process.env.DATABASE_DBNAME || conf.get("database.dbname")
  });

  _pool.on("acquire", function (connection: any) {
    LoggerModule.Logger.getLogger().log("info", "Connection %d acquired", connection.threadId );
  });

  _pool.on("connection", function (connection: any) {
    LoggerModule.Logger.getLogger().log("info", "Connection %d created", connection.threadId );
  });

  _pool.on("release", function (connection: any) {
    LoggerModule.Logger.getLogger().log("info", "Connection %d released", connection.threadId );
  });

  _pool.on("enqueue", function () {
    LoggerModule.Logger.getLogger().log("info", "Waiting for available connection slot");
  });

  export function getPool(): mysql.IPool {
    return _pool;
  }
}