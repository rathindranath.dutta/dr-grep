/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import ddDaoModule = require("./dynamicDiscountigDao");
// import iqpModule = require("../models/iqp");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import async = require("async");

export class MySqlDynamicDiscountingDao extends ddDaoModule.DynamicDiscountngDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }
    public earlyPayment(dynamicDiscounting: any, callback: any): void {
        let isCreated: boolean = false;
        let dynamicDiscountingObj = this.prepareDiscountingObj(dynamicDiscounting);
        this._mysqlDao.createMany(dynamicDiscountingObj, (id: number, error: any) => {
            isCreated = true;
            callback(isCreated, error);
        });
    }

    public actionOnDiscount(dynamicDiscounting: any, callback: any) {
        let isModified: boolean = false;
        let dynamicDiscountingObj: any = {
            "dd_approved_ind": dynamicDiscounting["dd_approved_ind"],
            "dd_approved_by_user_id": (dynamicDiscounting["dd_approved_by_user_id"] !== null
                && dynamicDiscounting["dd_approved_by_user_id"] !== undefined)
                ? dynamicDiscounting["dd_approved_by_user_id"] : null,
            "dd_approved_by_brand_id": (dynamicDiscounting["dd_approved_by_brand_id"] !== null
                && dynamicDiscounting["dd_approved_by_brand_id"] !== undefined)
                ? dynamicDiscounting["dd_approved_by_brand_id"] : null
        };
        let tableInfo: any = {
            "_tableName": "dynamic_discounting_payment",
            "dd_payment_id": dynamicDiscounting["dd_payment_id"]
        };
        this._mysqlDao.updateMultiplePropObj(tableInfo, dynamicDiscountingObj, (id: number, error: any) => {
            isModified = true;
            callback(error, isModified);
        });
    }

    private prepareDiscountingObj(dynamicDiscounting: any): any[] {
        let dynamicDiscountingObj: any[] = [{
            "_tableName": "dynamic_discounting_payment",
            "dd_created_by_brand_id": dynamicDiscounting["dd_created_by_brand_id"],
            "dd_created_by_user_id": dynamicDiscounting["dd_created_by_user_id"],
            "dd_iqp_id": dynamicDiscounting["dd_iqp_id"],
            "dd_iqp_issue_date": dynamicDiscounting["dd_iqp_issue_date"],
            "dd_iqp_due_date": dynamicDiscounting["dd_iqp_due_date"],
            "dd_early_pmnt_APR": dynamicDiscounting["dd_early_pmnt_APR"],
            "dd_early_pmnt_date": dynamicDiscounting["dd_early_pmnt_date"],
            "dd_iqp_total_amnt": dynamicDiscounting["dd_iqp_total_amnt"],
            "dd_iqp_total_amnt_discounted": dynamicDiscounting["dd_iqp_total_amnt_discounted"],
            "dd_approved_ind": dynamicDiscounting["dd_approved_ind"],
            "dd_approved_by_user_id": dynamicDiscounting["dd_approved_by_user_id"],
            "dd_approved_by_brand_id": dynamicDiscounting["dd_approved_by_brand_id"],
            "dd_active_ind": dynamicDiscounting["dd_active_ind"],
            "dd_sent_ind": dynamicDiscounting["dd_sent_ind"],
            "dd_sent_to_user_id": dynamicDiscounting["dd_sent_to_user_id"],
            "dd_sent_to_brand_id": dynamicDiscounting["dd_sent_to_brand_id"],
            "dd_invoice_number": dynamicDiscounting["dd_invoice_number"],
            "dd_iqp_term": dynamicDiscounting["dd_iqp_term"],
            "dd_nth_day_payment": dynamicDiscounting["dd_nth_day_payment"]
        }];
        return dynamicDiscountingObj;
    }
}