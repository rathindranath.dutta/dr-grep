/// <reference path="../_all.d.ts" />
"use strict";

import mysql = require("mysql");
import typedjson = require("typedjson");
import userModule = require("../models/user");
import connectionPoolModule = require("./connectionPool");
import LoggerModule = require("../service/logger");
var conf = require("../config/config.js");

/**
 * Abstract dao class
 */
export class MySqlDao {

  private _pool: mysql.IPool;
  /**
   * Constructor.
   *
   * @class MySqlDao
   * @constructor
   */

  constructor() {
    this._pool = connectionPoolModule.ConnectionPool.getPool();
  }

  // public createObject<T>(object: T, callback: any) {
  //   let tableNameKey = "_tableName";
  //   let jsonObject = JSON.parse(typedjson.TypedJSON.stringify(object));
  //   let tableName: string;
  //   if (jsonObject[tableNameKey] !== undefined) {
  //     tableName = object[tableNameKey] ? object[tableNameKey] : object.constructor.name;
  //     delete jsonObject[tableNameKey];
  //   }
  //   this.executeConnectionQuery("INSERT INTO " + tableName.toLowerCase() + " SET ?", jsonObject, callback);
  // }

  public createObject<T>(object: T, callback: any) {
    let tableNameKey = "_tableName";
    let tableName = object[tableNameKey] ? object[tableNameKey] : object.constructor.name;
    if (object[tableNameKey] !== undefined) {
      delete object[tableNameKey];
    }
    let jsonObject = JSON.parse(typedjson.TypedJSON.stringify(object));
    this.executeConnectionQuery("INSERT INTO " + tableName.
      toLowerCase() + " SET ?", jsonObject, callback);
  }

  public createMany<T>(objects: Array<T>, callback: any) {
    let keyToRemove = "__type";
    let tableNameKey = "_tableName";
    let objToSend = Array<Array<String>>();
    let tableName = objects[0][tableNameKey] ? objects[0][tableNameKey] : objects[0].constructor.name;
    let jsonObjects = JSON.parse(typedjson.TypedJSON.stringify(objects));
    jsonObjects.forEach((data) => {
      if (data[keyToRemove] !== undefined) {
        delete data[keyToRemove];
      }
      if (data[tableNameKey] !== undefined) {
        delete data[tableNameKey];
      }
      let objArray = new Array();
      Object.keys(data).forEach((key) => {
        objArray.push(data[key]);
      });
      objToSend.push(objArray);
    });
    let columns = Object.keys(jsonObjects[0]).join(",");
    this.executeConnectionQuery("INSERT INTO " + tableName.toLowerCase() + " (" + columns + ")" + " VALUES ?", [objToSend], callback);
  }

  public getObject<T>(object: T, param: Object, callback: any) {
    let tableNameKey = "_tableName";
    let tableName = object[tableNameKey] ? object[tableNameKey] : object.constructor.name;
    this.executeConnectionQuery("SELECT * FROM " + tableName.toLowerCase() + " WHERE " + Object.keys(param)[0] + " = ?",
      param[Object.keys(param)[0]], callback);
  }

  public getComplexObject<T>(object: T, query: string, param: Object, callback: any) {
    this.executeConnectionQuery(query + " = ?", param[Object.keys(param)[0]], callback);
  }

  public getObjectByQueryBuilder<T>(object: T, query: string, param: Object, callback: any) {
    let combinedValues: Array<string> = new Array<string>();
    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        combinedValues.push(param[key]);
      }
    }
    console.log("step3: query formed" + query);
    this.executeConnectionQuery(query, combinedValues, callback);
  }
  public getObjectByMuitipleCriteria<T>(object: T, query: string, param: Object, orderClause: boolean, callback: any) {
    let combinedValues: Array<string> = new Array<string>();
    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        query += key + " = ? and ";
        combinedValues.push(param[key]);
      }
    }
    query = query.substring(0, query.length - " and ".length);
    if (orderClause) {
      query = query + " order by det.iqp_issue_date desc";
    }
    console.log("step3: query formed" + query);
    this.executeConnectionQuery(query, combinedValues, callback);
  }
  public getObjectByQuery<T>(object: T, query: string, param: Object, callback: any) {
    let combinedValues: Array<string> = new Array<string>();
    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        // query += key + " = ? and ";
        combinedValues.push(param[key]);
      }
    }
    //query = query.substring(0, query.length - " and ".length);
    console.log("step3: query formed" + query);
    this.executeConnectionQuery(query, combinedValues, callback);
  }

  public listObject<T>(object: T, callback: any) {
    this.executeConnectionQuery("SELECT * FROM " + object.constructor.name.toLowerCase() + " LIMIT 10", {}, callback);
  }

  public listObjectByCriteria<T>(param: Object, callback: any) {
    const className = "className";
    const projection = "projection";
    const criteria = "criteria";
    let whereClause = new Array<string>();
    Object.keys(param[criteria]).forEach((key) => {
      whereClause.push(key + " in (" + param[criteria][key] + ")");
    });
    this.executeConnectionQuery("SELECT " + param[projection] +
      " FROM " + param[className].toLowerCase() + " WHERE " + whereClause.join(" AND "), {}, callback);
  }

  public updateObject<T>(object: T, param: Object, callback: any) {
    let primaryKeyColumnName: string = object.constructor.name.toLowerCase() + "_id";
    let prymarykeyValue: string = param[primaryKeyColumnName];
    delete param[primaryKeyColumnName];
    let combinedValues: Array<string> = new Array<string>();
    let query: string = "UPDATE " + object.constructor.name.toLowerCase() + " SET ";

    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        query += key + " = ? , ";
        combinedValues.push(param[key]);
      }
    }
    query = query.substring(0, query.length - ", ".length);
    query += "WHERE " + primaryKeyColumnName + " = ?";
    param[primaryKeyColumnName] = prymarykeyValue;
    combinedValues.push(prymarykeyValue);
    this.executeConnectionQuery(query, combinedValues, callback);
  }
  //param1: tableInfo = {"tableName":<tablename>,<updateClauseName>:"<updateClauseValue>"}
  //param2: param = {<param1>:value,<param2>:value,<param3>:value,...}
  public updateMultiplePropObj(tableInfo: Object, param: Object, callback: any) {
    let query: string = "UPDATE " + tableInfo[Object.keys(tableInfo)[0]] + " SET ";
    let primaryKeyColumnName: string = Object.keys(tableInfo)[1];
    let combinedValues: Array<string> = new Array<string>();

    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        query += key + " = ? , ";
        combinedValues.push(param[key]);
      }
    }
    query = query.substring(0, query.length - ", ".length);
    query += "WHERE " + primaryKeyColumnName + " = ?";
    combinedValues.push(tableInfo[primaryKeyColumnName]);
    let counter: number = 0;
    for (var key in tableInfo) {
      if (tableInfo.hasOwnProperty(key)) {
        if (counter > 1) {
          query += " AND " + key + " = ?";
          combinedValues.push(tableInfo[key]);
        }
        counter++;
      }
    }
    console.log("STEP3: query Formed: \n" + query);
    console.log("values: \n" + JSON.stringify(combinedValues));
    this.executeConnectionQuery(query, combinedValues, callback);
  }

  public updateObjectWithMultiple(param: Object, callback: any) {
    let arrPrimaryValues = param[Object.keys(param)[1]];
    let combinedValues: Array<string> = new Array<string>();
    combinedValues.push(param[Object.keys(param)[0]]);
    let query = "UPDATE " + Object.keys(param)[1] + " SET " + Object.keys(param)[0] + " = ? WHERE product_id IN (";
    arrPrimaryValues.forEach(element => {
      query += "? , ";
      combinedValues.push(element);
    });
    query = query.substring(0, query.length - " , ".length);
    query += ")";
    console.log("STEP3: query Formed: \n" + query);
    console.log("values: \n" + JSON.stringify(combinedValues));
    this.executeConnectionQuery(query, combinedValues, callback);
  }

  public deleteObject<T>(object: T, identifier: string, callback: any) {
    let columnName: string = object.constructor.name.toLowerCase() + "_id";
    this.executeConnectionQuery("DELETE FROM " + object.constructor.name.toLowerCase() + " WHERE " + columnName + " = ?",
      identifier, callback);
  }

  public deleteObjectWithCriteria(tableInfo: Object, callback: any) {
    let tableName: string = tableInfo[Object.keys(tableInfo)[0]];
    let primaryKeyColumnName: string = Object.keys(tableInfo)[1];
    let combinedValues: Array<string> = new Array<string>();
    combinedValues.push(tableInfo[primaryKeyColumnName]);
    this.executeConnectionQuery("DELETE FROM " + tableName + " WHERE " + primaryKeyColumnName + " = ?",
      combinedValues, callback);
  }

  public execSelectSP(spName: string, queryParam: any, callback: any) {
    let execSPQuery: string = "call " + spName + "(";
    queryParam.forEach(sqlParam => {
      execSPQuery = execSPQuery + " ?, ";
    });
    execSPQuery = execSPQuery.substring(0, execSPQuery.length - ", ".length);
    execSPQuery = execSPQuery + ")";
    console.log("values:\n" + JSON.stringify(queryParam));
    this.executeSelectSP(execSPQuery, queryParam, callback);
  }

  private executeConnectionQuery(statement: string, queryParam: any, callback: any) {
    LoggerModule.Logger.getLogger().log("info", "No of active connections now : ",
      this._pool["_allConnections"].length);
    this._pool.getConnection((err: mysql.IError, connection: mysql.IConnection) => {
      LoggerModule.Logger.getLogger().log("debug", "Query: ", statement);
      LoggerModule.Logger.getLogger().log("debug", "Parameters: ", queryParam);
      if (err != null) {
        console.log("err: " + err);
      }
      connection.query(statement, queryParam, (err: mysql.IError,
        result: any, fields: any) => {
        if (err != null) {
          console.log("query: " + statement);
          console.log("queryParam: " + JSON.stringify(queryParam));
          console.log("MYSQL ERROR:\n" + err);
          LoggerModule.Logger.getLogger().log("debug", "Error is: ", err);
          connection.release();
        }
        LoggerModule.Logger.getLogger().log("debug", "Result: ", result);
        if (result.constructor.name === "Array") { // result are returned rows
          let jsonRows: Array<Object> = new Array<Object>();
          result.forEach(function (row: any) {
            let jsonRow: Object = Object.assign({}, row);
            jsonRows.push(jsonRow);
          });
          callback(jsonRows, err);
        } else if (result.constructor.name === "OkPacket") {
          //multi insert or update
          if (result.affectedRows > 1) {
            callback(true, err);
          } else if (result.affectedRows === 1) {
            if (result.insertId > 0) { // single insert
              callback(result.insertId, err);
            } else { // single update or delete
              callback(true, err);
            }
          }
        } else {
          callback(false, err);
        }
        // And done with the connection.
        connection.release();
        // Don't use the connection here, it has been returned to the pool.
      });
    });
  }

  private executeSelectSP(statement: string, queryParam: any, callback: any) {
    LoggerModule.Logger.getLogger().log("info", "No of active connections now : ",
      this._pool["_allConnections"].length);
    this._pool.getConnection((err: mysql.IError, connection: mysql.IConnection) => {
      LoggerModule.Logger.getLogger().log("debug", "Query: ", statement);
      LoggerModule.Logger.getLogger().log("debug", "Parameters: ", queryParam);
      connection.query(statement, queryParam, (err: mysql.IError,
        result: any, fields: any) => {
        if (err != null) {
          LoggerModule.Logger.getLogger().log("debug", "Error is: ", err);
          connection.release();
        }
        LoggerModule.Logger.getLogger().log("debug", "Result: ", result);
        if (result.constructor.name === "Array") {
          let jsonRows: Array<Object> = [];
          result.forEach(item => {
            if (item.constructor.name !== "OkPacket") {
              jsonRows.push(item);
            }
          });
          callback(jsonRows, err);
        } else {
          callback(false, err);
        }
        // And done with the connection.
        connection.release();
        // Don't use the connection here, it has been returned to the pool.
      });
    });
  }
}