/// <reference path="../_all.d.ts" />
"use strict";

import brandModule = require("../models/brand");

export abstract class BrandDao {

  /**
   * Create a brand
   */
  public abstract create(brand: brandModule.Brand, callback: any): void;

  /**
   * Delete a brand
   */
  public abstract delete(id: number, callback: any): void;

  /**
  * Get a brand details
  */
  public abstract get(id: number, callback: any): void;

  /**
  * Get a brand details by userId
  */
  public abstract getBrandByUserEmail(userEmail: string, callback: any): void;

  /**
  * Get a brand by guid
  */
  public abstract getByGuid(guid: string, callback: any): void;

  /**
   * update brand details
   */
  public abstract updateBrand(brand: brandModule.Brand, callback: any): void;
}
