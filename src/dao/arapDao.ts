/// <reference path="../_all.d.ts" />
"use strict";

export abstract class ARAPDao {

    /**
     * Create a invoice
     */
    public abstract createEditInvoice(accountReceivable: any, sentInd: string, callback: any): void;

    // /**
    //  * Get all invoice details for a brand
    //  */
    // public abstract getArapDetailsByBrand(brandId: number, callback: any): void;

    /**
     * Get all AR/AP details for a brand
     */
    public abstract getAllArapDetails(brandId: number, type: string, account: string, isDiscountAvailble: string, callback: any): void

}
