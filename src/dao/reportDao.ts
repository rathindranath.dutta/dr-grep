/// <reference path="../_all.d.ts" />
"use strict";

export abstract class ReportDao {

    /**
     * get GSTR1 report
     */
    public abstract getGstr1Rpt(brandId: number, issueMonth: number, issueYear: number, callback: any): void;

}
