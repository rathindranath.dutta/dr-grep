/// <reference path="../_all.d.ts" />
"use strict";

import iqpModule = require("../models/iqp");

export abstract class IqpDao {

    /**
     * Create a brand
     */
    public abstract getIQPDetailsByBrand(brandId: number, callback: any): void;

    public abstract updateIQPStatus(iqpId: number, brandId: number, indicator: string, callback: any): void;

}
