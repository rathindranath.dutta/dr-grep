/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import iqpDaoModule = require("./arapDao");
import workflowModule = require("../dao/workflowDao");
import userModule = require("../models/user");
// import iqpModule = require("../models/iqp");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import async = require("async");

export class MySqlArAPDao extends iqpDaoModule.ARAPDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    private _workflowDao: workflowModule.WorkflowDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor(workflowDao: workflowModule.WorkflowDao) {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
        this._workflowDao = workflowDao;
    }

    /**
     * create Edit invoice
     */

    public createEditInvoice(accountReceivable: any, sentInd: string, callback: any): void {
        let updatedLineItems: any[];
        let insertedLineItems: any[];
        //let accountReceivable: any = accountReceivableObj; //.accountReceivable;
        let mode: string;
        console.log("DATA: " + JSON.stringify(accountReceivable));
        let iqpDetails: any[] = [{
            "iqp_type": accountReceivable["iqp_type"],
            "iqp_invoice_number": accountReceivable["iqp_invoice_number"],
            "iqp_brand_from_id": accountReceivable["iqp_brand_from_id"],
            "iqp_brand_to_id": accountReceivable["iqp_brand_to_id"],
            "iqp_user_from_id": accountReceivable["iqp_user_from_id"],
            "iqp_user_to_id": accountReceivable["iqp_user_to_id"],
            "iqp_contact_from_id": accountReceivable["iqp_contact_from_id"],
            "iqp_contact_to_email_address": accountReceivable["iqp_contact_to_email_address"],
            "iqp_contact_to_id": accountReceivable["iqp_contact_to_id"],
            "iqp_issue_date": (accountReceivable["iqp_issue_date"] !== ""
                && accountReceivable["iqp_issue_date"] !== null
                && accountReceivable["iqp_issue_date"] !== undefined)
                ? accountReceivable["iqp_issue_date"] : null,
            "iqp_number": accountReceivable["iqp_number"],
            "iqp_currency": accountReceivable["iqp_currency"],
            "iqp_due_date": (accountReceivable["iqp_due_date"] !== ""
                && accountReceivable["iqp_due_date"] !== null
                && accountReceivable["iqp_due_date"] !== undefined)
                ? accountReceivable["iqp_due_date"] : null,
            "iqp_last_date": (accountReceivable["iqp_last_date"] !== ""
                && accountReceivable["iqp_last_date"] !== null
                && accountReceivable["iqp_last_date"] !== undefined)
                ? accountReceivable["iqp_last_date"] : null,
            "iqp_note": accountReceivable["iqp_note"],
            "iqp_active_ind": accountReceivable["iqp_active_ind"],
            "iqp_term_days": accountReceivable["iqp_term_days"],
            "iqp_det_ledger_account_code": accountReceivable["iqp_det_ledger_account_code"],
            "iqp_reverse_charge_ind": accountReceivable["iqp_reverse_charge_ind"],
            "iqp_place_of_supply": accountReceivable["iqp_place_of_supply"],
            "iqp_received_invoice_ind": accountReceivable["iqp_received_invoice_ind"],
            "iqp_file_name": accountReceivable["iqp_file_name"]
        }];
        console.log("IQPDEtails: " + JSON.stringify(iqpDetails));
        if (accountReceivable.iqp_det_rowStateFlag === "I") {
            iqpDetails[0]._tableName = "iqp_det";
            this._mysqlDao.createMany<Array<any>>(iqpDetails, function (iqpId: number, error: any) {
                mode = "C";
                this.insertUpdateInvoiceDetails(iqpId, accountReceivable, mode, (isModified: boolean, error: any) => {
                    if (sentInd === "Y") {
                        this.sendInvoicePo(iqpId, accountReceivable, (error: any, isStatusInserted: boolean) => {
                            callback(isStatusInserted, iqpId, error);
                        });
                    } else {
                        callback(isModified, iqpId, error);
                    }
                });
            }.bind(this));
        } else if (accountReceivable.iqp_det_rowStateFlag === "U") {
            let tableInfo: any = {
                "_tableName": "iqp_det",
                "iqp_id": accountReceivable["iqp_id"],
            };
            mode = "E";
            this._mysqlDao.updateMultiplePropObj(tableInfo, iqpDetails[0], (isUpdated: boolean, error: any) => {
                if (!error) {
                    this.insertUpdateInvoiceDetails(accountReceivable["iqp_id"], accountReceivable, mode, (isModified: boolean, error: any) => {
                        if (!error) {
                            if (sentInd === "Y") {
                                this.sendInvoicePo(accountReceivable["iqp_id"], accountReceivable, (error: any, isStatusInserted: boolean) => {
                                    callback(isStatusInserted, accountReceivable["iqp_id"], error);
                                });
                            } else {
                                callback(isModified, accountReceivable["iqp_id"], error);
                            }
                        } else {
                            callback(error, false);
                        }
                    });
                } else {
                    callback(error, false);
                }
            });
        } else {
            this.insertUpdateInvoiceDetails(accountReceivable["iqp_id"], accountReceivable, mode, (isModified: boolean, error: any) => {
                if (sentInd === "Y") {
                    this.sendInvoicePo(accountReceivable["iqp_id"], accountReceivable, (error: any, isStatusInserted: boolean) => {
                        callback(isStatusInserted, accountReceivable["iqp_id"], error);
                    });
                } else {
                    callback(isModified, accountReceivable["iqp_id"], error);
                }
            });
        }
    }
    public sendInvoicePo(iqpId: number, accountReceivable: any, callback: any): void {
        let query: string = `SELECT iqp_id FROM iqp_status WHERE `;
        let iqpStatus: any;
        let param: any = { "iqp_id": iqpId };
        this._mysqlDao.getObjectByMuitipleCriteria(iqpStatus, query, param, false, (iqpStatusArray: any, error: any) => {
            if (iqpStatusArray !== null && iqpStatusArray !== undefined && iqpStatusArray.length > 0) {
                let updateIqpStatusInfo = {
                    "_tableName": "iqp_status",
                    "iqp_id": iqpId
                };
                let paramIqpStatus: any = {
                    "iqp_approved_ind": null,
                    "iqp_approved_by_user": null,
                    "iqp_approved_by_brand": null,
                    "iqp_rejected_ind": null,
                    "iqp_rejected_by_user": null,
                    "iqp_rejected_by_brand": null,
                    "iqp_paid_and_closed_ind": null
                };
                this._mysqlDao.updateMultiplePropObj(updateIqpStatusInfo, paramIqpStatus, (id: number, error: any) => {
                    if (!error) {
                        this.insertWorkflowFeedData(iqpId, accountReceivable, (error, isCompleted) => {
                            callback(error, isCompleted);
                        });
                    } else {
                        callback(error, false);
                    }
                });
            } else {
                let insertedItem: any[] = [{
                    "_tableName": "iqp_status",
                    "iqp_id": iqpId,
                    "iqp_sent_ind": "Y",
                    "iqp_sent_by_brand": accountReceivable.iqp_status.iqp_sent_by_brand
                }];
                this._mysqlDao.createMany(insertedItem, (id: number, error: any) => {
                    if (error === null) {
                        this.insertWorkflowFeedData(iqpId, accountReceivable, (error, isCompleted) => {
                            callback(error, isCompleted);
                        });
                    } else {
                        callback(error, false);
                    }
                });
            }
        });
    }

    public insertUpdateInvoiceDetails(iqpId: number, accountReceivable: any, mode: string, callback: any) {
        console.log("STEP3- iqpid: " + iqpId);

        let updatedLineItems: any[] = [];
        let insertedLineItems: any[] = [];
        let deletedLineItems: any[] = [];
        let isModified: boolean = false;
        accountReceivable["line_items"].forEach(element => {
            if (element["iqp_line_items_rowStateFlag"] === "I") {
                delete element["iqp_line_items_rowStateFlag"];
                element["_tableName"] = "iqp_line_item";
                element["iqp_id"] = iqpId;
                insertedLineItems.push(element);
            } else if (element["iqp_line_items_rowStateFlag"] === "U") {
                delete element["iqp_line_items_rowStateFlag"];
                updatedLineItems.push(element);
            } else if (element["iqp_line_items_rowStateFlag"] === "D") {
                delete element["iqp_line_items_rowStateFlag"];
                deletedLineItems.push(element);
            }
        });
        async.parallel([
            (callback: any) => {
                if (insertedLineItems.length > 0 || updatedLineItems.length > 0 || deletedLineItems.length > 0) {
                    if (insertedLineItems.length > 0) {
                        this._mysqlDao.createMany(insertedLineItems, (id: number, error: any) => {
                            isModified = true;
                            callback(null, isModified);
                        });
                    }
                    if (updatedLineItems.length > 0) {
                        async.eachSeries(updatedLineItems,
                            (eachUpdatedItem, callback: any) => {
                                let tableInfo: any = {
                                    "_tableName": "iqp_line_item",
                                    "iqp_line_item_id": eachUpdatedItem["iqp_line_item_id"]
                                };
                                this._mysqlDao.updateMultiplePropObj(tableInfo, eachUpdatedItem, (id: number, error: any) => {
                                    isModified = true;
                                    callback(null, isModified);
                                });
                                tableInfo = null;

                            }, (err: any) => {
                                isModified = true;
                                callback(err, isModified);
                            });
                    }
                    if (deletedLineItems.length > 0) {
                        async.eachSeries(deletedLineItems,
                            (eachUpdatedItem, callback: any) => {
                                let tableInfo: any = {
                                    "_tableName": "iqp_line_item",
                                    "iqp_line_item_id": eachUpdatedItem["iqp_line_item_id"]
                                };
                                this._mysqlDao.deleteObjectWithCriteria(tableInfo, (isDeleted: boolean, error: any) => {
                                    callback(error, isDeleted);
                                });
                                tableInfo = null;
                            }, (err: any) => {
                                callback(err, false);
                            });
                    }
                } else {
                    isModified = false;
                    callback(null, isModified);
                }
            },
            (callback: any) => {
                let totalAmount: any[] = [{
                    "iqp_id": iqpId,
                    "iqp_total_raw": accountReceivable["total_amount"].iqp_total_raw,
                    "iqp_discount_perc": accountReceivable["total_amount"].iqp_discount_perc,
                    "iqp_discount_perc_amnt": accountReceivable["total_amount"].iqp_discount_perc_amnt,
                    "iqp_discount_man": accountReceivable["total_amount"].iqp_discount_man,
                    "iqp_adjustment": accountReceivable["total_amount"].iqp_adjustment,
                    "iqp_shipping_charge": accountReceivable["total_amount"].iqp_shipping_charge,
                    "iqp_VAT_perc": accountReceivable["total_amount"].iqp_VAT_perc,
                    "iqp_VAT": accountReceivable["total_amount"].iqp_VAT,
                    "iqp_paid": accountReceivable["total_amount"].iqp_paid,
                    "iqp_total_final": accountReceivable["total_amount"].iqp_total_final,
                    "iqp_discount_perc_ind": accountReceivable["total_amount"].iqp_discount_perc_ind,
                    "iqp_discount_man_ind": accountReceivable["total_amount"].iqp_discount_man_ind,
                    "iqp_VAT_ind": accountReceivable["total_amount"].iqp_VAT_ind,
                    "iqp_CGST_perc_amount": accountReceivable["total_amount"].iqp_CGST_perc_amount,
                    "iqp_SGST_perc_amount": accountReceivable["total_amount"].iqp_SGST_perc_amount,
                    "iqp_IGST_perc_amount": accountReceivable["total_amount"].iqp_IGST_perc_amount,
                    "iqp_total_amnt_in_words": accountReceivable["total_amount"].iqp_total_amnt_in_words
                }];
                if (accountReceivable["total_amount"].iqp_total_amnt_rowStateFlag === "I") {
                    totalAmount[0]._tableName = "iqp_total_amnt";
                    this._mysqlDao.createMany(totalAmount, (iqpId: number, error: any) => {
                        isModified = true;
                        callback(null, isModified);
                    });
                } else if (accountReceivable["total_amount"].iqp_total_amnt_rowStateFlag === "U") {
                    delete accountReceivable.total_amount.iqp_line_items_rowStateFlag;
                    let tableInfo: any = {
                        "_tableName": "iqp_total_amnt",
                        "iqp_id": iqpId
                    };
                    this._mysqlDao.updateMultiplePropObj(tableInfo, totalAmount[0], (id: number, error: any) => {
                        isModified = true;
                        callback(null, isModified);
                    });
                    tableInfo = null;
                } else {
                    isModified = false;
                    callback(null, isModified);
                }
            },
        ], function (err: any, isModified: any) {
            callback(isModified, err);
        });
    }

    public getAllArapDetails(brandId: number, type: string, account: string, discountType: string, callback: any): void {
        let param: any;
        let action: string;
        if (type === "PO" && account === "pay") {
            param = { "det.iqp_brand_from_id": brandId, "iqp_type": type, "det.iqp_active_ind": 1 };
            action = "sent";
        } else if (type === "PO" && account === "rec") {
            param = { "det.iqp_brand_to_id": brandId, "iqp_type": type, "det.iqp_active_ind": 1 };
            action = "received";
        } else if (type === "I" && account === "pay") {
            param = { "det.iqp_brand_to_id": brandId, "iqp_type": type, "det.iqp_active_ind": 1 };
            action = "received";
        } else {
            param = { "det.iqp_brand_from_id": brandId, "iqp_type": type, "det.iqp_active_ind": 1 };
            action = "sent";
        }
        this.getArapDetailsByBrand(param, action, discountType, callback);
    }

    private insertWorkflowFeedData(iqpId: number, accountReceivable: any, callback: any): void {
        // let wffParam: any = {
        //     "wfrm_brand_id": accountReceivable["iqp_brand_to_id"],
        //     "wfrm_entity_type": (accountReceivable["iqp_type"] === "I") ? "Invoice" : "PO",
        //     "wfrm_activity_role": "Submitted"
        // };
        // let query: string = `select wfrm_user_id from work_flow_rule_master where `;
        let workflowFeedObjArray: any[] = [];
        // this._mysqlDao.getObjectByMuitipleCriteria(new userModule.User(), query, wffParam, false,
        //     (result: any, error: any) => {
        //         if (result.length > 0) {
        //             let users: any[] = result;
        //             users.forEach(element => {
        let workflowFeedObj: any = {
            "_tableName": "work_flow_feed",
            "source_user_id": global.currentUserId,
            "destination_user_id": 0, //element["wfrm_user_id"],
            "from_brand_id": accountReceivable.iqp_brand_from_id,
            "to_brand_id": accountReceivable.iqp_brand_to_id,
            "entity_type_code": (accountReceivable.iqp_type === "I") ? "Invoice" : "PO",
            "entity_number": iqpId,
            "activity_type_code": "Submitted",
            "activity_message": accountReceivable.iqp_note,
            "activity_close_ind": 1,
            "open_time": null
            //close_time is not present at the time of inserting, it will only update later
        };
        //workflowFeed["userId"] = element;
        workflowFeedObjArray.push(workflowFeedObj);
        // });
        this._workflowDao.insertUpdateWorkflowFeedData("SUBMIT", workflowFeedObjArray,
            (error: any, isCompleted: boolean) => {
                callback(error, isCompleted);
            });
    }

    private getArapDetailsByBrand(param: any, action: string, discountType: string, callback: any): void {
        //console.log("step2: " + "brandid: " + brandId);
        //let param: any = { "det.iqp_brand_from_id": brandId };
        let query: string;
        //Drafted(Not yet sent) PO/invoices will show in the repository
        //Left join with iqp_status
        if (action === "sent") {
            query = `SELECT
                IFNULL(det.iqp_id, 0) AS 'iqp_id',
                IFNULL(det.iqp_type, '') AS 'iqp_type',
                IFNULL(det.iqp_invoice_number, '') AS 'iqp_invoice_number',
                IFNULL(det.iqp_brand_from_id, 0) AS 'iqp_brand_from_id',
                IFNULL(det.iqp_brand_to_id, 0) AS 'iqp_brand_to_id',
                IFNULL(det.iqp_user_from_id, 0) AS 'iqp_user_from_id',
                IFNULL(det.iqp_user_to_id, 0) AS 'iqp_user_to_id',
                IFNULL(det.iqp_contact_from_id, 0) AS 'iqp_contact_from_id',
                IFNULL(det.iqp_contact_to_email_address, '') AS 'iqp_contact_to_email_address',
                IFNULL(det.iqp_contact_to_id, 0) AS 'iqp_contact_to_id',
                IFNULL(DATE_FORMAT(det.iqp_issue_date,'%d/%m/%Y'),'') AS 'iqp_issue_date',
                IFNULL(det.iqp_number, 0) AS 'iqp_number',
                IFNULL(det.iqp_currency, '') AS 'iqp_currency',
                IFNULL(DATE_FORMAT(det.iqp_due_date,'%d/%m/%Y'),'') AS 'iqp_due_date',
                IFNULL(det.iqp_last_date,'') AS 'iqp_last_date',
                IFNULL(det.iqp_note, '') AS 'iqp_note',
                IFNULL(det.iqp_active_ind, 0) AS 'iqp_active_ind',
                IFNULL(det.iqp_term_days, '') AS 'iqp_term_days',
                IFNULL(det.iqp_det_ledger_account_code, 0) AS 'iqp_det_ledger_account_code',
                IFNULL(det.iqp_reverse_charge_ind,0) AS 'iqp_reverse_charge_ind',
                IFNULL(det.iqp_place_of_supply,"") AS 'iqp_place_of_supply',
                IFNULL(det.iqp_received_invoice_ind, 0) As 'iqp_received_invoice_ind',
                IFNULL(det.iqp_file_name, '') As 'iqp_file_name',
                CASE 
                    WHEN IFNULL(det.iqp_id,0)=0 
                    THEN NULL 
                    ELSE 'N'
                END as 'iqp_det_rowStateFlag',

                st.iqp_id AS 'iqp_status:iqp_id',
                st.iqp_sent_ind AS 'iqp_status:iqp_sent_ind',
                st.iqp_sent_by_user AS 'iqp_status:iqp_sent_by_user',
                st.iqp_sent_by_brand AS 'iqp_status:iqp_sent_by_brand',
                st.iqp_approved_ind AS 'iqp_status:iqp_approved_ind',
                st.iqp_approved_by_user AS 'iqp_status:iqp_approved_by_user',
                st.iqp_approved_by_brand AS 'iqp_status:iqp_approved_by_brand',
                st.iqp_rejected_ind AS 'iqp_status:iqp_rejected_ind',
                st.iqp_rejected_by_user AS 'iqp_status:iqp_rejected_by_user',
                st.iqp_rejected_by_brand AS 'iqp_status:iqp_rejected_by_brand',
                st.iqp_paid_and_closed_ind AS 'iqp_status:iqp_paid_and_closed_ind',

                IFNULL(tamt.iqp_id,0.00) AS 'total_amount:iqp_id',
                IFNULL(tamt.iqp_total_raw,0.00) AS 'total_amount:iqp_total_raw',
                IFNULL(tamt.iqp_discount_perc,0.00) AS 'total_amount:iqp_discount_perc',
                IFNULL(tamt.iqp_discount_perc_amnt,0.00) AS 'total_amount:iqp_discount_perc_amnt',
                IFNULL(tamt.iqp_discount_man,0.00) AS 'total_amount:iqp_discount_man',
                IFNULL(tamt.iqp_adjustment,0.00) AS 'total_amount:iqp_adjustment',
                IFNULL(tamt.iqp_shipping_charge,0.00) AS 'total_amount:iqp_shipping_charge',
                IFNULL(tamt.iqp_VAT_perc,0.00) AS 'total_amount:iqp_VAT_perc',
                IFNULL(tamt.iqp_VAT,0.00) AS 'total_amount:iqp_VAT',
                IFNULL(tamt.iqp_paid,0.00) AS 'total_amount:iqp_paid',
                IFNULL(tamt.iqp_total_final,0.00) AS 'total_amount:iqp_total_final',
                IFNULL(tamt.iqp_discount_perc_ind,0.00) AS 'total_amount:iqp_discount_perc_ind',
                IFNULL(tamt.iqp_discount_man_ind,0.00) AS 'total_amount:iqp_discount_man_ind',
                IFNULL(tamt.iqp_VAT_ind,0) AS 'total_amount:iqp_VAT_ind',
                IFNULL(tamt.iqp_CGST_perc_amount,0.00) AS 'total_amount:iqp_CGST_perc_amount',
                IFNULL(tamt.iqp_SGST_perc_amount,0.00) AS 'total_amount:iqp_SGST_perc_amount',
                IFNULL(tamt.iqp_IGST_perc_amount,0.00) AS 'total_amount:iqp_IGST_perc_amount',
                IFNULL(tamt.iqp_total_amnt_in_words,'') AS 'total_amount:iqp_total_amnt_in_words',
                CASE 
                    WHEN IFNULL(tamt.iqp_id,0) = 0
                    THEN NULL 
                    ELSE 'N' 
                END as 'total_amount:iqp_total_amnt_rowStateFlag',
                
                dd_payment_id AS 'dynamic_discounting:dd_payment_id',
                dd_created_by_brand_id AS 'dynamic_discounting:dd_created_by_brand_id',
                dd_created_by_user_id AS 'dynamic_discounting:dd_created_by_user_id',
                dd_iqp_issue_date AS 'dynamic_discounting:dd_iqp_issue_date',
                dd_iqp_due_date AS 'dynamic_discounting:dd_iqp_due_date',
                dd_early_pmnt_APR AS 'dynamic_discounting:dd_early_pmnt_APR',
                DATE_FORMAT(dd_early_pmnt_date,'%d/%m/%Y') AS 'dynamic_discounting:dd_early_pmnt_date',
                dd_early_pmnt_applicable_APR AS 'dynamic_discounting:dd_early_pmnt_applicable_APR',
                dd_early_pmnt_amnt AS 'dynamic_discounting:dd_early_pmnt_amnt',
                dd_iqp_total_amnt AS 'dynamic_discounting:dd_iqp_total_amnt',
                dd_iqp_total_amnt_discounted AS 'dynamic_discounting:dd_iqp_total_amnt_discounted',
                dd_approved_ind AS 'dynamic_discounting:dd_approved_ind',
                dd_approved_by_user_id AS 'dynamic_discounting:dd_approved_by_user_id',
                dd_approved_by_brand_id AS 'dynamic_discounting:dd_approved_by_brand_id',
                dd_active_ind AS 'dynamic_discounting:dd_active_ind',
                dd_sent_ind AS 'dynamic_discounting:dd_sent_ind',
                dd_sent_to_user_id AS 'dynamic_discounting:dd_sent_to_user_id',
                dd_sent_to_brand_id AS 'dynamic_discounting:dd_sent_to_brand_id',
                dd_invoice_number AS 'dynamic_discounting:dd_invoice_number',
                dd_iqp_term AS 'dynamic_discounting:dd_iqp_term',

                li.iqp_line_item_id AS 'line_items:iqp_line_item_id',
                li.iqp_id AS 'line_items:iqp_id',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_prod_id,0)
                END AS 'line_items:iqp_prod_id',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_HSN_accounting_cd,0)
                END AS 'line_items:iqp_HSN_accounting_cd',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit,0)
                END AS 'line_items:iqp_unit',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit_rate,0)
                END AS 'line_items:iqp_unit_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_total_excl_tax,0)
                END AS 'line_items:iqp_line_total_excl_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_tax,0)
                END AS 'line_items:iqp_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_total_incl_tax,0)
                END AS 'line_items:iqp_line_total_incl_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_active_ind,0)
                END AS 'line_items:iqp_line_item_active_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_discount_amount,0)
                END AS 'line_items:iqp_line_item_discount_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_prod_desc,0)
                END AS 'line_items:iqp_prod_desc',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_discount_amount,0)
                END AS 'line_items:iqp_line_item_discount_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_taxable_amount,0)
                END AS 'line_items:iqp_line_item_taxable_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_HSN_accounting_cd,0)
                END AS 'line_items:iqp_HSN_accounting_cd',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_ind,0)
                END AS 'line_items:iqp_CGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_perc_rate,0)
                END AS 'line_items:iqp_CGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_perc_amount,0)
                END AS 'line_items:iqp_CGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_ind,0)
                END AS 'line_items:iqp_SGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_perc_rate,0)
                END AS 'line_items:iqp_SGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_perc_amount,0)
                END AS 'line_items:iqp_SGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_ind,0)
                END AS 'line_items:iqp_IGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_perc_rate,0)
                END AS 'line_items:iqp_IGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_perc_amount,0)
                END AS 'line_items:iqp_IGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_ledger_account_code,0)
                END AS 'line_items:iqp_line_item_ledger_account_code',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit_measure,"")
                END AS 'line_items:iqp_unit_measure',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_added_to_stock_ind,0)
                END AS 'line_items:iqp_line_item_added_to_stock_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL 
                    ELSE 'N'
                END as 'line_items:iqp_line_items_rowStateFlag',
                
                b.user_id AS 'sent_from_brand:_userId',
                b.brand_id AS 'sent_from_brand:_brandId',
                IFNULL(b.brand_name,"") AS 'sent_from_brand:_brandName',
                b.brand_desc AS 'sent_from_brand:_brandDesc',
                b.guid AS 'sent_from_brand:_guid',
                b.is_active AS 'sent_from_brand:_isActive',
                IFNULL(b.brand_about_us,"") AS 'sent_from_brand:_brandAboutUs',
                IFNULL(b.brand_localtion,"") AS 'sent_from_brand:_brandLocation', 
                IFNULL(b.brand_website,"") AS 'sent_from_brand:_brandWebsite',
                IFNULL(b.brand_registration_number,"") AS 'sent_from_brand:_brandRegistrationNumber',
                IFNULL(b.brand_addr_country,"") AS 'sent_from_brand:_brandAddrCountry',
                IFNULL(b.brand_addr_city,"") AS 'sent_from_brand:_brandAddrCity',
                IFNULL(b.brand_addr_post_code,"") AS 'sent_from_brand:_brandAddrPostCode',
                IFNULL(b.brand_type,"") AS 'sent_from_brand:_brandType',
                IFNULL(b.brand_industry,"") AS 'sent_from_brand:_brandIndustry',
                IFNULL(b.brand_contact_phone_number,0) AS 'sent_from_brand:_brandContactPhoneNumber',
                IFNULL(b.brand_contact_email_address,"") AS 'sent_from_brand:_brandContactEmailAddress',
                IFNULL(b.brand_lat,"") AS 'sent_from_brand:_brandLat',
                IFNULL(b.brand_long,"") AS 'sent_from_brand:_brandLong',
                IFNULL(b.brand_tax_id,"") AS 'sent_from_brand:_brandTaxId',
                IFNULL(b.facebook_profile_link,"") AS 'sent_from_brand:_facebookProfileLink',
                IFNULL(b.linkedin_profile_link,"") AS 'sent_from_brand:_linkedinProfileLink',
                IFNULL(b.twitter_profile_link,"") AS 'sent_from_brand:_twitterProfileLink',
                IFNULL(b.amazon_profile_link,"") AS 'sent_from_brand:_amazonProfileLink',
                IFNULL(b.brand_pan_number,"") AS 'sent_from_brand:_brandPanNumber',
                IFNULL(b.brand_bank_accnt_number,"") AS 'sent_from_brand:_brandBankAccountNumber',
                IFNULL(b.brand_billing_to_fullname,"") AS 'sent_from_brand:_brandBillingToFullname',
                IFNULL(b.brand_billing_to_email,"") AS 'sent_from_brand:_brandbillingToEmail',
                IFNULL(b.brand_addr_line1,"") AS 'sent_from_brand:_brandAddrLine1',
                IFNULL(b.brand_addr_line2,"") AS 'sent_from_brand:_brandAddrLine2',
                IFNULL(b.brand_addr_state,"") AS 'sent_from_brand:_brandAddrState',
                IFNULL(b.brand_GSTIN,"") AS 'sent_from_brand:_brandGSTIN',
                IFNULL(b.brand_state_cd,"") AS 'sent_from_brand:_brandStateCd',
                IFNULL(b.brand_shipping_info_as_billing_ind,0) AS 'sent_from_brand:_brandShippingInfoAsBillingInd',
                IFNULL(b.brand_shipping_to_fullname,"") AS 'sent_from_brand:_brandShippingToFullname',
                IFNULL(b.brand_shipping_to_email,"") AS 'sent_from_brand:_brandShippingToEmail',
                IFNULL(b.brand_shipping_address_line1,"") AS 'sent_from_brand:_brandShippingAddressLine1',
                IFNULL(b.brand_shipping_address_line2,"") AS 'sent_from_brand:_brandShippingAddressLine2',
                IFNULL(b.brand_shipping_city,"") AS 'sent_from_brand:_brandShippingCity',
                IFNULL(b.brand_shipping_state,"") AS 'sent_from_brand:_brandShippingState',
                IFNULL(b.brand_shipping_zip_cd,"") AS 'sent_from_brand:_brandShippingZiipCd',
                IFNULL(b.brand_shipping_country,"") AS 'sent_from_brand:_brandShippingCountry',
                IFNULL(b.period_in_business,0) AS 'sent_from_brand:_periodInBusiness',
                IFNULL(b.area_covered,"") AS 'sent_from_brand:_areaCovered',
                IFNULL(b.annual_turn_over,"") AS 'sent_from_brand:_annualTurnCover',
                IFNULL(b.NMMC_cess_number,0) AS 'sent_from_brand:_nmmcCessNumber',

                b1.user_id AS 'sent_to_brand:_userId',
                b1.brand_id AS 'sent_to_brand:_brandId',
                IFNULL(b1.brand_name,"") AS 'sent_to_brand:_brandName',
                b1.brand_desc AS 'sent_to_brand:_brandDesc',
                b1.guid AS 'sent_to_brand:_guid',
                b1.is_active AS 'sent_to_brand:_isActive',
                IFNULL(b1.brand_about_us,"") AS 'sent_to_brand:_brandAboutUs',
                IFNULL(b1.brand_localtion,"") AS 'sent_to_brand:_brandLocation', 
                IFNULL(b1.brand_website,"") AS 'sent_to_brand:_brandWebsite',
                IFNULL(b1.brand_registration_number,"") AS 'sent_to_brand:_brandRegistrationNumber',
                IFNULL(b1.brand_addr_country,"") AS 'sent_to_brand:_brandAddrCountry',
                IFNULL(b1.brand_addr_city,"") AS 'sent_to_brand:_brandAddrCity',
                IFNULL(b1.brand_addr_post_code,"") AS 'sent_to_brand:_brandAddrPostCode',
                IFNULL(b1.brand_type,"") AS 'sent_to_brand:_brandType',
                IFNULL(b1.brand_industry,"") AS 'sent_to_brand:_brandIndustry',
                IFNULL(b1.brand_contact_phone_number,0) AS 'sent_to_brand:_brandContactPhoneNumber',
                IFNULL(b1.brand_contact_email_address,"") AS 'sent_to_brand:_brandContactEmailAddress',
                IFNULL(b1.brand_lat,"") AS 'sent_to_brand:_brandLat',
                IFNULL(b1.brand_long,"") AS 'sent_to_brand:_brandLong',
                IFNULL(b1.brand_tax_id,"") AS 'sent_to_brand:_brandTaxId',
                IFNULL(b1.facebook_profile_link,"") AS 'sent_to_brand:_facebookProfileLink',
                IFNULL(b1.linkedin_profile_link,"") AS 'sent_to_brand:_linkedinProfileLink',
                IFNULL(b1.twitter_profile_link,"") AS 'sent_to_brand:_twitterProfileLink',
                IFNULL(b1.amazon_profile_link,"") AS 'sent_to_brand:_amazonProfileLink',
                IFNULL(b1.brand_pan_number,"") AS 'sent_to_brand:_brandPanNumber',
                IFNULL(b1.brand_bank_accnt_number,"") AS 'sent_to_brand:_brandBankAccountNumber',
                IFNULL(b1.brand_billing_to_fullname,"") AS 'sent_to_brand:_brandBillingToFullname',
                IFNULL(b1.brand_billing_to_email,"") AS 'sent_to_brand:_brandbillingToEmail',
                IFNULL(b1.brand_addr_line1,"") AS 'sent_to_brand:_brandAddrLine1',
                IFNULL(b1.brand_addr_line2,"") AS 'sent_to_brand:_brandAddrLine2',
                IFNULL(b1.brand_addr_state,"") AS 'sent_to_brand:_brandAddrState',
                IFNULL(b1.brand_GSTIN,"") AS 'sent_to_brand:_brandGSTIN',
                IFNULL(b1.brand_state_cd,"") AS 'sent_to_brand:_brandStateCd',
                IFNULL(b1.brand_shipping_info_as_billing_ind,0) AS 'sent_to_brand:_brandShippingInfoAsBillingInd',
                IFNULL(b1.brand_shipping_to_fullname,"") AS 'sent_to_brand:_brandShippingToFullname',
                IFNULL(b1.brand_shipping_to_email,"") AS 'sent_to_brand:_brandShippingToEmail',
                IFNULL(b1.brand_shipping_address_line1,"") AS 'sent_to_brand:_brandShippingAddressLine1',
                IFNULL(b1.brand_shipping_address_line2,"") AS 'sent_to_brand:_brandShippingAddressLine2',
                IFNULL(b1.brand_shipping_city,"") AS 'sent_to_brand:_brandShippingCity',
                IFNULL(b1.brand_shipping_state,"") AS 'sent_to_brand:_brandShippingState',
                IFNULL(b1.brand_shipping_zip_cd,"") AS 'sent_to_brand:_brandShippingZiipCd',
                IFNULL(b1.brand_shipping_country,"") AS 'sent_to_brand:_brandShippingCountry',
                IFNULL(b1.period_in_business,0) AS 'sent_to_brand:_periodInBusiness',
                IFNULL(b1.area_covered,"") AS 'sent_to_brand:_areaCovered',
                IFNULL(b1.annual_turn_over,"") AS 'sent_to_brand:_annualTurnCover',
                IFNULL(b1.NMMC_cess_number,0) AS 'sent_to_brand:_nmmcCessNumber',

                u.user_id as 'sent_from_user:_usrId',
                u.user_ext_id as 'sent_from_user:_usrExtId',
                u.user_name as 'sent_from_user:_usrName',
                u.user_email_id as 'sent_from_user:_usrEmail',
                u.user_dob as 'sent_from_user:_usrDob',
                u.user_sex as 'sent_from_user:_usrSex',
                u.user_is_seller as 'sent_from_user:_usrIsSeller',
                u.user_login_source as 'sent_from_user:_usrLoginSource',
                u.user_is_active as 'sent_from_user:_usrIsActive',

                u1.user_id as 'sent_to_user:_usrId',
                u1.user_ext_id as 'sent_to_user:_usrExtId',
                u1.user_name as 'sent_to_user:_usrName',
                u1.user_email_id as 'sent_to_user:_usrEmail',
                u1.user_dob as 'sent_to_user:_usrDob',
                u1.user_sex as 'sent_to_user:_usrSex',
                u1.user_is_seller as 'sent_to_user:_usrIsSeller',
                u1.user_login_source as 'sent_to_user:_usrLoginSource',
                u1.user_is_active as 'sent_to_user:_usrIsActive',

                c1.contact_id as 'sent_from_contact:_contactId',
                c1.contact_first_name as 'sent_from_contact:_contactFirstName',
                c1.contact_last_name as 'sent_from_contact:_contactLastName',
                c1.contact_phone_number as 'sent_from_contact:_contactPhoneNumber',
                c1.contact_address as 'sent_from_contact:_contactContactAddress',
                c1.contact_city as 'sent_from_contact:_contactContactCity',
                c1.contact_zip_cd as 'sent_from_contact:_contactZipCode',
                c1.contact_state as 'sent_from_contact:_contactState',
                c1.contact_email_address as 'sent_from_contact:_contactEmailAddress',
                c1.contact_addr_line1 as 'sent_from_contact:_contactAddrLine1',
                c1.contact_addr_line2 as 'sent_from_contact:_contactAddrLine2',
                c1.contact_country as 'sent_from_contact:_contactCountry',
                c1.contact_GSTIN as 'sent_from_contact:_contactGstin',
                c1.contact_state_cd as 'sent_from_contact:_contactStateCode',
                c1.contact_business_name as 'sent_from_contact:_contactBusinessName',
                c1.contact_shipping_to_fullname as 'sent_from_contact:_contactShippingToFullName',
                c1.contact_shipping_to_email as 'sent_from_contact:_contactShippingToEmail',
                c1.contact_shipping_address_line1 as 'sent_from_contact:_contactShippingAddressLine1',
                c1.contact_shipping_address_line2 as 'sent_from_contact:_contactShippingAddressLine2',
                c1.contact_shipping_city as 'sent_from_contact:_contactShippingCity',
                c1.contact_shipping_state as 'sent_from_contact:_contactShippingState',
                c1.contact_shipping_zip_cd as 'sent_from_contact:_contactShippingZipCode',
                c1.contact_shipping_country as 'sent_from_contact:_contactShippingCountry',
  
                c.contact_id as 'sent_to_contact:_contactId',
                c.contact_first_name as 'sent_to_contact:_contactFirstName',
                c.contact_last_name as 'sent_to_contact:_contactLastName',
                c.contact_phone_number as 'sent_to_contact:_contactPhoneNumber',
                c.contact_address as 'sent_to_contact:_contactContactAddress',
                c.contact_city as 'sent_to_contact:_contactContactCity',
                c.contact_zip_cd as 'sent_to_contact:_contactZipCode',
                c.contact_state as 'sent_to_contact:_contactState',
                c.contact_email_address as 'sent_to_contact:_contactEmailAddress',
                c.contact_addr_line1 as 'sent_to_contact:_contactAddrLine1',
                c.contact_addr_line2 as 'sent_to_contact:_contactAddrLine2',
                c.contact_country as 'sent_to_contact:_contactCountry',
                c.contact_GSTIN as 'sent_to_contact:_contactGstin',
                c.contact_state_cd as 'sent_to_contact:_contactStateCode',
                c.contact_business_name as 'sent_to_contact:_contactBusinessName',
                c.contact_shipping_to_fullname as 'sent_to_contact:_contactShippingToFullName',
                c.contact_shipping_to_email as 'sent_to_contact:_contactShippingToEmail',
                c.contact_shipping_address_line1 as 'sent_to_contact:_contactShippingAddressLine1',
                c.contact_shipping_address_line2 as 'sent_to_contact:_contactShippingAddressLine2',
                c.contact_shipping_city as 'sent_to_contact:_contactShippingCity',
                c.contact_shipping_state as 'sent_to_contact:_contactShippingState',
                c.contact_shipping_zip_cd as 'sent_to_contact:_contactShippingZipCode',
                c.contact_shipping_country as 'sent_to_contact:_contactShippingCountry'

                FROM  
                iqp_det det 
                LEFT JOIN iqp_status st ON det.iqp_id = st.iqp_id
                LEFT JOIN iqp_line_item li ON det.iqp_id = li.iqp_id and li.iqp_line_item_active_ind = 'Y'
                LEFT JOIN iqp_total_amnt tamt ON det.iqp_id = tamt.iqp_id
                LEFT JOIN dynamic_discounting_payment dd ON det.iqp_id = dd.dd_iqp_id
                LEFT JOIN brand b ON b.brand_id = det.iqp_brand_from_id
                LEFT JOIN brand b1 ON b1.brand_id = det.iqp_brand_to_id
                LEFT JOIN user u ON u.user_id = det.iqp_user_from_id
                left join user u1 on u1.user_id = det.iqp_user_to_id
                left join contact c1 on c1.contact_id = det.iqp_contact_from_id
                left join contact c on c.contact_id = det.iqp_contact_to_id
                WHERE `;
        } else {
            //Drafted(Not yet sent) PO/invoices will NOT show in the repository
            //inner join with iqp_status
            query = `SELECT
                IFNULL(det.iqp_id, 0) AS 'iqp_id',
                IFNULL(det.iqp_type, '') AS 'iqp_type',
                IFNULL(det.iqp_invoice_number, '') AS 'iqp_invoice_number',
                IFNULL(det.iqp_brand_from_id, 0) AS 'iqp_brand_from_id',
                IFNULL(det.iqp_brand_to_id, 0) AS 'iqp_brand_to_id',
                IFNULL(det.iqp_user_from_id, 0) AS 'iqp_user_from_id',
                IFNULL(det.iqp_user_to_id, 0) AS 'iqp_user_to_id',
                IFNULL(det.iqp_contact_from_id, 0) AS 'iqp_contact_from_id',
                IFNULL(det.iqp_contact_to_email_address, '') AS 'iqp_contact_to_email_address',
                IFNULL(det.iqp_contact_to_id, 0) AS 'iqp_contact_to_id',
                IFNULL(DATE_FORMAT(det.iqp_issue_date,'%d/%m/%Y'),'') AS 'iqp_issue_date',
                IFNULL(det.iqp_number, 0) AS 'iqp_number',
                IFNULL(det.iqp_currency, '') AS 'iqp_currency',
                IFNULL(DATE_FORMAT(det.iqp_due_date,'%d/%m/%Y'),'') AS 'iqp_due_date',
                IFNULL(det.iqp_last_date,'') AS 'iqp_last_date',
                IFNULL(det.iqp_note, '') AS 'iqp_note',
                IFNULL(det.iqp_active_ind, 0) AS 'iqp_active_ind',
                IFNULL(det.iqp_term_days, '') AS 'iqp_term_days',
                IFNULL(det.iqp_det_ledger_account_code, 0) AS 'iqp_det_ledger_account_code',
                IFNULL(det.iqp_reverse_charge_ind,0) AS 'iqp_reverse_charge_ind',
                IFNULL(det.iqp_place_of_supply,"") AS 'iqp_place_of_supply',
                IFNULL(det.iqp_received_invoice_ind, 0) As 'iqp_received_invoice_ind',
                IFNULL(det.iqp_file_name, '') As 'iqp_file_name',
                CASE 
                    WHEN IFNULL(det.iqp_id,0) = 0 
                    THEN NULL 
                    ELSE 'N'
                END as 'iqp_det_rowStateFlag',

                st.iqp_id AS 'iqp_status:iqp_id',
                st.iqp_sent_ind AS 'iqp_status:iqp_sent_ind',
                st.iqp_sent_by_user AS 'iqp_status:iqp_sent_by_user',
                st.iqp_sent_by_brand AS 'iqp_status:iqp_sent_by_brand',
                st.iqp_approved_ind AS 'iqp_status:iqp_approved_ind',
                st.iqp_approved_by_user AS 'iqp_status:iqp_approved_by_user',
                st.iqp_approved_by_brand AS 'iqp_status:iqp_approved_by_brand',
                st.iqp_rejected_ind AS 'iqp_status:iqp_rejected_ind',
                st.iqp_rejected_by_user AS 'iqp_status:iqp_rejected_by_user',
                st.iqp_rejected_by_brand AS 'iqp_status:iqp_rejected_by_brand',
                st.iqp_paid_and_closed_ind AS 'iqp_status:iqp_paid_and_closed_ind',

                IFNULL(tamt.iqp_id,0.00) AS 'total_amount:iqp_id',
                IFNULL(tamt.iqp_total_raw,0.00) AS 'total_amount:iqp_total_raw',
                IFNULL(tamt.iqp_discount_perc,0.00) AS 'total_amount:iqp_discount_perc',
                IFNULL(tamt.iqp_discount_perc_amnt,0.00) AS 'total_amount:iqp_discount_perc_amnt',
                IFNULL(tamt.iqp_discount_man,0.00) AS 'total_amount:iqp_discount_man',
                IFNULL(tamt.iqp_adjustment,0.00) AS 'total_amount:iqp_adjustment',
                IFNULL(tamt.iqp_shipping_charge,0.00) AS 'total_amount:iqp_shipping_charge',
                IFNULL(tamt.iqp_VAT_perc,0.00) AS 'total_amount:iqp_VAT_perc',
                IFNULL(tamt.iqp_VAT,0.00) AS 'total_amount:iqp_VAT',
                IFNULL(tamt.iqp_paid,0.00) AS 'total_amount:iqp_paid',
                IFNULL(tamt.iqp_total_final,0.00) AS 'total_amount:iqp_total_final',
                IFNULL(tamt.iqp_discount_perc_ind,0.00) AS 'total_amount:iqp_discount_perc_ind',
                IFNULL(tamt.iqp_discount_man_ind,0.00) AS 'total_amount:iqp_discount_man_ind',
                IFNULL(tamt.iqp_VAT_ind,0) AS 'total_amount:iqp_VAT_ind',
                IFNULL(tamt.iqp_CGST_perc_amount,0.00) AS 'total_amount:iqp_CGST_perc_amount',
                IFNULL(tamt.iqp_SGST_perc_amount,0.00) AS 'total_amount:iqp_SGST_perc_amount',
                IFNULL(tamt.iqp_IGST_perc_amount,0.00) AS 'total_amount:iqp_IGST_perc_amount',
                IFNULL(tamt.iqp_total_amnt_in_words,'') AS 'total_amount:iqp_total_amnt_in_words',
                CASE 
                    WHEN IFNULL(tamt.iqp_id,0) = 0
                    THEN NULL 
                    ELSE 'N'
                END as 'total_amount:iqp_total_amnt_rowStateFlag',
                
                dd_payment_id AS 'dynamic_discounting:dd_payment_id',
                dd_created_by_brand_id AS 'dynamic_discounting:dd_created_by_brand_id',
                dd_created_by_user_id AS 'dynamic_discounting:dd_created_by_user_id',
                dd_iqp_issue_date AS 'dynamic_discounting:dd_iqp_issue_date',
                dd_iqp_due_date AS 'dynamic_discounting:dd_iqp_due_date',
                dd_early_pmnt_APR AS 'dynamic_discounting:dd_early_pmnt_APR',
                DATE_FORMAT(dd_early_pmnt_date,'%d/%m/%Y') AS 'dynamic_discounting:dd_early_pmnt_date',
                dd_early_pmnt_applicable_APR AS 'dynamic_discounting:dd_early_pmnt_applicable_APR',
                dd_early_pmnt_amnt AS 'dynamic_discounting:dd_early_pmnt_amnt',
                dd_iqp_total_amnt AS 'dynamic_discounting:dd_iqp_total_amnt',
                dd_iqp_total_amnt_discounted AS 'dynamic_discounting:dd_iqp_total_amnt_discounted',
                dd_approved_ind AS 'dynamic_discounting:dd_approved_ind',
                dd_approved_by_user_id AS 'dynamic_discounting:dd_approved_by_user_id',
                dd_approved_by_brand_id AS 'dynamic_discounting:dd_approved_by_brand_id',
                dd_active_ind AS 'dynamic_discounting:dd_active_ind',
                dd_sent_ind AS 'dynamic_discounting:dd_sent_ind',
                dd_sent_to_user_id AS 'dynamic_discounting:dd_sent_to_user_id',
                dd_sent_to_brand_id AS 'dynamic_discounting:dd_sent_to_brand_id',
                dd_invoice_number AS 'dynamic_discounting:dd_invoice_number',
                dd_iqp_term AS 'dynamic_discounting:dd_iqp_term',

                li.iqp_line_item_id AS 'line_items:iqp_line_item_id',
                li.iqp_id AS 'line_items:iqp_id',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_prod_id,0)
                END AS 'line_items:iqp_prod_id',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_HSN_accounting_cd,0)
                END AS 'line_items:iqp_HSN_accounting_cd',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit,0)
                END AS 'line_items:iqp_unit',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit_rate,0)
                END AS 'line_items:iqp_unit_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_total_excl_tax,0)
                END AS 'line_items:iqp_line_total_excl_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_tax,0)
                END AS 'line_items:iqp_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_total_incl_tax,0)
                END AS 'line_items:iqp_line_total_incl_tax',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_active_ind,0)
                END AS 'line_items:iqp_line_item_active_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_discount_amount,0)
                END AS 'line_items:iqp_line_item_discount_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_prod_desc,0)
                END AS 'line_items:iqp_prod_desc',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_discount_amount,0)
                END AS 'line_items:iqp_line_item_discount_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_taxable_amount,0)
                END AS 'line_items:iqp_line_item_taxable_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_HSN_accounting_cd,0)
                END AS 'line_items:iqp_HSN_accounting_cd',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_ind,0)
                END AS 'line_items:iqp_CGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_perc_rate,0)
                END AS 'line_items:iqp_CGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_CGST_perc_amount,0)
                END AS 'line_items:iqp_CGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_ind,0)
                END AS 'line_items:iqp_SGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_perc_rate,0)
                END AS 'line_items:iqp_SGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_SGST_perc_amount,0)
                END AS 'line_items:iqp_SGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_ind,0)
                END AS 'line_items:iqp_IGST_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_perc_rate,0)
                END AS 'line_items:iqp_IGST_perc_rate',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_IGST_perc_amount,0)
                END AS 'line_items:iqp_IGST_perc_amount',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_ledger_account_code,0)
                END AS 'line_items:iqp_line_item_ledger_account_code',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_unit_measure,"")
                END AS 'line_items:iqp_unit_measure',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL
                    ELSE IFNULL(li.iqp_line_item_added_to_stock_ind,0)
                END AS 'line_items:iqp_line_item_added_to_stock_ind',
                CASE 
                    WHEN IFNULL(li.iqp_id,0) = 0 
                    THEN NULL 
                    ELSE 'N'
                END as 'line_items:iqp_line_items_rowStateFlag',
                
                b.user_id AS 'sent_from_brand:_userId',
                b.brand_id AS 'sent_from_brand:_brandId',
                IFNULL(b.brand_name,"") AS 'sent_from_brand:_brandName',
                b.brand_desc AS 'sent_from_brand:_brandDesc',
                b.guid AS 'sent_from_brand:_guid',
                b.is_active AS 'sent_from_brand:_isActive',
                IFNULL(b.brand_about_us,"") AS 'sent_from_brand:_brandAboutUs',
                IFNULL(b.brand_localtion,"") AS 'sent_from_brand:_brandLocation', 
                IFNULL(b.brand_website,"") AS 'sent_from_brand:_brandWebsite',
                IFNULL(b.brand_registration_number,"") AS 'sent_from_brand:_brandRegistrationNumber',
                IFNULL(b.brand_addr_country,"") AS 'sent_from_brand:_brandAddrCountry',
                IFNULL(b.brand_addr_city,"") AS 'sent_from_brand:_brandAddrCity',
                IFNULL(b.brand_addr_post_code,"") AS 'sent_from_brand:_brandAddrPostCode',
                IFNULL(b.brand_type,"") AS 'sent_from_brand:_brandType',
                IFNULL(b.brand_industry,"") AS 'sent_from_brand:_brandIndustry',
                IFNULL(b.brand_contact_phone_number,0) AS 'sent_from_brand:_brandContactPhoneNumber',
                IFNULL(b.brand_contact_email_address,"") AS 'sent_from_brand:_brandContactEmailAddress',
                IFNULL(b.brand_lat,"") AS 'sent_from_brand:_brandLat',
                IFNULL(b.brand_long,"") AS 'sent_from_brand:_brandLong',
                IFNULL(b.brand_tax_id,"") AS 'sent_from_brand:_brandTaxId',
                IFNULL(b.facebook_profile_link,"") AS 'sent_from_brand:_facebookProfileLink',
                IFNULL(b.linkedin_profile_link,"") AS 'sent_from_brand:_linkedinProfileLink',
                IFNULL(b.twitter_profile_link,"") AS 'sent_from_brand:_twitterProfileLink',
                IFNULL(b.amazon_profile_link,"") AS 'sent_from_brand:_amazonProfileLink',
                IFNULL(b.brand_pan_number,"") AS 'sent_from_brand:_brandPanNumber',
                IFNULL(b.brand_bank_accnt_number,"") AS 'sent_from_brand:_brandBankAccountNumber',
                IFNULL(b.brand_billing_to_fullname,"") AS 'sent_from_brand:_brandBillingToFullname',
                IFNULL(b.brand_billing_to_email,"") AS 'sent_from_brand:_brandbillingToEmail',
                IFNULL(b.brand_addr_line1,"") AS 'sent_from_brand:_brandAddrLine1',
                IFNULL(b.brand_addr_line2,"") AS 'sent_from_brand:_brandAddrLine2',
                IFNULL(b.brand_addr_state,"") AS 'sent_from_brand:_brandAddrState',
                IFNULL(b.brand_GSTIN,"") AS 'sent_from_brand:_brandGSTIN',
                IFNULL(b.brand_state_cd,"") AS 'sent_from_brand:_brandStateCd',
                IFNULL(b.brand_shipping_info_as_billing_ind,0) AS 'sent_from_brand:_brandShippingInfoAsBillingInd',
                IFNULL(b.brand_shipping_to_fullname,"") AS 'sent_from_brand:_brandShippingToFullname',
                IFNULL(b.brand_shipping_to_email,"") AS 'sent_from_brand:_brandShippingToEmail',
                IFNULL(b.brand_shipping_address_line1,"") AS 'sent_from_brand:_brandShippingAddressLine1',
                IFNULL(b.brand_shipping_address_line2,"") AS 'sent_from_brand:_brandShippingAddressLine2',
                IFNULL(b.brand_shipping_city,"") AS 'sent_from_brand:_brandShippingCity',
                IFNULL(b.brand_shipping_state,"") AS 'sent_from_brand:_brandShippingState',
                IFNULL(b.brand_shipping_zip_cd,"") AS 'sent_from_brand:_brandShippingZiipCd',
                IFNULL(b.brand_shipping_country,"") AS 'sent_from_brand:_brandShippingCountry',
                IFNULL(b.period_in_business,0) AS 'sent_from_brand:_periodInBusiness',
                IFNULL(b.area_covered,"") AS 'sent_from_brand:_areaCovered',
                IFNULL(b.annual_turn_over,"") AS 'sent_from_brand:_annualTurnCover',
                IFNULL(b.NMMC_cess_number,0) AS 'sent_from_brand:_nmmcCessNumber',

                b1.user_id AS 'sent_to_brand:_userId',
                b1.brand_id AS 'sent_to_brand:_brandId',
                IFNULL(b1.brand_name,"") AS 'sent_to_brand:_brandName',
                b1.brand_desc AS 'sent_to_brand:_brandDesc',
                b1.guid AS 'sent_to_brand:_guid',
                b1.is_active AS 'sent_to_brand:_isActive',
                IFNULL(b1.brand_about_us,"") AS 'sent_to_brand:_brandAboutUs',
                IFNULL(b1.brand_localtion,"") AS 'sent_to_brand:_brandLocation', 
                IFNULL(b1.brand_website,"") AS 'sent_to_brand:_brandWebsite',
                IFNULL(b1.brand_registration_number,"") AS 'sent_to_brand:_brandRegistrationNumber',
                IFNULL(b1.brand_addr_country,"") AS 'sent_to_brand:_brandAddrCountry',
                IFNULL(b1.brand_addr_city,"") AS 'sent_to_brand:_brandAddrCity',
                IFNULL(b1.brand_addr_post_code,"") AS 'sent_to_brand:_brandAddrPostCode',
                IFNULL(b1.brand_type,"") AS 'sent_to_brand:_brandType',
                IFNULL(b1.brand_industry,"") AS 'sent_to_brand:_brandIndustry',
                IFNULL(b1.brand_contact_phone_number,0) AS 'sent_to_brand:_brandContactPhoneNumber',
                IFNULL(b1.brand_contact_email_address,"") AS 'sent_to_brand:_brandContactEmailAddress',
                IFNULL(b1.brand_lat,"") AS 'sent_to_brand:_brandLat',
                IFNULL(b1.brand_long,"") AS 'sent_to_brand:_brandLong',
                IFNULL(b1.brand_tax_id,"") AS 'sent_to_brand:_brandTaxId',
                IFNULL(b1.facebook_profile_link,"") AS 'sent_to_brand:_facebookProfileLink',
                IFNULL(b1.linkedin_profile_link,"") AS 'sent_to_brand:_linkedinProfileLink',
                IFNULL(b1.twitter_profile_link,"") AS 'sent_to_brand:_twitterProfileLink',
                IFNULL(b1.amazon_profile_link,"") AS 'sent_to_brand:_amazonProfileLink',
                IFNULL(b1.brand_pan_number,"") AS 'sent_to_brand:_brandPanNumber',
                IFNULL(b1.brand_bank_accnt_number,"") AS 'sent_to_brand:_brandBankAccountNumber',
                IFNULL(b1.brand_billing_to_fullname,"") AS 'sent_to_brand:_brandBillingToFullname',
                IFNULL(b1.brand_billing_to_email,"") AS 'sent_to_brand:_brandbillingToEmail',
                IFNULL(b1.brand_addr_line1,"") AS 'sent_to_brand:_brandAddrLine1',
                IFNULL(b1.brand_addr_line2,"") AS 'sent_to_brand:_brandAddrLine2',
                IFNULL(b1.brand_addr_state,"") AS 'sent_to_brand:_brandAddrState',
                IFNULL(b1.brand_GSTIN,"") AS 'sent_to_brand:_brandGSTIN',
                IFNULL(b1.brand_state_cd,"") AS 'sent_to_brand:_brandStateCd',
                IFNULL(b1.brand_shipping_info_as_billing_ind,0) AS 'sent_to_brand:_brandShippingInfoAsBillingInd',
                IFNULL(b1.brand_shipping_to_fullname,"") AS 'sent_to_brand:_brandShippingToFullname',
                IFNULL(b1.brand_shipping_to_email,"") AS 'sent_to_brand:_brandShippingToEmail',
                IFNULL(b1.brand_shipping_address_line1,"") AS 'sent_to_brand:_brandShippingAddressLine1',
                IFNULL(b1.brand_shipping_address_line2,"") AS 'sent_to_brand:_brandShippingAddressLine2',
                IFNULL(b1.brand_shipping_city,"") AS 'sent_to_brand:_brandShippingCity',
                IFNULL(b1.brand_shipping_state,"") AS 'sent_to_brand:_brandShippingState',
                IFNULL(b1.brand_shipping_zip_cd,"") AS 'sent_to_brand:_brandShippingZiipCd',
                IFNULL(b1.brand_shipping_country,"") AS 'sent_to_brand:_brandShippingCountry',
                IFNULL(b1.period_in_business,0) AS 'sent_to_brand:_periodInBusiness',
                IFNULL(b1.area_covered,"") AS 'sent_to_brand:_areaCovered',
                IFNULL(b1.annual_turn_over,"") AS 'sent_to_brand:_annualTurnCover',
                IFNULL(b1.NMMC_cess_number,0) AS 'sent_to_brand:_nmmcCessNumber',

                u.user_id as 'sent_from_user:_usrId',
                u.user_ext_id as 'sent_from_user:_usrExtId',
                u.user_name as 'sent_from_user:_usrName',
                u.user_email_id as 'sent_from_user:_usrEmail',
                u.user_dob as 'sent_from_user:_usrDob',
                u.user_sex as 'sent_from_user:_usrSex',
                u.user_is_seller as 'sent_from_user:_usrIsSeller',
                u.user_login_source as 'sent_from_user:_usrLoginSource',
                u.user_is_active as 'sent_from_user:_usrIsActive',

                u1.user_id as 'sent_to_user:_usrId',
                u1.user_ext_id as 'sent_to_user:_usrExtId',
                u1.user_name as 'sent_to_user:_usrName',
                u1.user_email_id as 'sent_to_user:_usrEmail',
                u1.user_dob as 'sent_to_user:_usrDob',
                u1.user_sex as 'sent_to_user:_usrSex',
                u1.user_is_seller as 'sent_to_user:_usrIsSeller',
                u1.user_login_source as 'sent_to_user:_usrLoginSource',
                u1.user_is_active as 'sent_to_user:_usrIsActive',

                c1.contact_id as 'sent_from_contact:_contactId',
                c1.contact_first_name as 'sent_from_contact:_contactFirstName',
                c1.contact_last_name as 'sent_from_contact:_contactLastName',
                c1.contact_phone_number as 'sent_from_contact:_contactPhoneNumber',
                c1.contact_address as 'sent_from_contact:_contactContactAddress',
                c1.contact_city as 'sent_from_contact:_contactContactCity',
                c1.contact_zip_cd as 'sent_from_contact:_contactZipCode',
                c1.contact_state as 'sent_from_contact:_contactState',
                c1.contact_email_address as 'sent_from_contact:_contactEmailAddress',
                c1.contact_addr_line1 as 'sent_from_contact:_contactAddrLine1',
                c1.contact_addr_line2 as 'sent_from_contact:_contactAddrLine2',
                c1.contact_country as 'sent_from_contact:_contactCountry',
                c1.contact_GSTIN as 'sent_from_contact:_contactGstin',
                c1.contact_state_cd as 'sent_from_contact:_contactStateCode',
                c1.contact_business_name as 'sent_from_contact:_contactBusinessName',
                c1.contact_shipping_to_fullname as 'sent_from_contact:_contactShippingToFullName',
                c1.contact_shipping_to_email as 'sent_from_contact:_contactShippingToEmail',
                c1.contact_shipping_address_line1 as 'sent_from_contact:_contactShippingAddressLine1',
                c1.contact_shipping_address_line2 as 'sent_from_contact:_contactShippingAddressLine2',
                c1.contact_shipping_city as 'sent_from_contact:_contactShippingCity',
                c1.contact_shipping_state as 'sent_from_contact:_contactShippingState',
                c1.contact_shipping_zip_cd as 'sent_from_contact:_contactShippingZipCode',
                c1.contact_shipping_country as 'sent_from_contact:_contactShippingCountry',
  
                c.contact_id as 'sent_to_contact:_contactId',
                c.contact_first_name as 'sent_to_contact:_contactFirstName',
                c.contact_last_name as 'sent_to_contact:_contactLastName',
                c.contact_phone_number as 'sent_to_contact:_contactPhoneNumber',
                c.contact_address as 'sent_to_contact:_contactAddress',
                c.contact_city as 'sent_to_contact:_contactCity',
                c.contact_zip_cd as 'sent_to_contact:_contactZipCode',
                c.contact_state as 'sent_to_contact:_contactState',
                c.contact_email_address as 'sent_to_contact:_contactEmailAddress',
                c.contact_addr_line1 as 'sent_to_contact:_contactAddrLine1',
                c.contact_addr_line2 as 'sent_to_contact:_contactAddrLine2',
                c.contact_country as 'sent_to_contact:_contactCountry',
                c.contact_GSTIN as 'sent_to_contact:_contactGstin',
                c.contact_state_cd as 'sent_to_contact:_contactStateCode',
                c.contact_business_name as 'sent_to_contact:_contactBusinessName',
                c.contact_shipping_to_fullname as 'sent_to_contact:_contactShippingToFullName',
                c.contact_shipping_to_email as 'sent_to_contact:_contactShippingToEmail',
                c.contact_shipping_address_line1 as 'sent_to_contact:_contactShippingAddressLine1',
                c.contact_shipping_address_line2 as 'sent_to_contact:_contactShippingAddressLine2',
                c.contact_shipping_city as 'sent_to_contact:_contactShippingCity',
                c.contact_shipping_state as 'sent_to_contact:_contactShippingState',
                c.contact_shipping_zip_cd as 'sent_to_contact:_contactShippingZipCode',
                c.contact_shipping_country as 'sent_to_contact:_contactShippingCountry'

                FROM  
                iqp_det det 
                INNER JOIN iqp_status st ON det.iqp_id = st.iqp_id
                LEFT JOIN iqp_line_item li ON det.iqp_id = li.iqp_id and li.iqp_line_item_active_ind = 'Y'
                LEFT JOIN iqp_total_amnt tamt ON det.iqp_id = tamt.iqp_id
                LEFT JOIN dynamic_discounting_payment dd ON det.iqp_id = dd.dd_iqp_id
                LEFT JOIN brand b ON b.brand_id = det.iqp_brand_from_id
                LEFT JOIN brand b1 ON b1.brand_id = det.iqp_brand_to_id
                LEFT JOIN user u ON u.user_id = det.iqp_user_from_id
                left join user u1 on u1.user_id = det.iqp_user_to_id
                left join contact c1 on c1.contact_id = det.iqp_contact_from_id
                left join contact c on c.contact_id = det.iqp_contact_to_id
                WHERE `;
        }
        if (discountType === "N") {
            query = query + "det.iqp_due_date>CURDATE() and dd.dd_iqp_id is null and st.iqp_paid_and_closed_ind is null and ";
        } else if (discountType === "C") {
            query = query + "det.iqp_due_date>CURDATE() and dd.dd_iqp_id is not null and st.iqp_paid_and_closed_ind is null and ";
        } else if (discountType === "P") {
            query = query + "dd.dd_iqp_id is not null and st.iqp_paid_and_closed_ind = 1 and ";
        }
        this.getArapDetails(query, param, callback);
    }

    private getArapDetails(query: string, param: any, callback: any) {
        let accountPayableData: any;
        this._mysqlDao.getObjectByMuitipleCriteria(accountPayableData, query, param, true,
            function (result: any, error: any) {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    accountPayableData = tree.getData();
                    callback(accountPayableData, error);
                } else {
                    callback(null, error);
                }
            });
    }
}