/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import iqpDaoModule = require("./iqpDao");
import iqpModule = require("../models/iqp");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
// import async = require("async");

export class MySqlIqpDao extends iqpDaoModule.IqpDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /**
    * Get IQP details by brand
    */

    public getIQPDetailsByBrand(brandId: number, callback: any): void {
        console.log("step2: " + "brandid: " + brandId);
        let param: any = { "det.iqp_brand_to_id": brandId };
        let query: string = `select
                IFNULL(b.brand_name, '') as '_fromBrandName',
                IFNULL(u.user_name, '') as '_fromUserName',
                IFNULL(det.iqp_id, 0) as '_iqpId',
                IFNULL(iqp_type, '') as '_type',
                IFNULL(iqp_invoice_number, '') as '_invoiceNumber',
                IFNULL(iqp_brand_from_id, 0) as '_brandFromId',
                IFNULL(iqp_brand_to_id, 0) as '_brandToId',
                IFNULL(iqp_user_from_id, 0) as '_userFromId',
                IFNULL(iqp_user_to_id, 0) as '_userToId',
                IFNULL(iqp_contact_to_email_address, '') as '_contactToEmailAddress',
                IFNULL(iqp_contact_to_id, '') as '_contactToId',
                IFNULL(iqp_issue_date, '') as '_issueDate',
                IFNULL(iqp_number, 0) as '_iqpNumber',
                IFNULL(iqp_currency, '') as '_currency',
                IFNULL(iqp_due_date, '') as '_dueDate',
                IFNULL(iqp_last_date, '') as '_lastDate',
                IFNULL(iqp_note, '') as '_note',
                IFNULL(iqp_active_ind, 0) as '_activeInd',
                IFNULL(iqp_term_days, '') as '_termDays',
                IFNULL(iqp_det_ledger_account_code, 0) as '_ledgerAccountCode',
                IFNULL(iqp_sent_ind, '') as '_sentInd',
                IFNULL(iqp_sent_by_user, 0) as '_sentByUser',
                IFNULL(iqp_sent_by_brand, 0) as '_sentByBrand',
                IFNULL(iqp_approved_ind, '') as '_approvedInd',
                IFNULL(iqp_approved_by_user, 0) as '_approvedByUser',
                IFNULL(iqp_approved_by_brand, 0) as '_approvedByBrand',
                IFNULL(iqp_rejected_ind, '') as '_rejectedInd',
                IFNULL(iqp_rejected_by_user, 0) as '_rejectedByUser',
                IFNULL(iqp_rejected_by_brand, 0) as '_rejectedByBrand',
                IFNULL(iqp_paid_and_closed_ind, '') as '_paidAndClosedInd',
                IFNULL(iqp_total_raw, 0) as '_rawTotal',
                IFNULL(iqp_discount_perc, 0) as '_discountPer',
                IFNULL(iqp_discount_perc_amnt, 0) as '_discountPerAmt',
                IFNULL(iqp_discount_man, 0) as '_discountManual',
                IFNULL(iqp_adjustment, 0) as '_adjustment',
                IFNULL(iqp_shipping_charge, 0) as '_shippingCharge',
                IFNULL(iqp_VAT_perc, 0) as '_vatPercent',
                IFNULL(iqp_VAT, 0) as '_vatAmt',
                IFNULL(iqp_paid, 0) as '_paidAmt',
                IFNULL(iqp_total_final, 0) as '_totalFinal',
                IFNULL(iqp_discount_perc_ind, 0) as '_discountPerInd',
                IFNULL(iqp_discount_man_ind, 0) as '_discountManualInd',
                IFNULL(iqp_VAT_ind, 0) as '_vatInd',
                IFNULL(tamt.iqp_CGST_perc_amount, 0) as '_CGSTPerAmt',
                IFNULL(tamt.iqp_SGST_perc_amount, 0) as '_SGSTPerAmt',
                IFNULL(tamt.iqp_IGST_perc_amount, 0) as '_IGSTPerAmt',
                IFNULL(tamt.iqp_total_amnt_in_words, '') as '_totalAmtInWords',
                dd_payment_id as '_ddPaymentId',
				dd_created_by_brand_id as '_ddCreatedByBrandId',
				dd_created_by_user_id as '_ddCreatedByUserId',
				dd_iqp_issue_date as '_ddIqpIssueDate',
				dd_iqp_due_date as '_ddIqpDueDate',
				dd_early_pmnt_APR as '_ddEarlyPmntAPR',
				dd_early_pmnt_date as '_ddEarlyPmntDate',
				dd_early_pmnt_applicable_APR as '_ddEarlyPmntApplicableAPR',
				dd_early_pmnt_amnt as '_ddEarlyPmntAmnt',
				dd_iqp_total_amnt as '_ddIqpTotalAmnt',
				dd_iqp_total_amnt_discounted as '_ddIqpTotalAmntDiscounted',
				dd_approved_ind as '_ddApprovedInd',
				dd_approved_by_user_id as '_ddApprovedByuserId',
				dd_approved_by_brand_id as '_ddApprovedByBrandId',
				dd_active_ind as '_ddActiveInd',
				dd_sent_ind as '_ddSentInd',
				dd_sent_to_user_id as '_ddsentToUserId',
				dd_sent_to_brand_id as '_ddSentToBrandId',
				dd_invoice_number as '_ddInvoiceNumber',
				dd_iqp_term as '_ddIqpTerm',
				iqp_line_item_id as 'iqpLineItems:_lineItemId',
				li.iqp_id as 'iqpLineItems:iqpId',
				li.iqp_prod_id as 'iqpLineItems:_prodId',
				li.iqp_unit as 'iqpLineItems:_unit',
				li.iqp_line_total_excl_tax as 'iqpLineItems:_totalExclTax',
				li.iqp_tax as 'iqpLineItems:_tax',
				li.iqp_line_total_incl_tax as 'iqpLineItems:_lineTotalInclTax',
				li.iqp_line_item_active_ind as 'iqpLineItems:_lineitemActiveInd',
				li.iqp_prod_desc as 'iqpLineItems:_prodDesc',
				li.iqp_line_item_discount_amount as 'iqpLineItems:_lineItemDiscountAmount',
				li.iqp_line_item_taxable_amount as 'iqpLineItems:_linetaxableAmount',
				li.iqp_HSN_accounting_cd as 'iqpLineItems:_hsnAccountingCd',
				li.iqp_CGST_ind as 'iqpLineItems:_cgstInd',
				li.iqp_CGST_perc_rate as 'iqpLineItems:_cgstPercRate',
				li.iqp_CGST_perc_amount as 'iqpLineItems:_cgstPercAmount',
				li.iqp_SGST_ind as 'iqpLineItems:_sgstInd',
				li.iqp_SGST_perc_rate as 'iqpLineItems:_sgstPercRate',
				li.iqp_SGST_perc_amount as 'iqpLineItems:_sgstPercAmount',
				li.iqp_IGST_ind as 'iqpLineItems:_igstInd',
				li.iqp_IGST_perc_rate as 'iqpLineItems:_igstPercRate',
				li.iqp_IGST_perc_amount as 'iqpLineItems:_igstPercAmount',
				li.iqp_line_item_ledger_account_code as 'iqpLineItems:_lineItemLedgerAccountCode',
				li.iqp_unit_measure as 'iqpLineItems:_unitMeasure'
                from 
                iqp_det det 
                inner join iqp_status st on det.iqp_id = st.iqp_id
                inner join iqp_line_item li on det.iqp_id = li.iqp_id
                inner join iqp_total_amnt tamt on det.iqp_id = tamt.iqp_id
                left join dynamic_discounting_payment dd on det.iqp_id = dd.dd_iqp_id
                left join brand b on b.brand_id = det.iqp_brand_from_id
                left join user u on u.user_id = det.iqp_user_from_id
                where `;
        this.getIQPDetails(query, param, callback);
    }

    public getIQPDetails(query: string, param: any, callback: any) {
        this._mysqlDao.getObjectByMuitipleCriteria(new iqpModule.IQP(), query, param, false,
            function (result: any, error: any) {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    let iqp: iqpModule.IQP = tree.getData();
                    callback(iqp, error);
                } else {
                    callback(null, error);
                }
            });
    }

    public updateIQPStatus(iqpId: number, brandId: number, indicator: string, callback: any): void {
        let param: any = {
            "iqp_sent_by_brand": null,
            "iqp_approved_by_brand": null,
            "iqp_rejected_by_brand": null,
            "iqp_sent_ind": "",
            "iqp_approved_ind": "",
            "iqp_rejected_ind": ""
        };
        let tableInfo: any = {
            "_tableName": "iqp_status",
            "iqp_id": iqpId,
        };
        param.iqp_approved_ind = (indicator === "A") ? "Y" : "";
        param.iqp_rejected_ind = (indicator === "R") ? "Y" : "";
        param.iqp_approved_by_brand = (indicator === "A") ? brandId : null;
        param.iqp_rejected_by_brand = ((indicator === "R")) ? brandId : null;
        //updateMultiplePropObj
        this._mysqlDao.updateMultiplePropObj(tableInfo, param, function (isUpdated: boolean, error: any) {
            if (error) {
                isUpdated = false;
            } else {
                isUpdated = true;
            }
            callback(isUpdated, error);
        });
    }
}
