/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import imageDaoModule = require("./imageDao");
import imageModule = require("../models/image");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");

export class MySqlImageDao extends imageDaoModule.ImageDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class ImageDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  /**
   * Create a image
   */
  public create(image: imageModule.Image, callback: any): void {
    this._mysqlDao.createObject<imageModule.Image>(image,
      function (id: number, error: any) {
        callback(id, error);
      });
  }

  public createManyProductImages(images: Array<imageModule.Image>, callback: any): void {
    this._mysqlDao.createMany<imageModule.Image>(images,
      function (id: number, error: any) {
        callback(id, error);
      });
  }

  /**
  * Delete a image
  */
  public delete(id: number, callback: any): void {
    this._mysqlDao.deleteObject<imageModule.Image>(new imageModule.Image(), <string><any>id,
      function (isDeleted: boolean, error: any) {
        callback(isDeleted, error);
      });
  }

  /**
  * Get a image
  */
  public get(id: number, callback: any): void {
    this._mysqlDao.getObject(new imageModule.Image(), { prod_img_id: id },
      function (result: any, error: any) {
        if (result.length > 0) {
          let image: imageModule.Image = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), imageModule.Image);
          callback(image, error);
        } else {
          callback(null, error);
        }
      });
  }

  /**
   * Get images for a product
   */
  public getByProduct(productId: number, callback: any): void {
    var mysqlDao = new mysqlDaoModule.MySqlDao();
    mysqlDao.getObject(new imageModule.Image(), { product_id: productId },
      function (result: any, error: any) {
        let images: Array<imageModule.Image> = []; //new imageModule.Image[result.length];
        for (let i: number = 0; i < result.length; i++) {
          let image: imageModule.Image = typedjson.
            TypedJSON.parse(JSON.stringify(result[i]), imageModule.Image);
          images.push(image);
        }
        callback(images, error);
      });
    mysqlDao = null;
  }

}
