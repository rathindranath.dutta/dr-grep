/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import productDaoModule = require("./productDao");
import prodModule = require("../models/product");
import variationModule = require("../models/variation");
import imageModule = require("../models/image");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import imageDaoModule = require("./imageDao");
import mysqlImageDaoModule = require("./mysqlImageDao");
import Treeize = require("treeize");
import async = require("async");

export class MySqlProductDao extends productDaoModule.ProductDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  public _imageDao: imageDaoModule.ImageDao;
  /**
   * Constructor.
   *
   * @class MySqlProductDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  /**
  * List all products
  */
  public list(page: number, callback: any): void { // pagination logic to be implemented
    this._mysqlDao.listObject(new prodModule.Product(),
      function (result: any, error: any) {
        let productDaoObj = new MySqlProductDao();
        let products: Array<prodModule.Product> = []; //new prodModule.Product[result.length]();
        async.eachSeries(result,
          (item, callback: any) => {
            let product: prodModule.Product = typedjson.
              TypedJSON.parse(JSON.stringify(item), prodModule.Product);
            async.parallel([
              (callback: any) => {
                this.getImageByProduct(product.id, function (images: any, error: any) {
                  product.images = images;
                  callback(null, images);
                });
              },
              (callback: any) => {
                this.getVariations(product.id, function (variations: any, error: any) {
                  product.variations = variations;
                  callback(null, variations);
                });
              }
            ], function (err: any, resultArr: any) {
              products.push(product);
              callback();
            });
          }, (err: any) => {
            callback(products, error);
          });
        productDaoObj = null;
      }.bind(this));
  }
  public attachProducts(products: any[], callback: any): void {
    let isSuccessfulUpdation: boolean = false;
    async.eachSeries(products,
      (item, callback: any) => {
        this._mysqlDao.updateObjectWithMultiple(item, function (isUpdated: boolean, error: any) {
          if (error) {
            isUpdated = false;
          } else {
            isUpdated = true;
          }
          callback(error, isUpdated);
        });
      }, (err: any) => {
        isSuccessfulUpdation = true;
        callback(isSuccessfulUpdation, err);
      });
  }
  public getImageByProduct(productId: number, callback: any): void {
    let imageDao = new mysqlImageDaoModule.MySqlImageDao();
    imageDao.getByProduct(productId, function (result: any, error: any) {
      callback(result, error);
    });
    imageDao = null;
  }
  /**
  * Get all product ids by guids --test
  */
  public getProductIdsByGuids(guids: Array<string>, callback: any): void {
    let param = { className: "Product", projection: "product_id, guid", criteria: { guid: guids } };
    this._mysqlDao.listObjectByCriteria(param, (data: any, error: any) => {
      let guidProductIdMap = {};
      data.forEach((record) => {
        guidProductIdMap[record.guid] = record.product_id;
      });
      callback(guidProductIdMap, error);
    });
  }
  /**
  * Get all product ids by upcs and user_id
  */
  getProductIdsByUpcs(upcs: Array<string>, userId: number, callback: any): void {
    let param = { className: "Product", projection: "product_id, guid", criteria: { upc: upcs, user_id: userId } };
    this._mysqlDao.listObjectByCriteria(param, (data: any, error: any) => {
      let guidProductIdMap = {};
      data.forEach((record) => {
        guidProductIdMap[record.guid] = record.product_id;
      });
      callback(guidProductIdMap, error);
    });
  }

  /**
   * Get product(s) by brand
   */
  public getProductsByBrand(brandId: number, callback: any): void {
    var projection: string = `product_id AS '_id', 
                              title AS '_title', 
                              description AS '_desc', 
                              price AS '_price',
                              prod_HSN_cd AS '_hsnCode', 
                              category AS '_category', 
                              upc AS '_upc', 
                              is_new AS '_isNew', 
                              guid AS '_guid',
                              prod_unit_measure AS '_prodUnitMeasure'`;
    let param = { className: "Product", projection: projection, criteria: { brand_id: brandId } };
    console.log("PARAMS" + JSON.stringify(param));
    this._mysqlDao.listObjectByCriteria(param, (data: any, error: any) => {
      let products = [];
      data.forEach((record) => {
        products.push(record);
      });
      callback(products, error);
    });
  }

  /**
   * Get a product details
   */
  public get(id: number, callback: any): void {
    this._mysqlDao.getObject(new prodModule.Product(), { product_id: id },
      function (result: any, error: any) {
        if (result.length > 0) {
          let product: prodModule.Product = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), prodModule.Product);
          callback(product, error);
        } else {
          callback(null, error);
        }
      });
  }

  /**
   * Create a product
   */
  public create(product: prodModule.Product, callback: any): void {
    this._mysqlDao.createObject<prodModule.Product>(product,
      function (id: number, error: any) {
        callback(id, error);
      });
  }

  /**
   * Create many products at a time
   */
  public createMany(products: Array<prodModule.Product>, callback: any): void {
    this._mysqlDao.createMany<prodModule.Product>(products,
      function (id: number, error: any) {
        callback(id, error);
      });
  }

  /**
   * Update a product
   */
  public update(product: prodModule.Product, callback: any): void {
    let jsonProduct = JSON.parse(typedjson.TypedJSON.stringify(product));
    this._mysqlDao.updateObject<prodModule.Product>(new prodModule.Product(), jsonProduct,
      function (isUpdated: boolean, error: any) {
        if (error) {
          isUpdated = false;
        } else {
          isUpdated = true;
        }
        callback(isUpdated, error);
      });
  }

  /**
   * Delete a product
   */
  public delete(id: number, callback: any): void {
    this._mysqlDao.deleteObject<prodModule.Product>(new prodModule.Product(), <string><any>id,
      function (isDeleted: boolean, error: any) {
        if (error) {
          isDeleted = false;
        } else {
          isDeleted = true;
        }
        callback(isDeleted, error);
      });
  }

  /**
   * Get all variations of a product
   */
  public getVariations(id: number, callback: any): void {
    this._mysqlDao.getObject(new variationModule.Variation(), { prod_id: id },
      function (result: any, error: any) {
        let variations: Array<variationModule.Variation> = []; //new variationModule.Variation[result.length];
        for (let i: number = 0; i < result.length; i++) {
          let variation: variationModule.Variation = typedjson.
            TypedJSON.parse(JSON.stringify(result[i]), variationModule.Variation);
          variations.push(variation);
        }
        callback(variations, error);
      });
  }

  /**
   * Get a variation of a product
   */
  public getVariation(id: number, variationId: number, callback: any): void {
    this._mysqlDao.getObject(new variationModule.Variation(), { variation_id: variationId },
      function (result: any, error: any) {
        if (result.length > 0) {
          let variation: variationModule.Variation = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), variationModule.Variation);
          callback(variation, error);
        } else {
          callback(null, error);
        }
      });
  }

  /**
   * Add variations
   */
  public addVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void {
    let ids: Array<number> = [];
    for (let i: number = 0; i < variations.length; i++) {
      this._mysqlDao.createObject<variationModule.Variation>(variations[i],
        function (id: number, error: any) {
          if (!error) {
            ids.push(id);
          }
        });
    }
    callback(ids, null);
  }

  /**
   * Remove variations
   */
  public removeVariations(id: number, variations: Array<variationModule.Variation>, callback: any): void {
    let ids: Array<number> = [];
    for (let i: number = 0; i < variations.length; i++) {
      this._mysqlDao.deleteObject<variationModule.Variation>(new variationModule.Variation(), <string><any>id,
        function (isDeleted: boolean, error: any) {
          if (isDeleted) {
            ids.push(id);
          }
        });
    }
    callback(ids, null);
  }

  /**
   * Update variations
   */
  public updateVariations(variations: Array<variationModule.Variation>, callback: any): void {
    let ids: Array<number> = [];
    for (let i: number = 0; i < variations.length; i++) {
      let jsonVariation = JSON.parse(typedjson.TypedJSON.stringify(variations[i]));
      this._mysqlDao.updateObject<variationModule.Variation>(new variationModule.Variation(), jsonVariation,
        function (isUpdated: boolean, error: any) {
          if (isUpdated) {
            ids.push(variations[i].id);
          }
        });
    }
    callback(ids, null);
  }
  public getProductsByUserEmail(userEmail: string, callback: any): void {
    let param: any = { user_email: userEmail };
    let query: string = `select p.product_id as '_id', p.user_id as
    '_userId', p.brand_id as '_brandId', p.title as '_title', 
    p.description as '_desc',p.price as '_price', 
    p.category as '_category', p.upc as '_upc', 
    p.guid as '_guid', p.is_new as '_isNew', p.add_time as 
    '_addTime', p.latitude as '_lat', p.longitude as '_long', 
    p.update_time as '_updateTime', p.is_active as '_isActive',
    p.prod_HSN_cd AS _hsnCode,
    p.prod_weight AS _weight,
    p.prod_benefits AS _benefits,
    p.prod_Ingredients AS _meterial,
    p.prod_msrp AS _msrp,
    p.prod_unit_measure AS _prodUnitMeasure,
    p.prod_purchase_price AS _prodPurchasePrice,
    p.prod_discount AS _prodDiscount,
    p.prod_margin_perc AS _prodMarginPerc,
    p.prod_brand_name AS _brandName,
    p.prod_batch_no AS _batchNo,
    p.prod_bar_code AS _barCode,
    p.prod_warning AS _warning,
    p.prod_how_to_use AS _howToUse,
    p.item_ledger_account_cogs_code AS _ledgerAcctCogsCode,
    p.item_ledger_account_inventory_code AS _ledgerAcctInventoryCode,
    p.item_ledger_account_purchase_code AS _ledgerAcctPurchaseCode,
    p.item_ledger_account_sales_code AS _ledgerAcctSalesCode,
    p.purchase_unit_rate AS _purchaseUnitRate,
    p.prod_type AS _prodType, 
    IFNULL(i.prod_img_id, "") as '_images:_id', IFNULL(i.product_id, "") as '_images:_productId',
    IFNULL(i.prod_img_link,"") as '_images:_link' from
    product p left join prod_img i on p.product_id = i.product_id
    inner join user u on u.user_id = p.user_id where u.user_email_id`;
    this.getProducts(query, param, callback);
  }

  private getProducts(query: string, param: any, callback: any): void {
    this._mysqlDao.getComplexObject(new prodModule.Product(), query, param,
      function (result: any, error: any) {
        if (result.length > 0) {
          let tree = new Treeize();
          tree.grow(result);
          let products: prodModule.Product[] = tree.getData();
          callback(products, error);
        } else {
          callback(null, error);
        }
      });
  }
}
