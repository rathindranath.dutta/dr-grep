/// <reference path="../_all.d.ts" />
"use strict";

import userModule = require("../models/user");

export abstract class ReadUserDao {


  public abstract getUserByEmail(emailId: string, callback: any): void;

}