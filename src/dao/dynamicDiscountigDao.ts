/// <reference path="../_all.d.ts" />
"use strict";

export abstract class DynamicDiscountngDao {

    /**
     * Make early payment
     */
    public abstract earlyPayment(dynamicDiscounting: any, callback: any): void;

    public abstract actionOnDiscount(dynamicDiscounting: any, callback: any): void;

}
