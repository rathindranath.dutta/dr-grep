/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import iqpDaoModule = require("./arapDao");
import stockDaoModule = require("./stockDao");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import async = require("async");

export class MySqlStockDao extends stockDaoModule.StockDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /** 
     * get all stock Details for a brand
    */
    public getStockDetails(brandId: number, callback: any): void {
        let spName: string = "sp_getStock";
        let params: Array<number> = [brandId];
        this._mysqlDao.execSelectSP(spName, params, callback);
    }

    public getInOutStockByProduct(productId: number, callback: any): void {
        let stock: any = { "_inStock": [], "_outStock": [] };
        let param: any = { "productId": productId };
        let outStockQuery: string = `SELECT
                            IFNULL(DATE_FORMAT(iqp_issue_date,'%d/%m/%Y'),'') AS '_stockIssuedate',
                            ide.iqp_invoice_number AS '_iqpInvoiceNumber',
                            sum(iqp_unit) AS '_totalIssuedStock'
                            FROM iqp_line_item ilt 
                            INNER JOIN iqp_status ist ON ilt.iqp_id=ist.iqp_id
                            INNER JOIN iqp_det ide ON ide.iqp_id=ilt.iqp_id
                            INNER JOIN brand b ON b.brand_id = ide.iqp_brand_from_id
                            INNER JOIN product p ON ilt.iqp_prod_id = p.product_id
                            WHERE p.product_id = ?
                            GROUP BY iqp_issue_date
                            ORDER BY ide.iqp_issue_date DESC;`;
        let inStockQuery: string = `SELECT
                            stock_id AS '_stockId', 
                            stock_prod_id AS '_productId',
                            stock_quantity AS '_stockQuantity',
                            IFNULL(DATE_FORMAT(stock_entry_date,'%d/%m/%Y'),'') AS '_stockEntryDate',
                            IFNULL(stock_memo,'') AS '_stockMemo',
                            IFNULL(stock_uom,'') AS '_stockUom',
                            IFNULL(stock_total_value,0) AS '_totalStockvalue',
                            IFNULL(stock_unit_purchase_price,0) AS '_stockUnitPurchasePrice'
                            FROM stock WHERE stock_prod_id = ?
                            ORDER BY stock_entry_date DESC;`;
        async.parallel([
            (callback: any) => {
                this.getInOutStockDetails(inStockQuery, param, function (inStock: any, error: any) {
                    stock._inStock = inStock;
                    callback(null, inStock);
                });
            },
            (callback: any) => {
                this.getInOutStockDetails(outStockQuery, param, function (outStock: any, error: any) {
                    stock._outStock = outStock;
                    callback(null, outStock);
                });
            }
        ], function (err: any, stock: any) {
            callback(stock, err);
        });
    }

    private getInOutStockDetails(query: string, param: any, callback: any): void {
        let stockDetails: any;
        this._mysqlDao.getObjectByQuery(stockDetails, query, param,
            function (result: any, error: any) {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    stockDetails = tree.getData();
                    callback(stockDetails, error);
                } else {
                    callback(null, error);
                }
            });
    }

    public addStock(stock: any, isProductAddtoStock: number, callback: any): void {
        let isCreated: boolean = false;
        let stockObj: any[] = this.prepareStockObj(stock);
        this._mysqlDao.createMany(stockObj, (id: number, error: any) => {
            isCreated = true;
            if (error === null && isProductAddtoStock === 1) {
                let tableInfo: any = {
                    "_tableName": "iqp_line_item",
                    "iqp_line_item_id": stock["_lineItemId"]
                };
                let iqpObj: any[] = [{
                    "iqp_line_item_added_to_stock_ind": 1
                }];
                this._mysqlDao.updateMultiplePropObj(tableInfo, iqpObj[0], (id: number, error: any) => {
                    callback(isCreated, error);
                });
            } else {
                callback(isCreated, error);
            }
        });
    }

    public updateStock(stock: any, callback: any): void {
        let stockObj = this.prepareStockObj(stock);
        delete stockObj[0]["_tableName"];
        let isModified: boolean = false;
        let tableInfo: any = {
            "_tableName": "stock",
            "stock_id": stock["_stockId"]
        };
        this._mysqlDao.updateMultiplePropObj(tableInfo, stockObj[0], (id: number, error: any) => {
            isModified = true;
            callback(isModified, error);
        });
    }
    private prepareStockObj(stock: any): any[] {
        let stockObj: any[] = [{
            "_tableName": "stock",
            "stock_prod_id": stock["_productId"],
            "stock_quantity": stock["_stockQuantity"],
            "stock_unit_purchase_price": stock["_stockUnitPurchasePrice"],
            "stock_entry_date": stock["_stockEntryDate"],
            "stock_memo": stock["_stockMemo"],
            "stock_uom": stock["_stockUom"],
            "stock_total_value": stock["_totalStockvalue"]
        }];
        return stockObj;
    }
}