/// <reference path="../_all.d.ts" />
"use strict";

import userModule = require("../models/user");

export abstract class UpdateUserDao {


  public abstract saveUser(user: userModule.User, callback: any): void;

  public abstract getUserByEmail(emailId: string, callback: any): void;

}