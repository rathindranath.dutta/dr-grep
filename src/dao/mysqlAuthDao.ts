/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import authDaoModule = require("./authDao");
import configModule = require("./databaseConfig");
import tokenModule = require("../models/token");
import userModule = require("../models/user");
import typedjson = require("typedjson");
import Treeize = require("treeize");

export class MySqlAuthDao extends authDaoModule.AuthDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class StoreDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  public getTokenInfo(tokenId: string, callback: any): void {
    let param: any = { token_id: tokenId };
    this.getToken(param, callback);
  }

  public getTokenInfoByRefreshId(refreshId: string, callback: any): void {
    let param: any = { refresh_token: refreshId };
    this.getToken(param, callback);
  }

  public saveUser(user: userModule.User, callback: any): void {
    this._mysqlDao.createObject<userModule.User>(user,
      function (insertedId: number, error: any) {
        user.id = insertedId;
        callback(user, error);
      });
  }

  public saveToken(token: tokenModule.Token, callback: any): void {
    this._mysqlDao.createObject<tokenModule.Token>(token,
      function (insertedId: string, error: any) {
        callback(token, error);
      });
  }

  public updateToken(token: tokenModule.Token, callback: any): void {
    let jsonToken = JSON.parse(typedjson.TypedJSON.stringify(token));
    delete jsonToken.user_id;
    this._mysqlDao.updateObject<tokenModule.Token>(new tokenModule.Token(), jsonToken,
      function (isUpdated: boolean, error: any) {
        callback(isUpdated, error);
      });
  }

  public updateUser(user: userModule.User, callback: any): void {
    let jsonUser = JSON.parse(typedjson.TypedJSON.stringify(user));
    this._mysqlDao.updateObject<userModule.User>(new userModule.User(), jsonUser,
      function (isUpdated: boolean, error: any) {
        callback(isUpdated, error);
      });
  }

  public deleteToken(tokenId: string, callback: any): void {
    this._mysqlDao.deleteObject<tokenModule.Token>(new tokenModule.Token(), tokenId,
      function (isDeleted: boolean, error: any) {
        callback(isDeleted, error);
      });
  }

  public getUserById(id: string, callback: any): void {
    let param: any = { user_id: id };
    this.getUser(param, callback);
  }

  public getUserByEmail(emailId: string, callback: any): void {
    let param: any = { user_email_id: emailId };
    let query: string = `select u.user_id as '_id', u.user_email_id as '_email', u.user_name as '_name', 
                        ifNull(b.brand_id, -1) as '_brandId', u.password as password, u.salt as salt,
                        u.user_is_active as active
                        from user u left outer join brand b on u.user_id = b.user_id
                        where u.user_email_id`;
    this._mysqlDao.getComplexObject(new userModule.User(), query, param,
      function (result: any, error: any) {
        if (result.length > 0) {
          let tree = new Treeize();
          tree.grow(result);
          let user: userModule.User = tree.getData();
          console.log("user data: " + JSON.stringify(user[0]._brandId));
          callback(user[0], error);
        } else {
          callback(null, error);
        }
      });
  }

  public getUserByToken(token: string, callback: any): void {
    let param: any = { token_id: token };
    this.getToken(param, function (tokenObj: tokenModule.Token, error: any) {
      let userParam: any = { user_id: tokenObj.userId };
      if (tokenObj == null) {
        callback(null, error);
      } else {
        this.getUser(userParam, callback);
      }
    });
  }

  private getToken(param: any, callback: any) {
    this._mysqlDao.getObject(new tokenModule.Token(), param,
      function (result: any, error: any) {
        if (result.length > 0) {
          let token: tokenModule.Token = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), tokenModule.Token);
          callback(token, error);
        } else {
          callback(null, error);
        }
      });
  }

  private getUser(param: any, callback: any) {
    this._mysqlDao.getObject(new userModule.User(), param,
      function (result: any, error: any) {
        if (result.length > 0) {
          let user: userModule.User = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), userModule.User);
          callback(user, error);
        } else {
          callback(null, error);
        }
      });
  }

}