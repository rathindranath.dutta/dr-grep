/// <reference path="../_all.d.ts" />
"use strict";

export abstract class StockDao {

    /**
     * get Stock Details
     */
    public abstract getStockDetails(brandId: number, callback: any): void;

    // /**
    //  * Get in/out stock details for a product
    //  */
    public abstract getInOutStockByProduct(productId: number, callback: any): void;

    /**
     * Add Stock for a product
     */
    public abstract addStock(stock: any, isProductAddtoStock: number, callback: any): void;

    /**
     * Update Stock for a product  updateStock
     */
    public abstract updateStock(stock: any, callback: any): void;
}
