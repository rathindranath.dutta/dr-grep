/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import readUserDaoModule = require("./readUserDao");
import configModule = require("./databaseConfig");
import userModule = require("../models/user");
import typedjson = require("typedjson");

export class MySqlReadUserDao extends readUserDaoModule.ReadUserDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class MySqlReadUserDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }


  public getUserByEmail(emailId: string, callback: any): void {
    let param: any = { user_email_id: emailId };
    this._mysqlDao.getObject(new userModule.User(), param,
      function (result: any, error: any) {
        if (result.length > 0) {
          let user: userModule.User = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), userModule.User);
          callback(user, error);
        } else {
          callback(null, error);
        }
      });
  }

}