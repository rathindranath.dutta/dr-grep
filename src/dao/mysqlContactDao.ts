/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import contactDaoModule = require("./contactDao");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import contactModule = require("../models/contact");

export class MySqlContactDao extends contactDaoModule.ContactDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class MySqlProductDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  /**
  * create a contact
  */

  public create(contact: contactModule.Contact, callback: any): void {
    // this._mysqlDao.createObject<contactModule.Contact>(contact,
    //   function (id: number, error: any) {
    //     callback(id, error);
    //   });
    let isCreated: boolean = true;
    let contactObj = this.prepareContactObj(contact);
    this._mysqlDao.createMany(contactObj, (id: number, error: any) => {
      isCreated = true;
      callback(error, isCreated);
    });
  }

  /**
  * update a contact
  */

  public update(contact: contactModule.Contact, callback: any): void {
    //let jsonContact = JSON.parse(typedjson.TypedJSON.stringify(contact));
    // this._mysqlDao.updateObject<contactModule.Contact>(new contactModule.Contact,
    //   jsonContact, function (id: number, error: any) {
    //     callback(id, error);
    //   });
    let contactObj = this.prepareContactObj(contact);
    delete contactObj[0]["_tableName"];
    let isModified: boolean = false;
    let tableInfo: any = {
      "_tableName": "contact",
      "contact_id": contact["_contactId"]
    };
    this._mysqlDao.updateMultiplePropObj(tableInfo, contactObj[0], (id: number, error: any) => {
      isModified = true;
      callback(null, isModified);
    });
  }

  private prepareContactObj(contact: contactModule.Contact): contactModule.Contact[] {
    let contactObj: any[] = [{
      "_tableName": "contact",
      "contact_first_name": contact["_contactFirstName"],
      "contact_last_name": contact["_contactLastName"],
      "contact_phone_number": contact["_contactPhoneNumber"],
      "contact_address": contact["_contactAddress"],
      "contact_city": contact["_contactContactCity"],
      "contact_zip_cd": contact["_contactZipCode"],
      "contact_state": contact["_contactState"],
      "is_contact_from_shop": contact["_contactIsFromShop"],
      "is_contact_from_website": contact["_contactIsFromWeb"],
      "is_contact_from_drgrep": contact["_contactIsFromDrgrep"],
      "shop_id": contact["_contactShopId"],
      "contact_add_timestamp": contact["_contactAddTimeStamp"],
      "contact_email_address": contact["_contactEmailAddress"],
      "user_id": contact["_contactUserId"],
      "brand_id": contact["_contactBrandId"],
      "is_a_team_mamber_ind": contact["_contactIsTeamMemberInd"],
      "contact_user_id": contact["_contactContactUserId"],
      "is_invitation_sent": contact["_contactIsInvitationSent"],
      "is_a_customer": contact["_contactIsCustomer"],
      "contact_note": contact["_contactNote"],
      "contact_lat": contact["_contactLat"],
      "contact_long": contact["_contactLong"],
      "contact_addr_line1": contact["_contactAddrLine1"],
      "contact_addr_line2": contact["_contactAddrLine2"],
      "contact_country": contact["_contactCountry"],
      "contact_GSTIN": contact["_contactGstin"],
      "contact_state_cd": contact["_contactStateCode"],
      "contact_business_name": contact["_contactBusinessName"],
      "contact_shipping_info_as_billing_ind": contact["_contactShippingBilingInd"],
      "contact_shipping_to_fullname": contact["_contactShippingToFullName"],
      "contact_shipping_to_email": contact["_contactShippingToEmail"],
      "contact_shipping_address_line1": contact["_contactShippingAddressLine1"],
      "contact_shipping_address_line2": contact["_contactShippingAddressLine2"],
      "contact_shipping_city": contact["_contactShippingCity"],
      "contact_shipping_state": contact["_contactShippingState"],
      "contact_shipping_zip_cd": contact["_contactShippingZipCode"],
      "contact_shipping_country": contact["_contactShippingCountry"]
    }];
    return contactObj;
  }

}
