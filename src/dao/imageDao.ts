/// <reference path="../_all.d.ts" />
"use strict";

import imageModule = require("../models/image");


export abstract class ImageDao {

  /**
   * Create a image
   */
  public abstract create(image: imageModule.Image, callback: any): void;

  /**
  * create multiple product images
  */
  public abstract createManyProductImages(images: Array<imageModule.Image>, callback: any): void;

  /**
   * Delete a image
   */
  public abstract delete(id: number, callback: any): void;

  /**
   * Get a image details
   */
  public abstract get(id: number, callback: any): void;

  /**
   * Get images for a product
   */
  public abstract getByProduct(productId: number, callback: any): void;

}
