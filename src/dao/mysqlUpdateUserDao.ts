/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import updateUserDaoModule = require("./updateUserDao");
import configModule = require("./databaseConfig");
import userModule = require("../models/user");
import typedjson = require("typedjson");

export class MySqlUpdateUserDao extends updateUserDaoModule.UpdateUserDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class MySqlUpdateUserDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

  public saveUser(user: userModule.User, callback: any): void {
    this._mysqlDao.createObject<userModule.User>(user,
      function(insertedId: number, error: any){
      user.id = insertedId;
      callback(user, error);
    });
  }


  public getUserByEmail(emailId: string, callback: any): void {
    let param: any = {user_email_id: emailId};
    this._mysqlDao.getObject(new userModule.User(), param,
      function(result: any, error: any){
        if (result.length > 0) {
          let user: userModule.User = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), userModule.User);
          callback(user, error);
        } else {
          callback(null, error);
        }
    });
  }

}