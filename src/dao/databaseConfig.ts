/// <reference path="../_all.d.ts" />
"use strict";

/**
 * Database Configuration
 */
export interface DatabaseConfig {
  host?: string;
  port?: number;
  database?: string;
  user?: string;
  password?: string;
  poolSize?: number;
}
