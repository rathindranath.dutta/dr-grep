/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import teamDaoModule = require("./teamDao");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import async = require("async");
import teamModule = require("../models/team");
import userModule = require("../models/user");

export class MySqlTeamDao extends teamDaoModule.TeamDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlProductDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /** 
     * get all stock Details for a brand
    */
    public addUserToTeam(team: teamModule.Team, callback: any) {
        this._mysqlDao.createObject<teamModule.Team>(team,
            function (insertedId: number, error: any) {
                callback(insertedId, error);
            });
    }

    public getTeamMembers(brandId: number, callback: any): void {
        let param: any = { "brand_id": brandId };
        let query: string = `select u.user_id as '_id', u.user_email_id as '_email', u.user_name as '_name',
                        u.user_is_active as _active
                        from user u inner join brand_team_profile t on t.user_id = u.user_id
                        where t.brand_id`;
        this._mysqlDao.getComplexObject(new userModule.User(), query, param,
            function (result: any, error: any) {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    let users: userModule.User[] = tree.getData();
                    callback(users, error);
                } else {
                    callback(null, error);
                }
            });
    }

    public insertRule(ruleInfo: any, callback: any): void {
        let ruleObject: any = {};
        let ruleObjArray: any[] = [];
        let isCreated: boolean = false;
        ruleInfo.forEach(rule => {
            ruleObject = {
                "_tableName": "work_flow_rule_master",
                "wfrm_brand_id": rule["brandId"],
                "wfrm_entity_type": rule["entityType"],
                "wfrm_activity_role": rule["activityType"],
                "wfrm_user_id": rule["userId"]
            };
            ruleObjArray.push(ruleObject);
        });
        this._mysqlDao.createMany(ruleObjArray, (id: number, error: any) => {
            isCreated = true;
            callback(isCreated, error);
        });
    }

    public getRuleSets(brandId: number, callback: any): void {
        let param: any = { "brand_id": brandId };
        let query: string = `select 
                            w.wfrm_entity_type as 'entityType',
                            w.wfrm_activity_role as 'activityType',
                            w.wfrm_brand_id as 'brandId',
                            w.wfrm_user_id as 'users:_id',
                            u.user_email_id as 'users:_email',
                            u.user_name as 'users:_name',
                            u.user_is_active as 'users:_active'
                            From work_flow_rule_master w
                            inner join user u on w.wfrm_user_id = u.user_id
                            where w.wfrm_brand_id`;
        this._mysqlDao.getComplexObject(new teamModule.Team(), query, param,
            (result: any, error: any) => {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    let ruleSets: any[] = tree.getData();
                    callback(ruleSets, error);
                } else {
                    callback(null, error);
                }
            });
    }
}