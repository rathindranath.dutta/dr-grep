/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import workflowDaoModule = require("./workflowDao");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");
import Treeize = require("treeize");
import async = require("async");
import userModule = require("../models/user");

export class MySqlWorkflowDao extends workflowDaoModule.WorkflowDao {

    private _mysqlDao: mysqlDaoModule.MySqlDao;
    /**
     * Constructor.
     *
     * @class MySqlWorkflowDao
     * @constructor
     */
    constructor() {
        super();
        this._mysqlDao = new mysqlDaoModule.MySqlDao();
    }

    /** 
     * get all stock Details for a brand
    */
    public insertUpdateWorkflowFeedData(action: string, workflowFeed: any, callback: any): void {
        let workflowFeedObjArray: any[] = [];
        let workflowFeedObj: any = {};
        let users: any[] = [];
        let isActionCompleted: boolean = false;
        let activityCodeCriteria: string = "";
        let activityCode: string = "";
        switch (action) {
            case "SUBMIT":
                //workflowFeedObj = this.prepareWorkflowObj(workflowFeed, "Submitted", 1);
                workflowFeedObjArray = workflowFeed;
                this._mysqlDao.createMany(workflowFeedObjArray,
                    (id: number, error: any) => {
                        if (error === null) {
                            this.prepareNextAction(workflowFeedObjArray[0], "Approved", 0, (error: any, isActionCompleted: boolean) => {
                                callback(error, isActionCompleted);
                            });
                        }
                    });
                break;
            case "APPROVE":
            case "PAID":
            case "REJECT":
            case "REQUESTED":
                if (action === "REJECT" || action === "REQUESTED") {
                    //Search for entities where activity = "APPROVE" with closeInd = 0
                    activityCodeCriteria = "Approved";
                    activityCode = "Rejected";
                } else {
                    activityCodeCriteria = activityCode = workflowFeed["activity_type_code"];
                }
                let update1tableInfo: any = {
                    "_tableName": "work_flow_feed",
                    "entity_number": workflowFeed["entity_number"],
                    "activity_type_code": activityCodeCriteria,
                    "activity_close_ind": "0"
                };
                let paramUpdate1: any = {
                    "activity_close_ind": "1",
                    "activity_type_code": activityCode,
                };
                //Set the closeInd = 1 for all the rows of specified activity to indicate activity is completed
                this._mysqlDao.updateMultiplePropObj(update1tableInfo, paramUpdate1, (id: number, error: any) => {
                    if (error === null) {
                        //update iqpStatus

                        let update2TableInfo: any = {
                            "_tableName": "work_flow_feed",
                            "entity_number": workflowFeed["entity_number"],
                            "activity_type_code": activityCode,
                            "to_brand_id": workflowFeed["to_brand_id"],
                            "destination_user_id": global.currentUserId
                        };
                        let currentDateTime: Date = new Date();
                        currentDateTime.setMinutes(currentDateTime.getMinutes());
                        let paramUpdate2: any = {
                            "close_time": currentDateTime
                        };
                        //Set the closingTime to indicate who has completed the activity
                        this._mysqlDao.updateMultiplePropObj(update2TableInfo, paramUpdate2, (id: number, error: any) => {
                            if (error === null) {
                                if (action === "APPROVE") {
                                    this.prepareNextAction(workflowFeed, "Paid", 0, (error: any, isActionCompleted: boolean) => {
                                        //callback(error, isActionCompleted);
                                        this.updateIqpStatus(action, workflowFeed, (error: any, isCompleted: boolean) => {
                                            callback(isCompleted, error);
                                        });
                                    });
                                } else if (action === "REQUESTED") {
                                    workflowFeedObj = this.prepareWorkflowObj(workflowFeed, "Requested", 1);
                                    workflowFeedObjArray.push(workflowFeedObj);
                                    this._mysqlDao.createMany(workflowFeedObjArray, (id: number, error: any) => {
                                        callback(error, true);
                                    });
                                } else {
                                    //callback(error, true);
                                    this.updateIqpStatus(action, workflowFeed, (error: any, isCompleted: boolean) => {
                                        callback(isCompleted, error);
                                    });
                                }
                            } else {
                                callback(false, error);
                            }
                        });
                        update2TableInfo = paramUpdate2 = null;
                    } else {
                        callback(false, error);
                    }
                });
                break;
            case "DELEGATED":
                workflowFeedObj = this.prepareWorkflowObj(workflowFeed, "Delegated", 1);
                workflowFeedObjArray.push(workflowFeedObj);
                this._mysqlDao.createMany(workflowFeedObjArray, (id: number, error: any) => {
                    callback(error, true);
                });
                break;
        }
    }
    private prepareWorkflowObj(workflowFeed: any, activityTypeCode: string, closeInd: number): any {
        let workflowFeedObj: any = {
            "_tableName": "work_flow_feed",
            "source_user_id": workflowFeed["source_user_id"],
            "destination_user_id": workflowFeed["destination_user_id"],
            "from_brand_id": workflowFeed["from_brand_id"],
            "to_brand_id": workflowFeed["to_brand_id"],
            "entity_type_code": workflowFeed["entity_type_code"],
            "entity_number": workflowFeed["entity_number"],
            "activity_type_code": activityTypeCode,
            "activity_message": workflowFeed["activity_message"],
            "activity_close_ind": closeInd,
            "open_time": workflowFeed["open_time"]
            //close_time is not present at the time of inserting, it will only update later
        };
        return workflowFeedObj;
    }

    private prepareNextAction(workflowFeed: any, activityTypeCode: string, closeInd: number, callback: any) {
        let users: any[] = [];
        let workflowFeedObjArray: any[] = [];
        let isInserted: boolean = false;
        let param: any = {
            "wfrm_brand_id": workflowFeed["to_brand_id"],
            "wfrm_entity_type": workflowFeed["entity_type_code"],
            "wfrm_activity_role1": activityTypeCode,
            "wfrm_activity_role2": "All"
        };
        let query: string = `select wfrm_user_id from work_flow_rule_master where 
                             wfrm_brand_id = ? and 
                             wfrm_entity_type = ? and
                             wfrm_activity_role in (?,?)`;

        this._mysqlDao.getObjectByQueryBuilder(new userModule.User(), query, param,
            (result: any, error: any) => {
                if (result.length > 0) {
                    users = result;
                    users.forEach(element => {
                        workflowFeed["destination_user_id"] = element["wfrm_user_id"];
                        let workflowFeedObj: any = this.prepareWorkflowObj(workflowFeed, activityTypeCode, closeInd);
                        workflowFeedObjArray.push(workflowFeedObj);
                    });
                    if (workflowFeedObjArray !== undefined && workflowFeedObjArray.length > 0) {
                        this._mysqlDao.createMany(workflowFeedObjArray, (id: number, error: any) => {
                            isInserted = (error === null) ? true : false;
                            callback(error, isInserted);
                        });
                    }
                } else {
                    callback(error, isInserted);
                }
            });
    }

    private updateIqpStatus(action: string, workflowFeed: any, callback: any) {
        if (workflowFeed["entity_type_code"] === "Invoice" || workflowFeed["entity_type_code"] === "PO") {
            let updateIqpStatusInfo = {
                "_tableName": "iqp_status",
                "iqp_id": workflowFeed["entity_number"]
            };
            let paramIqpStatus: any = {
                "iqp_id": workflowFeed["entity_number"],
            };
            paramIqpStatus["iqp_approved_ind"] = (action === "APPROVE") ? 1 : null;
            paramIqpStatus["iqp_approved_by_user"] = (action === "APPROVE") ? workflowFeed["destination_user_id"] : null;
            paramIqpStatus["iqp_approved_by_brand"] = (action === "APPROVE") ? workflowFeed["to_brand_id"] : null;
            paramIqpStatus["iqp_rejected_ind"] = (action === "REJECT") ? 1 : null;
            paramIqpStatus["iqp_rejected_by_user"] = (action === "REJECT") ? workflowFeed["destination_user_id"] : null;
            paramIqpStatus["iqp_rejected_by_brand"] = (action === "REJECT") ? workflowFeed["to_brand_id"] : null;
            paramIqpStatus["iqp_paid_and_closed_ind"] = (action === "PAID") ? 1 : null;
            this._mysqlDao.updateMultiplePropObj(updateIqpStatusInfo, paramIqpStatus, (id: number, error: any) => {
                let success: boolean = (!error) ? true : false;
                callback(error, success);
            });
        }
    }

    public getToDoList(brandId: number, userId: number, callback: any): any {
        let toDoListObj: any[] = [];
        let users: any[] = [];
        let isAdmin: boolean = false;
        let query1: string = `select user_id from brand where`;
        let param: any = {
            "wff.to_brand_id": brandId,
            "wff.destination_user_id": userId,
            "wff.activity_close_ind": 0
        };
        let query: string = `SELECT
                    wff.source_user_id,
                    wff.destination_user_id, 
                    wff.entity_number,
                    wff.from_brand_id,
                    wff.to_brand_id,
                    wff.entity_type_code,
                    wff.activity_type_code,
                    wff.activity_message,
                    wff.open_time,
                    b.brand_name,
                    IFNULL(det.iqp_invoice_number,"") AS 'iqp_invoice_number'
                    FROM work_flow_feed wff 
                    LEFT JOIN iqp_det det ON wff.entity_number = det.iqp_id
                    LEFT JOIN brand b ON b.brand_id = wff.from_brand_id
                    WHERE `;
        this._mysqlDao.getObjectByMuitipleCriteria(toDoListObj, query, param, false,
            function (result: any, error: any) {
                if (result.length > 0) {
                    let tree = new Treeize();
                    tree.grow(result);
                    toDoListObj = tree.getData();
                    callback(toDoListObj, error);
                } else {
                    callback(null, error);
                }
            });
    }

}