/// <reference path="../_all.d.ts" />
"use strict";

import mysqlDaoModule = require("./mysqlDao");
import storeDaoModule = require("./storeDao");
import storeModule = require("../models/store");
import configModule = require("./databaseConfig");
import typedjson = require("typedjson");

export class MySqlStoreDao extends storeDaoModule.StoreDao {

  private _mysqlDao: mysqlDaoModule.MySqlDao;
  /**
   * Constructor.
   *
   * @class MySqlStoreDao
   * @constructor
   */
  constructor() {
    super();
    this._mysqlDao = new mysqlDaoModule.MySqlDao();
  }

 /**
  * List all stores
  */
  public list(callback: any): void {
    this._mysqlDao.listObject(new storeModule.Store(),
      function(result: any, error: any){
        let stores: Array<storeModule.Store> = new storeModule.Store[result.length];
        for (let i: number = 0; i < result.length; i++) {
          let store: storeModule.Store = typedjson.
            TypedJSON.parse(JSON.stringify(result[i]), storeModule.Store);
          stores.push(store);
        }
        callback(stores, error);
    });
  }

 /**
  * Get a store details
  */
  public get(id: number, callback: any): void {
    this._mysqlDao.getObject(new storeModule.Store(), {shop_id: id},
      function(result: any, error: any){
        if (result.length > 0) {
          let store: storeModule.Store = typedjson.
            TypedJSON.parse(JSON.stringify(result[0]), storeModule.Store);
          callback(store, error);
        } else {
          callback(null, error);
        }
    });
  }

 /**
  * Create a store
  */
  public create(store: storeModule.Store, callback: any): void {
    this._mysqlDao.createObject<storeModule.Store>(store,
      function(id: number, error: any){
      callback(id, error);
    });
  }

 /**
  * Update a store
  */
  public update(store: storeModule.Store, callback: any): void {
    let jsonStore = JSON.parse(typedjson.TypedJSON.stringify(store));
    this._mysqlDao.updateObject<storeModule.Store>(new storeModule.Store(), jsonStore,
      function(isUpdated: boolean, error: any){
      callback(isUpdated, error);
    });
  }

 /**
  * Delete a store
  */
  public delete(id: number, callback: any): void {
    this._mysqlDao.deleteObject<storeModule.Store>(new storeModule.Store(), <string><any>id,
      function(isDeleted: boolean, error: any){
      callback(isDeleted, error);
    });
  }

}
