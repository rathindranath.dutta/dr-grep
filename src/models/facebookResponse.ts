/// <reference path="../_all.d.ts" />
"use strict";

import * as response from "../models/response";

/**
 * Response interface
 */
export class FacebookResponse extends response.Response {
  public data: string;

  public isValid(): boolean {
	let validParam = "is_valid";
    if (this.data != null && this.data[validParam] === true) {
      return true;
    }
    return false;
  }
}
