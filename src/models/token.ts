/// <reference path="../_all.d.ts" />
"use strict";

import userModule = require("./user");
import typedjson = require("typedjson");

/**
 * Token model
 */
@typedjson.JsonObject
export class Token {

  @typedjson.JsonMember({ type: String, name: "token_id" })
  private _id: string;

  @typedjson.JsonMember({ type: String, name: "refresh_token" })
  private _refreshToken: string;

  @typedjson.JsonMember({ type: Date, name: "expires_on" })
  private _expiresOn: Date;

  private _user: userModule.User;

  @typedjson.JsonMember({ type: Number, name: "user_id"})
  private _userId: number;

  /**
  *  Instead of constructor we need to have a properties setter
  *  since typedjson does not work with a constructor
  */
  public setProperties(id: string, refreshToken: string, expiresOn: Date, user: userModule.User) {
    this._id = id;
    this._refreshToken = refreshToken;
    this._expiresOn = expiresOn || new Date();
    this._user = user;
    this._userId = user["_id"];
  }

  public get id() : string {
    return this._id;
  }

  public get refreshToken() : string {
    return this._refreshToken;
  }

  public get expiresOn() : Date {
    return this._expiresOn;
  }

  public set expiresOn(newExpiry: Date) {
    this._expiresOn = newExpiry;
  }

  public get user() : userModule.User {
    return this._user;
  }

  public get userId() : Number {
    return this._userId;
  }
}
