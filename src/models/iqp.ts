/// <reference path="../_all.d.ts" />

import typedjson = require("typedjson");
/**
* Brand model
*/
@typedjson.JsonObject
export class IQP {

    @typedjson.JsonMember({ type: String, name: "brand_name" })
    private _fromBrandName: string;

    @typedjson.JsonMember({ type: String, name: "user_name" })
    private _fromUserName: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_id" })
    private _iqpId: number;

    @typedjson.JsonMember({ type: String, name: "iqp_type" })
    private _type: string;

    @typedjson.JsonMember({ type: String, name: "iqp_invoice_number" })
    private _invoiceNumber: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_brand_from_id" })
    private _brandFromId: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_brand_to_id" })
    private _brandToId: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_user_from_id" })
    private _userFromId: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_user_to_id" })
    private _userToId: number;

    @typedjson.JsonMember({ type: String, name: "iqp_contact_to_email_address" })
    private _contactToEmailAddress: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_contact_to_id" })
    private _contactToId: number;

    @typedjson.JsonMember({ type: Date, name: "iqp_issue_date" })
    private _issueDate: Date;

    @typedjson.JsonMember({ type: Number, name: "iqp_number" })
    private _iqpNumber: number;

    @typedjson.JsonMember({ type: String, name: "iqp_currency" })
    private _currency: string;

    @typedjson.JsonMember({ type: Date, name: "iqp_due_date" })
    private _dueDate: Date;

    @typedjson.JsonMember({ type: Date, name: "iqp_last_date" })
    private _lastDate: Date;

    @typedjson.JsonMember({ type: String, name: "iqp_note" })
    private _note: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_active_ind" })
    private _activeInd: number;

    @typedjson.JsonMember({ type: String, name: "iqp_term_days" })
    private _termDays: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_det_ledger_account_code" })
    private _ledgerAccountCode: number;

    @typedjson.JsonMember({ type: String, name: "iqp_sent_ind" })
    private _sentInd: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_sent_by_user" })
    private _sentByUser: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_sent_by_brand" })
    private _sentByBrand: number;

    @typedjson.JsonMember({ type: String, name: "iqp_approved_ind" })
    private _approvedInd: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_approved_by_user" })
    private _approvedByUser: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_approved_by_brand" })
    private _approvedByBrand: number;

    @typedjson.JsonMember({ type: String, name: "iqp_rejected_ind" })
    private _rejectedInd: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_rejected_by_user" })
    private _rejectedByUser: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_rejected_by_brand" })
    private _rejectedByBrand: number;

    @typedjson.JsonMember({ type: String, name: "iqp_paid_and_closed_ind" })
    private _paidAndClosedInd: string;

    @typedjson.JsonMember({ type: Number, name: "iqp_total_raw" })
    private _rawTotal: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_discount_perc" })
    private _discountPer: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_discount_perc_amnt" })
    private _discountPerAmt: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_discount_man" })
    private _discountManual: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_adjustment" })
    private _adjustment: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_shipping_charge" })
    private _shippingCharge: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_VAT_perc" })
    private _vatPercent: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_VAT" })
    private _vatAmt: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_paid" })
    private _paidAmt: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_total_final" })
    private _totalFinal: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_discount_perc_ind" })
    private _discountPerInd: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_discount_man_ind" })
    private _discountManualInd: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_VAT_ind" })
    private _vatInd: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_CGST_perc_amount" })
    private _CGSTPerAmt: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_SGST_perc_amount" })
    private _SGSTPerAmt: number;

    @typedjson.JsonMember({ type: Number, name: "iqp_IGST_perc_amount" })
    private _IGSTPerAmt: number;

    @typedjson.JsonMember({ type: String, name: "iqp_total_amnt_in_words" })
    private _totalAmtInWords: string;

    //*************************************dynaminc discounting********************************************** */



    @typedjson.JsonMember({ type: Number, name: "dd_payment_id" })
    private _ddPaymentId: number;

    @typedjson.JsonMember({ type: Number, name: "dd_created_by_brand_id" })
    private _ddCreatedByBrandId: number;

    @typedjson.JsonMember({ type: Number, name: "dd_created_by_user_id" })
    private _ddCreatedByUserId: number;

    @typedjson.JsonMember({ type: Date, name: "dd_iqp_issue_date" })
    private _ddIqpIssueDate: Date;

    @typedjson.JsonMember({ type: Date, name: "dd_iqp_due_date" })
    private _ddIqpDueDate: Date;

    @typedjson.JsonMember({ type: Number, name: "dd_early_pmnt_APR" })
    private _ddEarlyPmntAPR: number;

    @typedjson.JsonMember({ type: Date, name: "dd_early_pmnt_date" })
    private _ddEarlyPmntDate: Date;

    @typedjson.JsonMember({ type: Number, name: "dd_early_pmnt_applicable_APR" })
    private _ddEarlyPmntApplicableAPR: number;

    @typedjson.JsonMember({ type: Number, name: "dd_early_pmnt_amnt" })
    private _ddEarlyPmntAmnt: number;

    @typedjson.JsonMember({ type: Number, name: "dd_iqp_total_amnt" })
    private _ddIqpTotalAmnt: number;

    @typedjson.JsonMember({ type: Number, name: "dd_iqp_total_amnt_discounted" })
    private _ddIqpTotalAmntDiscounted: number;

    @typedjson.JsonMember({ type: Number, name: "dd_approved_ind" })
    private _ddApprovedInd: number;

    @typedjson.JsonMember({ type: Number, name: "dd_approved_by_user_id" })
    private _ddApprovedByuserId: number;

    @typedjson.JsonMember({ type: Number, name: "dd_approved_by_brand_id" })
    private _ddApprovedByBrandId: number;

    @typedjson.JsonMember({ type: Number, name: "dd_active_ind" })
    private _ddActiveInd: number;

    @typedjson.JsonMember({ type: Number, name: "dd_sent_ind" })
    private _ddSentInd: number;

    @typedjson.JsonMember({ type: Number, name: "dd_sent_to_user_id" })
    private _ddsentToUserId: number;

    @typedjson.JsonMember({ type: Number, name: "dd_sent_to_brand_id" })
    private _ddSentToBrandId: number;

    @typedjson.JsonMember({ type: String, name: "dd_invoice_number" })
    private _ddInvoiceNumber: string;

    @typedjson.JsonMember({ type: Number, name: "dd_iqp_term" })
    private _ddIqpTerm: number;


    // @typedjson.JsonMember({ type: String })
    // private _tableName: string = "Prod_img";

    public setProperties(fromBrandName: string, fromUserName: string, iqpId: number, type: string, invoiceNumber: string,
        brandFromId: number, brandToId: number,
        userFromId: number, userToId: number, contactToEmailAddress: string, contactToId: number, issueDate: Date,
        iqpNumber: number, currency: string, dueDate: Date, lastDate: Date, note: string, activeInd: number, termDays: string,
        ledgerAccountCode: number, sentInd: string, sentByUser: number, sentByBrand: number, approvedInd: string,
        approvedByUser: number, approvedByBrand: number, rejectedInd: string, rejectedByUser: number, rejectedByBrand: number,
        paidAndClosedInd: string, rawTotal: number, discountPer: number, discountPerAmt: number, discountManual: number,
        adjustment: number, shippingCharge: number, vatPercent: number, vatAmt: number, paidAmt: number, totalFinal: number,
        discountPerInd: number, discountManualInd: number, vatInd: number, cGSTPerAmt: number, sGSTPerAmt: number,
        iGSTPerAmt: number, totalAmtInWords: string, ddPaymentId: number, ddCreatedByBrandId: number,
        ddCreatedByUserId: number, ddIqpIssueDate: Date, ddIqpDueDate: Date, ddEarlyPmntAPR: number,
        ddEarlyPmntDate: Date, ddEarlyPmntApplicableAPR: number, ddEarlyPmntAmnt: number, ddIqpTotalAmnt: number,
        ddIqpTotalAmntDiscounted: number, ddApprovedInd: number, ddApprovedByuserId: number, ddApprovedByBrandId: number,
        ddActiveInd: number, ddSentInd: number, ddsentToUserId: number, ddSentToBrandId: number, ddInvoiceNumber: string,
        ddIqpTerm: number
    ) {

        this._fromBrandName = fromBrandName;
        this._fromUserName = fromUserName;
        this._iqpId = iqpId;
        this._type = type;
        this._invoiceNumber = invoiceNumber;
        this._brandFromId = brandFromId;
        this._brandToId = brandToId;
        this._userFromId = userFromId;
        this._userToId = userToId;
        this._contactToEmailAddress = contactToEmailAddress;
        this._contactToId = contactToId;
        this._issueDate = issueDate;
        this._iqpNumber = iqpNumber;
        this._currency = currency;
        this._dueDate = dueDate;
        this._lastDate = lastDate;
        this._note = note;
        this._activeInd = activeInd;
        this._termDays = termDays;
        this._ledgerAccountCode = ledgerAccountCode;
        this._sentInd = sentInd;
        this._sentByUser = sentByUser;
        this._sentByBrand = sentByBrand;
        this._approvedInd = approvedInd;
        this._approvedByUser = approvedByUser;
        this._approvedByBrand = approvedByBrand;
        this._rejectedInd = rejectedInd;
        this._rejectedByUser = rejectedByUser;
        this._rejectedByBrand = rejectedByBrand;
        this._paidAndClosedInd = paidAndClosedInd;
        this._rawTotal = rawTotal;
        this._discountPer = discountPer;
        this._discountPerAmt = discountPerAmt;
        this._discountManual = discountManual;
        this._adjustment = adjustment;
        this._shippingCharge = shippingCharge;
        this._vatPercent = vatPercent;
        this._vatAmt = vatAmt;
        this._paidAmt = paidAmt;
        this._totalFinal = totalFinal;
        this._discountPerInd = discountPerInd;
        this._discountManualInd = discountManualInd;
        this._vatInd = vatInd;
        this._CGSTPerAmt = cGSTPerAmt;
        this._SGSTPerAmt = sGSTPerAmt;
        this._IGSTPerAmt = iGSTPerAmt;
        this._totalAmtInWords = totalAmtInWords;

        this._ddPaymentId = ddPaymentId;
        this._ddCreatedByBrandId = ddCreatedByBrandId;
        this._ddCreatedByUserId = ddCreatedByUserId;
        this._ddIqpIssueDate = ddIqpIssueDate;
        this._ddIqpDueDate = ddIqpDueDate;
        this._ddEarlyPmntAPR = ddEarlyPmntAPR;
        this._ddEarlyPmntDate = ddEarlyPmntDate;
        this._ddEarlyPmntApplicableAPR = ddEarlyPmntApplicableAPR;
        this._ddEarlyPmntAmnt = ddEarlyPmntAmnt;
        this._ddIqpTotalAmnt = ddIqpTotalAmnt;
        this._ddIqpTotalAmntDiscounted = ddIqpTotalAmntDiscounted;
        this._ddApprovedInd = ddApprovedInd;
        this._ddApprovedByuserId = ddApprovedByuserId;
        this._ddApprovedByBrandId = ddApprovedByBrandId;
        this._ddActiveInd = ddActiveInd;
        this._ddSentInd = ddSentInd;
        this._ddsentToUserId = ddsentToUserId;
        this._ddSentToBrandId = ddSentToBrandId;
        this._ddInvoiceNumber = ddInvoiceNumber;
        this._ddIqpTerm = ddIqpTerm;

    }
    public get fromBrandName(): string {
        return this._fromBrandName;
    }

    public get fromUserName(): string {
        return this._fromUserName;
    }

    public get iqpId(): number {
        return this._iqpId;
    }

    public get type(): string {
        return this._type;
    }

    public get invoiceNumber(): string {
        return this._invoiceNumber;
    }

    public get brandFromId(): number {
        return this._brandFromId;
    }

    public get brandToId(): number {
        return this._brandToId;
    }

    public get userFromId(): number {
        return this._userFromId;
    }

    public get userToId(): number {
        return this._userToId;
    }

    public get contactToEmailAddress(): string {
        return this._contactToEmailAddress;
    }

    public get contactToId(): number {
        return this._contactToId;
    }

    public get issueDate(): Date {
        return this._issueDate;
    }

    public get iqpNumber(): number {
        return this._iqpNumber;
    }

    public get currency(): string {
        return this._currency;
    }

    public get dueDate(): Date {
        return this._dueDate;
    }

    public get lastDate(): Date {
        return this.lastDate;
    }

    public get note(): string {
        return this._note;
    }

    public get activeInd(): number {
        return this._activeInd;
    }

    public get termDays(): string {
        return this._termDays;
    }

    public get ledgerAccountCode(): number {
        return this._ledgerAccountCode;
    }

    public get sentInd(): string {
        return this._sentInd;
    }

    public get sentByBrand(): number {
        return this._sentByBrand;
    }

    public get approvedInd(): string {
        return this._approvedInd;
    }

    public get approvedByUser(): number {
        return this._approvedByUser;
    }

    public get approvedByBrand(): number {
        return this._approvedByBrand;
    }

    public get rejectedInd(): string {
        return this._rejectedInd;
    }

    public get rejectedByUser(): number {
        return this._rejectedByUser;
    }

    public get rejectedByBrand(): number {
        return this._rejectedByBrand;
    }

    public get paidAndClosedInd(): string {
        return this._paidAndClosedInd;
    }

    public get rawTotal(): number {
        return this._rawTotal;
    }

    public get discountPer(): number {
        return this._discountPer;
    }

    public get discountPerAmt(): number {
        return this._discountPerAmt;
    }

    public get discountManual(): number {
        return this._discountManual;
    }

    public get adjustment(): number {
        return this._adjustment;
    }

    public get shippingCharge(): number {
        return this._shippingCharge;
    }

    public get vatPercent(): number {
        return this._vatPercent;
    }

    public get vatAmt(): number {
        return this._vatAmt;
    }

    public get paidAmt(): number {
        return this._paidAmt;
    }

    public get totalFinal(): number {
        return this._totalFinal;
    }

    public get discountPerInd(): number {
        return this._discountPerInd;
    }

    public get discountManualInd(): number {
        return this._discountManualInd;
    }

    public get vatInd(): number {
        return this._vatInd;
    }

    public get cGSTPerAmt(): number {
        return this._CGSTPerAmt;
    }

    public get sGSTPerAmt(): number {
        return this._SGSTPerAmt;
    }

    public get iGSTPerAmt(): number {
        return this._IGSTPerAmt;
    }

    public get totalAmtInWords(): string {
        return this._totalAmtInWords;
    }

    public get ddPaymentId(): number {
        return this._ddPaymentId;
    }

    public get ddCreatedByBrandId(): number {
        return this._ddCreatedByBrandId;
    }

    public get ddCreatedByUserId(): number {
        return this._ddCreatedByUserId;
    }

    public get ddIqpIssueDate(): Date {
        return this._ddIqpIssueDate;
    }

    public get ddIqpDueDate(): Date {
        return this._ddIqpDueDate;
    }

    public get ddEarlyPmntAPR(): number {
        return this._ddEarlyPmntAPR;
    }

    public get ddEarlyPmntDate(): Date {
        return this._ddEarlyPmntDate;
    }

    public get ddEarlyPmntApplicableAPR(): number {
        return this._ddEarlyPmntApplicableAPR;
    }

    public get ddEarlyPmntAmnt(): number {
        return this._ddEarlyPmntAmnt;
    }

    public get ddIqpTotalAmnt(): number {
        return this._ddIqpTotalAmnt;
    }

    public get ddIqpTotalAmntDiscounted(): number {
        return this._ddIqpTotalAmntDiscounted;
    }

    public get ddApprovedInd(): number {
        return this._ddApprovedInd;
    }

    public get ddApprovedByuserId(): number {
        return this._ddApprovedByuserId;
    }

    public get ddApprovedByBrandId(): number {
        return this._ddApprovedByBrandId;
    }

    public get ddActiveInd(): number {
        return this._ddActiveInd;
    }

    public get ddSentInd(): number {
        return this._ddSentInd;
    }

    public get ddsentToUserId(): number {
        return this._ddsentToUserId;
    }

    public get ddSentToBrandId(): number {
        return this._ddSentToBrandId;
    }

    public get ddInvoiceNumber(): string {
        return this._ddInvoiceNumber;
    }

    public get ddIqpTerm(): number {
        return this._ddIqpTerm;
    }

}
