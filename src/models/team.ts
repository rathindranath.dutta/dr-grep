/// <reference path="../_all.d.ts" />
"use strict";

import typedjson = require("typedjson");

/**
 * Team model
 */
@typedjson.JsonObject
export class Team {

    @typedjson.JsonMember({ type: Number, name: "team_member_id" })
    private _teamMemberId: number;

    @typedjson.JsonMember({ type: Number, name: "brand_id" })
    private _brandId: number;

    @typedjson.JsonMember({ type: Number, name: "user_id" })
    private _userId: number;

    @typedjson.JsonMember({ type: String, name: "team_member_role" })
    private _teamMemberRole: string;

    @typedjson.JsonMember({ type: String, name: "dept_name" })
    private _deptName: string;

    @typedjson.JsonMember({ type: Number, name: "is_active" })
    private _isActive: number;

    @typedjson.JsonMember({ type: String })
    private _tableName: string = "brand_team_profile";

    /**
    *  Instead of constructor we need to have a properties setter
    *  since typedjson does not work with a constructor
    */
    public setProperties(teamMemberId: number, brandId: number, userId: number, teamMemberRole: string,
        deptName: string, isActive: number) {
        this._teamMemberId = teamMemberId;
        this._brandId = brandId;
        this._userId = userId;
        this._teamMemberRole = teamMemberRole;
        this._deptName = deptName;
        this._isActive = isActive;
    }

    public get teamMemberId(): number {
        return this._teamMemberId;
    }
}
