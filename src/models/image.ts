/// <reference path="../_all.d.ts" />

import typedjson = require("typedjson");
/**
* Brand model
*/
@typedjson.JsonObject
export class Image {

  @typedjson.JsonMember({ type: Number, name: "prod_img_id" })
  private _id: number;

  @typedjson.JsonMember({ type: Number, name: "product_id" })
  private _productId: number;

  @typedjson.JsonMember({ type: String, name: "prod_img_link" })
  private _link: string;

  @typedjson.JsonMember({ type: String })
  private _tableName: string = "Prod_img";

  public setProperties(id: number, productId: number, link: string) {

    this._id = id;
    this._productId = productId;
    this._link = link;
  }

  public get id(): number {
    return this._id;
  }

  public get productId(): number {
    return this._productId;
  }

  public get link(): string {
    return this._link;
  }

  public set productId(productId: number) {
    this._productId = productId;
  }

}
