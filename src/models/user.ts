/// <reference path="../_all.d.ts" />
"use strict";

import typedjson = require("typedjson");

/**
 * User model
 */
@typedjson.JsonObject
export class User {

  @typedjson.JsonMember({ type: Number, name: "user_id" })
  private _id: number;

  @typedjson.JsonMember({ type: String, name: "user_email_id" })
  private _email: string;

  @typedjson.JsonMember({ type: String, name: "user_name" })
  private _name: string;

  @typedjson.JsonMember({ type: String, name: "password" })
  private _password: string;

  @typedjson.JsonMember({ type: String, name: "salt" })
  private _salt: string;

  @typedjson.JsonMember({ type: String, name: "user_is_active" })
  private _active: string;

  @typedjson.JsonMember({ type: Number, name: "brand_id" })
  private _brandId: number;

  private _roles: string[];

  /**
  *  Instead of constructor we need to have a properties setter
  *  since typedjson does not work with a constructor
  */
  public setProperties(id: number, email: string, name: string, password: string,
    salt: string, roles: string[]) {
    this._id = id;
    this._email = email;
    this._name = name;
    this._password = password;
    this._salt = salt;
    this._active = "N";
    //this._brandId = brandId; //Team member add
    this._roles = roles || [];
  }

  public get id(): number {
    return this._id;
  }

  public get email(): string {
    return this._email;
  }

  public get name(): string {
    return this._name;
  }

  public get active(): string {
    return this._active;
  }

  public set active(status: string) {
    this._active = status;
  }

  public get password(): string {
    return this._password;
  }

  public get salt(): string {
    return this._salt;
  }

  public get roles(): string[] {
    return this._roles;
  }

  public get brandId(): number {
    return this._brandId;
  }

  public set id(id: number) {
    this._id = id;
  }

}
