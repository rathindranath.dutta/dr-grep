/// <reference path="../_all.d.ts" />
"use strict";

import typedjson = require("typedjson");

/**
 * Store model
 */
 @typedjson.JsonObject
 export class Store {

  @typedjson.JsonMember({type: Number, name: "id"})
 	private _id: number;

  @typedjson.JsonMember({type: String, name: "userId"})
  private _userId: string;

  @typedjson.JsonMember({type: String, name: "desc"})
 	private _desc: string;

  @typedjson.JsonMember({type: String, name: "logoUrl"})
 	private _logoUrl: string;

  @typedjson.JsonMember({type: String, name: "name"})
 	private _name: string;

  @typedjson.JsonMember({type: String, name: "url"})
 	private _url: string;

  @typedjson.JsonMember({type: String, name: "email"})
 	private _email: string;

  @typedjson.JsonMember({type: String, name: "blogUrl"})
 	private _blogUrl: string;

  @typedjson.JsonMember({type: Boolean, name: "isOpen"})
 	private _isOpen: boolean;

  setProperties(id: number, userId: string, desc: string, logoUrl: string,
    name: string, url: string, email: string, blogUrl: string,
    isOpen: boolean) {

    this._id = id;
    this._desc = desc;
    this._logoUrl = logoUrl;
    this._name = name;
    this._url = url;
    this._email = email;
    this._blogUrl = blogUrl;
    this._isOpen = isOpen;
   }

   public get id(): number {
     return this._id;
   }

   public get desc(): string {
     return this._desc;
   }

   public get logoUrl(): string {
     return this._logoUrl;
   }

   public get name(): string {
     return this._name;
   }

   public get url(): string {
     return this._url;
   }

   public get email(): string {
     return this._email;
   }

   public get blogUrl(): string {
     return this._blogUrl;
   }

   public get isOpen(): boolean {
     return this._isOpen;
   }

 }
 