/// <reference path="../_all.d.ts" />

import typedjson = require ("typedjson");
/**
* Variation model
*/
@typedjson.JsonObject
export class Variation {

  @typedjson.JsonMember({type: Number, name: "variation_id"})
	private _id: number;

  @typedjson.JsonMember({type: Number, name: "prod_id"})
  private _productId: number;

  @typedjson.JsonMember({type: String, name: "variation_name"})
	private _name: string;

  @typedjson.JsonMember({type: Number, name: "variation_quantity"})
	private _quantity: number;

  @typedjson.JsonMember({type: Date, name: "variation_timestamp"})
  private _timestamp: Date;

  @typedjson.JsonMember({ type: String})
  private _tableName: string = "prod_variation_entries";

  public setProperties(id: number, productId: number, name: string, quantity: number) {
    this._id = id;
    this._productId = productId;
    this._name = name;
    this._quantity = quantity;
  }

   public get id(): number {
     return this._id;
   }

   public get productId(): number {
     return this._productId;
   }

   public get name(): string {
     return this._name;
   }

   public get quantity(): number {
     return this._quantity;
   }

}
