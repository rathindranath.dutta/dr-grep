/// <reference path="../_all.d.ts" />

import typedjson = require("typedjson");
import productModule = require("./product");

/**
* Brand model
*/
@typedjson.JsonObject
export class Brand {

  @typedjson.JsonMember({ type: Number, name: "brand_id" })
  private _brandId: number;

  @typedjson.JsonMember({ type: Number, name: "user_id" })
  private _userId: number;

  @typedjson.JsonMember({ type: String, name: "brand_name" })
  private _brandName: string;

  @typedjson.JsonMember({ type: String, name: "guid" })
  private _guid: string;

  @typedjson.JsonMember({ type: String, name: "brand_desc" })
  private _brandDesc: string;

  @typedjson.JsonMember({ type: Number, name: "is_active" })
  private _isActive: number;

  @typedjson.JsonMember({ type: String, name: "brand_about_us" })
  private _brandAboutUs: string;

  @typedjson.JsonMember({ type: String, name: "brand_localtion" })
  private _brandLocation: string;

  @typedjson.JsonMember({ type: String, name: "brand_website" })
  private _brandWebsite: string;

  @typedjson.JsonMember({ type: String, name: "brand_registration_number" })
  private _brandRegistrationNumber: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_country" })
  private _brandAddrCountry: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_city" })
  private _brandAddrCity: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_post_code" })
  private _brandAddrPostCode: string;

  @typedjson.JsonMember({ type: String, name: "brand_type" })
  private _brandType: string;

  @typedjson.JsonMember({ type: String, name: "brand_industry" })
  private _brandIndustry: string;

  @typedjson.JsonMember({ type: String, name: "brand_contact_phone_number" })
  private _brandContactPhoneNumber: string;

  @typedjson.JsonMember({ type: String, name: "brand_contact_email_address" })
  private _brandContactEmailAddress: string;

  @typedjson.JsonMember({ type: String, name: "brand_lat" })
  private _brandLat: string;

  @typedjson.JsonMember({ type: String, name: "brand_long" })
  private _brandLong: string;

  @typedjson.JsonMember({ type: String, name: "brand_tax_id" })
  private _brandTaxId: string;

  @typedjson.JsonMember({ type: String, name: "facebook_profile_link" })
  private _facebookProfileLink: string;

  @typedjson.JsonMember({ type: String, name: "linkedin_profile_link" })
  private _linkedinProfileLink: string;

  @typedjson.JsonMember({ type: String, name: "twitter_profile_link" })
  private _twitterProfileLink: string;

  @typedjson.JsonMember({ type: String, name: "amazon_profile_link" })
  private _amazonProfileLink: string;

  @typedjson.JsonMember({ type: String, name: "other_profile_link" })
  private _otherProfileLink: string;

  @typedjson.JsonMember({ type: String, name: "brand_pan_number" })
  private _brandPanNumber: string;

  @typedjson.JsonMember({ type: String, name: "brand_bank_accnt_number" })
  private _brandBankAccountNumber: string;

  @typedjson.JsonMember({ type: String, name: "brand_billing_to_fullname" })
  private _brandBillingToFullname: string;

  @typedjson.JsonMember({ type: String, name: "brand_billing_to_email" })
  private _brandbillingToEmail: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_line1" })
  private _brandAddrLine1: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_line2" })
  private _brandAddrLine2: string;

  @typedjson.JsonMember({ type: String, name: "brand_addr_state" })
  private _brandAddrState: string;

  @typedjson.JsonMember({ type: String, name: "brand_GSTIN" })
  private _brandGSTIN: string;

  @typedjson.JsonMember({ type: String, name: "brand_state_cd" })
  private _brandStateCd: string;

  @typedjson.JsonMember({ type: Number, name: "brand_shipping_info_as_billing_ind" })
  private _brandShippingInfoAsBillingInd: number;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_to_fullname" })
  private _brandShippingToFullname: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_to_email" })
  private _brandShippingToEmail: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_address_line1" })
  private _brandShippingAddressLine1: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_address_line2" })
  private _brandShippingAddressLine2: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_city" })
  private _brandShippingCity: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_state" })
  private _brandShippingState: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_zip_cd" })
  private _brandShippingZipCd: string;

  @typedjson.JsonMember({ type: String, name: "brand_shipping_country" })
  private _brandShippingCountry: string;

  @typedjson.JsonMember({ type: Number, name: "period_in_business" })
  private _periodInBusiness: number;

  @typedjson.JsonMember({ type: String, name: "area_covered" })
  private _areaCovered: string;

  @typedjson.JsonMember({ type: String, name: "annual_turn_over" })
  private _annualTurnCover: string;

  @typedjson.JsonMember({ type: Number, name: "NMMC_cess_number" })
  private _nmmcCessNumber: number;

  @typedjson.JsonMember({ type: String, name: "brand_CIN" })
  private _brandCin: string;

  @typedjson.JsonMember({ type: String, name: "brand_yoy_revenue" })
  private _brandYoyRevenue: string;

  @typedjson.JsonMember({ type: String, name: "brand_bank_name" })
  private _brandBankName: string;

  @typedjson.JsonMember({ type: String, name: "brand_bank_branch_name" })
  private _brandBankBranchName: string;

  @typedjson.JsonMember({ type: String, name: "brand_bank_accnout_number" })
  private _brandBankAccountNum: string;

  @typedjson.JsonMember({ type: String, name: "brand_bank_IFSC_code" })
  private _brandBankIfscCode: string;

  @typedjson.JsonMember({ type: String, name: "brand_drug_lic_number" })
  private _brandDrugLicNum: string;

  @typedjson.JsonMember({ type: String, name: "brand_FSSAI_number" })
  private _brandFssiNum: string;

  @typedjson.JsonMember({ elements: productModule.Product })
  private _products: Array<productModule.Product>;

  public setProperties(brandId: number, userId: number, brandName: string,
    brandDesc: string, guid: string, isActive: number, brandAboutUs: string,
    brandLocation: string, brandWebsite: string, brandRegistrationNumber: string,
    brandAddrCountry: string, brandAddrCity: string, brandAddrPostCode: string,
    brandType: string, brandIndustry: string, brandContactPhoneNumber: string,
    brandContactEmailAddress: string, brandLat: string, brandLong: string,
    brandTaxId: string, facebookProfileLink: string, linkedinProfileLink: string,
    twitterProfileLink: string, amazonProfileLink: string, brandPanNumber: string,
    brandBankAccountNumber: string, brandBillingToFullname: string, brandbillingToEmail: string,
    brandAddrLine1: string, brandAddrLine2: string, brandAddrState: string,
    brandGSTIN: string, brandStateCd: string, brandShippingInfoAsBillingInd: number,
    brandShippingToFullname: string, brandShippingToEmail: string, brandShippingAddressLine1: string,
    brandShippingAddressLine2: string, brandShippingCity: string, brandShippingState: string,
    brandShippingZipCd: string, brandShippingCountry: string, periodInBusiness: number,
    areaCovered: string, annualTurnCover: string, nmmcCessNumber: number) {

    this._brandId = brandId;
    this._userId = userId;
    this._brandName = brandName;
    this._brandDesc = brandDesc;
    this._guid = guid;
    this._isActive = isActive;
    this._brandAboutUs = brandAboutUs;
    this._brandLocation = brandLocation;
    this._brandWebsite = brandWebsite;
    this._brandRegistrationNumber = brandRegistrationNumber;
    this._brandAddrCountry = brandAddrCountry;
    this._brandAddrCity = brandAddrCity;
    this._brandAddrPostCode = brandAddrPostCode;
    this._brandType = brandType;
    this._brandIndustry = brandIndustry;
    this._brandContactPhoneNumber = brandContactPhoneNumber;
    this._brandContactEmailAddress = brandContactEmailAddress;
    this._brandLat = brandLat;
    this._brandLong = brandLong;
    this._brandTaxId = brandTaxId;
    this._facebookProfileLink = facebookProfileLink;
    this._linkedinProfileLink = linkedinProfileLink;
    this._twitterProfileLink = twitterProfileLink;
    this._amazonProfileLink = amazonProfileLink;
    this._brandPanNumber = brandPanNumber;
    this._brandBankAccountNumber = brandBankAccountNumber;
    this._brandBillingToFullname = brandBillingToFullname;
    this._brandbillingToEmail = brandbillingToEmail;
    this._brandAddrLine1 = brandAddrLine1;
    this._brandAddrLine2 = brandAddrLine2;
    this._brandAddrState = brandAddrState;
    this._brandGSTIN = brandGSTIN;
    this._brandStateCd = brandStateCd;
    this._brandShippingInfoAsBillingInd = brandShippingInfoAsBillingInd;
    this._brandShippingToFullname = brandShippingToFullname;
    this._brandShippingToEmail = brandShippingToEmail;
    this._brandShippingAddressLine1 = brandShippingAddressLine1;
    this._brandShippingAddressLine2 = brandShippingAddressLine2;
    this._brandShippingCity = brandShippingCity;
    this._brandShippingState = brandShippingState;
    this._brandShippingZipCd = brandShippingZipCd;
    this._brandShippingCountry = brandShippingCountry;
    this._periodInBusiness = periodInBusiness;
    this._areaCovered = areaCovered;
    this._annualTurnCover = annualTurnCover;
    this._nmmcCessNumber = nmmcCessNumber;
  }

  public get brandId(): number {
    return this._brandId;
  }
  public get userId(): number {
    return this._userId;
  }
  public get brandName(): string {
    return this._brandName;
  }
  public get brandDesc(): string {
    return this._brandDesc;
  }
  public get guid(): string {
    return this._guid;
  }
  public get isActive(): number {
    return this._isActive;
  }
  public get brandAboutUs(): string {
    return this._brandAboutUs;
  }
  public get brandLocation(): string {
    return this._brandLocation;
  }
  public get brandWebsite(): string {
    return this._brandWebsite;
  }
  public get brandRegistrationNumber(): string {
    return this._brandRegistrationNumber;
  }
  public get brandAddrCountry(): string {
    return this._brandAddrCountry;
  }
  public get brandAddrCity(): string {
    return this._brandAddrCity;
  }
  public get brandAddrPostCode(): string {
    return this._brandAddrPostCode;
  }
  public get brandType(): string {
    return this._brandType;
  }
  public get brandIndustry(): string {
    return this._brandIndustry;
  }
  public get brandContactPhoneNumber(): string {
    return this._brandContactPhoneNumber;
  }
  public get brandContactEmailAddress(): string {
    return this._brandContactEmailAddress;
  }
  public get brandLat(): string {
    return this._brandLat;
  }
  public get brandLong(): string {
    return this._brandLong;
  }
  public get brandTaxId(): string {
    return this._brandTaxId;
  }
  public get facebookProfileLink(): string {
    return this._facebookProfileLink;
  }
  public get linkedinProfileLink(): string {
    return this._linkedinProfileLink;
  }
  public get twitterProfileLink(): string {
    return this._twitterProfileLink;
  }
  public get amazonProfileLink(): string {
    return this._amazonProfileLink;
  }
  public get brandPanNumber(): string {
    return this._brandPanNumber;
  }
  public get brandBankAccountNumber(): string {
    return this._brandBankAccountNumber;
  }
  public get brandBillingToFullname(): string {
    return this._brandBillingToFullname;
  }
  public get brandbillingToEmail(): string {
    return this._brandbillingToEmail;
  }
  public get brandAddrLine1(): string {
    return this._brandAddrLine1;
  }
  public get brandAddrLine2(): string {
    return this._brandAddrLine2;
  }
  public get brandAddrState(): string {
    return this._brandAddrState;
  }
  public get brandGSTIN(): string {
    return this._brandGSTIN;
  }
  public get brandStateCd(): string {
    return this._brandStateCd;
  }
  public get brandShippingInfoAsBillingInd(): number {
    return this._brandShippingInfoAsBillingInd;
  }
  public get brandShippingToFullname(): string {
    return this._brandShippingToFullname;
  }
  public get brandShippingToEmail(): string {
    return this._brandShippingToEmail;
  }
  public get brandShippingAddressLine1(): string {
    return this._brandShippingAddressLine1;
  }
  public get brandShippingAddressLine2(): string {
    return this._brandShippingAddressLine2;
  }
  public get brandShippingCity(): string {
    return this._brandShippingCity;
  }
  public get brandShippingState(): string {
    return this._brandShippingState;
  }
  public get brandShippingZipCd(): string {
    return this.brandShippingZipCd;
  }
  public get brandShippingCountry(): string {
    return this._brandShippingCountry;
  }
  public get periodInBusiness(): number {
    return this._periodInBusiness;
  }
  public get areaCovered(): string {
    return this._areaCovered;
  }
  public get annualTurnCover(): string {
    return this._annualTurnCover;
  }
  public get nmmcCessNumber(): number {
    return this._nmmcCessNumber;
  }
  public get products(): Array<productModule.Product> {
    return this._products;
  }

}
