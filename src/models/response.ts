/// <reference path="../_all.d.ts" />
"use strict";

/**
 * Response interface
 */
export abstract class Response {

  public abstract isValid(): boolean;

}
