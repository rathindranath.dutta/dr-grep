/// <reference path="../_all.d.ts" />

import variationModule = require("./variation");
import imageModule = require("./image");
import typedjson = require("typedjson");
/**
* Product model
*/
@typedjson.JsonObject
export class Product {

  @typedjson.JsonMember({ type: Number, name: "product_id" })
  private _id: number;

  @typedjson.JsonMember({ type: Number, name: "user_id" })
  private _userId: number;

  @typedjson.JsonMember({ type: Number, name: "brand_id" })
  private _brandId: number;

  @typedjson.JsonMember({ type: String, name: "title" })
  private _title: string;

  @typedjson.JsonMember({ type: String, name: "description" })
  private _desc: string;

  @typedjson.JsonMember({ type: String, name: "price" })
  private _price: string;

  @typedjson.JsonMember({ type: String, name: "category" })
  private _category: string;

  @typedjson.JsonMember({ type: Number, name: "upc" })
  private _upc: number;

  @typedjson.JsonMember({ type: String, name: "guid" })
  private _guid: string;

  @typedjson.JsonMember({ type: String, name: "is_new" })
  private _isNew: string;

  @typedjson.JsonMember({ type: String, name: "latitude" })
  private _lat: string;

  @typedjson.JsonMember({ type: String, name: "longitude" })
  private _long: string;

  @typedjson.JsonMember({ type: Number, name: "is_active" })
  private _isActive: number;

  @typedjson.JsonMember({ elements: variationModule.Variation, name: "variations" })
  private _variations: Array<variationModule.Variation>;

  @typedjson.JsonMember({ elements: imageModule.Image })
  private _images: Array<imageModule.Image>;

  @typedjson.JsonMember({ type: Date, name: "add_time" })
  private _addTime: Date;

  @typedjson.JsonMember({ type: Date, name: "update_time" })
  private _updateTime: Date;

  @typedjson.JsonMember({ type: String, name: "prod_HSN_cd" })
  private _prodHsnCd: string;

  @typedjson.JsonMember({ type: Number, name: "item_ledger_account_cogs_code" })
  private _ledgerAccountCogsCode: number;

  @typedjson.JsonMember({ type: Number, name: "item_ledger_account_inventory_code" })
  private _ledgerAccountInventoryCode;

  @typedjson.JsonMember({ type: Number, name: "item_ledger_account_purchase_code" })
  private _ledgerAccountPurchaseCode: number;

  @typedjson.JsonMember({ type: Number, name: "item_ledger_account_sales_code" })
  private _ledgerAccountSalesCode: number;

  @typedjson.JsonMember({ type: Number, name: "purchase_unit_rate" })
  private _purchaseUnitRate: number;

  @typedjson.JsonMember({ type: Number, name: "prod_purchase_price" })
  private _prodPurchasePrice: number;

  @typedjson.JsonMember({ type: Number, name: "prod_discount" })
  private _prodDiscount: number;

  @typedjson.JsonMember({ type: String, name: "prod_unit_measure" })
  private _prodUnitMeasure: string;

  @typedjson.JsonMember({ type: String, name: "prod_type" })
  private _prodType: string;

  @typedjson.JsonMember({ type: Number, name: "prod_margin_perc" })
  private _prodMarginPerc: number;

  @typedjson.JsonMember({ type: String, name: "prod_weight" })
  private _weight: string;

  @typedjson.JsonMember({ type: String, name: "prod_benefits" })
  private _benefits: string;

  @typedjson.JsonMember({ type: String, name: "prod_Ingredients" })
  private _meterial: string;

  @typedjson.JsonMember({ type: Number, name: "prod_msrp" })
  private _msrp: number;

  @typedjson.JsonMember({ type: String, name: "prod_brand_name" })
  private _brandName: string;

  @typedjson.JsonMember({ type: String, name: "prod_batch_no" })
  private _batchNo: string;

  @typedjson.JsonMember({ type: String, name: "prod_bar_code" })
  private _barCode: string;

  @typedjson.JsonMember({ type: String, name: "prod_warning" })
  private _warning: string;

  @typedjson.JsonMember({ type: String, name: "prod_how_to_use" })
  private _howToUse: string;

  public setProperties(id: number, userId: number, brandId: number, title: string,
    desc: string, price: string, category: string, upc: number, guid: string, isNew: string,
    lat: string, long: string, isActive: number,
    variations: Array<variationModule.Variation>, images: Array<imageModule.Image>,
    addTime: Date, updateTime: Date) {

    this._id = id;
    this._userId = userId;
    this._brandId = brandId;
    this._title = title;
    this._desc = desc;
    this._price = price;
    this._category = category;
    this._upc = upc;
    this._guid = guid;
    this._isNew = isNew;
    this._lat = lat;
    this._long = long;
    this._isActive = isActive;
    this._variations = variations;
    this._images = images;
    this._addTime = addTime;
    this._updateTime = updateTime;
  }

  public get id(): number {
    return this._id;
  }

  public get userId(): number {
    return this._userId;
  }

  public get brandId(): number {
    return this._brandId;
  }

  public get title(): string {
    return this._title;
  }

  public get desc(): string {
    return this._desc;
  }

  public get price(): string {
    return this._price;
  }

  public get category(): string {
    return this._category;
  }

  public get isNew(): string {
    return this._isNew;
  }

  public get guid(): string {
    return this._guid;
  }

  public get lat(): string {
    return this._lat;
  }

  public get long(): string {
    return this._long;
  }

  public get isActive(): number {
    return this._isActive;
  }

  public get variations(): Array<variationModule.Variation> {
    return this._variations;
  }

  public set variations(variations: Array<variationModule.Variation>) {
    this._variations = variations;
  }

  public get images(): Array<imageModule.Image> {
    return this._images;
  }

  public set images(images: Array<imageModule.Image>) {
    this._images = images;
  }

  public get addTime(): Date {
    return this._addTime;
  }

  public get updateTime(): Date {
    return this._updateTime;
  }

  public set updateTime(updateTime: Date) {
    this._updateTime = updateTime;
  }

  public set addTime(addTime: Date) {
    this._addTime = addTime;
  }

}
