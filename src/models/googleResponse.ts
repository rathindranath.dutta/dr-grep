/// <reference path="../_all.d.ts" />
"use strict";

import * as response from "../models/response";

/**
 * Response interface
 */
export class GoogleResponse extends response.Response {
  public aud: string;

  public isValid(): boolean {
    if (this.aud != null && this.aud.indexOf("626783376623-nlhqq0k7hs02vq6jk5niqufkk15p2l2m.apps.googleusercontent.com") !== -1) {
      return true;
    }
    return false;
  }
}
