/// <reference path="../_all.d.ts" />

import typedjson = require("typedjson");
/**
* Brand model
*/
@typedjson.JsonObject
export class Contact {

  @typedjson.JsonMember({ type: Number, name: "contact_id" })
  private _contactId: number;

  @typedjson.JsonMember({ type: String, name: "contact_first_name" })
  private _contactFirstName: string;

  @typedjson.JsonMember({ type: String, name: "contact_last_name" })
  private _contactLastName: string;

  @typedjson.JsonMember({ type: String, name: "contact_phone_number" })
  private _contactPhoneNumber: string;

  @typedjson.JsonMember({ type: String, name: "contact_address" })
  private _contactAddress: string;

  @typedjson.JsonMember({ type: String, name: "contact_city" })
  private _contactContactCity: string;

  @typedjson.JsonMember({ type: String, name: "contact_zip_cd" })
  private _contactZipCode: string;

  @typedjson.JsonMember({ type: String, name: "contact_state" })
  private _contactState: string;

  @typedjson.JsonMember({ type: String, name: "is_contact_from_shop" })
  private _contactIsFromShop: string;

  @typedjson.JsonMember({ type: String, name: "is_contact_from_website" })
  private _contactIsFromWeb: string;

  @typedjson.JsonMember({ type: String, name: "is_contact_from_drgrep" })
  private _contactIsFromDrgrep: string;

  @typedjson.JsonMember({ type: Number, name: "shop_id" })
  private _contactShopId: number;

  @typedjson.JsonMember({ type: String, name: "contact_add_timestamp" })
  private _contactAddTimeStamp: String;

  @typedjson.JsonMember({ type: String, name: "contact_email_address" })
  private _contactEmailAddress: string;

  @typedjson.JsonMember({ type: Number, name: "user_id" })
  private _contactUserId: number;

  @typedjson.JsonMember({ type: Number, name: "brand_id" })
  private _contactBrandId: number;

  @typedjson.JsonMember({ type: String, name: "is_a_team_mamber_ind" })
  private _contactIsTeamMemberInd: string;

  @typedjson.JsonMember({ type: Number, name: "contact_user_id" })
  private _contactContactUserId: number;

  @typedjson.JsonMember({ type: String, name: "is_invitation_sent" })
  private _contactIsInvitationSent: string;

  @typedjson.JsonMember({ type: Number, name: "is_a_customer" })
  private _contactIsCustomer: number;

  @typedjson.JsonMember({ type: String, name: "contact_note" })
  private _contactNote: string;

  @typedjson.JsonMember({ type: String, name: "contact_lat" })
  private _contactLat: string;

  @typedjson.JsonMember({ type: String, name: "contact_long" })
  private _contactLong: string;

  @typedjson.JsonMember({ type: String, name: "contact_addr_line1" })
  private _contactAddrLine1: string;

  @typedjson.JsonMember({ type: String, name: "contact_addr_line2" })
  private _contactAddrLine2: string;

  @typedjson.JsonMember({ type: String, name: "contact_country" })
  private _contactCountry: string;

  @typedjson.JsonMember({ type: String, name: "contact_GSTIN" })
  private _contactGstin: string;

  @typedjson.JsonMember({ type: String, name: "contact_state_cd" })
  private _contactStateCode: string;

  @typedjson.JsonMember({ type: String, name: "contact_business_name" })
  private _contactBusinessName: string;

  @typedjson.JsonMember({ type: Number, name: "contact_shipping_info_as_billing_ind" })
  private _contactShippingBilingInd: number;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_to_fullname" })
  private _contactShippingToFullName: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_to_email" })
  private _contactShippingToEmail: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_address_line1" })
  private _contactShippingAddressLine1: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_address_line2" })
  private _contactShippingAddressLine2: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_city" })
  private _contactShippingCity: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_state" })
  private _contactShippingState: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_zip_cd" })
  private _contactShippingZipCode: string;

  @typedjson.JsonMember({ type: String, name: "contact_shipping_country" })
  private _contactShippingCountry: string;

}
