/// <reference path="../_all.d.ts" />

import typedjson = require("typedjson");
/**
* Brand model
*/
@typedjson.JsonObject
export class Connection {

  @typedjson.JsonMember({ type: Number, name: "ctu_connection_id" })
  private _id: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_from_brand_id" })
  private _fromBrandId: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_to_brand_id" })
  private _toBrandId: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_from_user_id" })
  private _fromUserId: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_to_user_id" })
  private _toUserId: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_status_pending" })
  private _statusPending: number;

  @typedjson.JsonMember({ type: Number, name: "ctu_status_approved" })
  private _statusApproved: number;

  @typedjson.JsonMember({ type: String, name: "ctu_to_contact_email_address" })
  private _contectEmail: string;

}
