/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import contactServiceModule = require("../service/contactService");
import contactModule = require("../models/contact");
import typedjson = require("typedjson");
/**
 * Router class responsible to register all store routes
 */
export class ContactRouter extends routerModule.Router {

    private _contactService: contactServiceModule.ContactService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: contactServiceModule.ContactService) {
        super(app);
        this._contactService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {

        this.app.post("/contacts", function (req: express.Request, res: express.Response) {
            //let contact: contactModule.Contact = typedjson.TypedJSON.parse(JSON.stringify(req.body.contact), contactModule.Contact);
            console.log("Contact Obj: " + JSON.stringify(req.body.contact));
            this._contactService.create(req.body.contact, function (data: any, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(201).json({ message: "Contact created" });
                }
            });
        }.bind(this));

        this.app.put("/contacts/:id", function (req: express.Request, res: express.Response) {
            // let reqContact: any = req.body.contact;
            // reqContact["contact_id"] = parseInt(req.params.id);
            // let contact: contactModule.Contact = typedjson.TypedJSON.parse(JSON.stringify(reqContact), contactModule.Contact);
            this._contactService.update(req.body.contact, function (data: any, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "Contact updated" });
                }
            });
        }.bind(this));
    }

}
