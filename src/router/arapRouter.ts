/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import arapServiceModule = require("../service/arapService");
import multer = require("multer");
import aws = require("aws-sdk");
import bodyParser = require("body-parser");
import multerS3 = require("multer-s3");
import mime = require("mime");
// import formData = require('form-data');

/**
 * Router class responsible to register all store routes
 */
export class ARAPRouter extends routerModule.Router {

    private _arapService: arapServiceModule.ARAPService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: arapServiceModule.ARAPService) {
        super(app);
        this._arapService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        var mult = multer({ dest: "uploads/" });
        // aws.config.update({
        //     signatureVersion: 'v4'
        // });
        // var app = express(),
        var s3 = new aws.S3({
            accessKeyId: "AKIAI2D3ZP7AV3MN3GQA",
            secretAccessKey: "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",
            Bucket: "drgrep-staging",
            region: "ap-south-1"
        });
        // app.use(bodyParser.json());
        var upload = multer({
            storage: multerS3({
                s3: s3,
                bucket: "drgrep-staging",
                key: function (req: any, file: any, cb: any) {
                    console.log(file);
                    cb(null, "uploaded-files/" + Date.now().toString() + "." + mime.extension(file.mimetype)); //use Date.now() for unique file keys
                }
            })
        });
        this.app.post("/accounts/payable-receivable", function (req: express.Request, res: express.Response) {
            //console.log("STEP1:ARAP Module data" + JSON.stringify(req.body.data));
            this._arapService.createEditInvoice(req.body.data, req.query.sentInd, function (isSuccessfull: boolean, iqpId: number, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "Successful", iqpId: iqpId });
                }
            });
        }.bind(this));

        this.app.get("/accounts/payable-receivable/brand", function (req: express.Request, res: express.Response) {
            //console.log("STEP1:GET ARAP Module data\n" + req.query.id + " " + req.query.type + " " + req.query.acct);
            let discountType: boolean = (req.query.ddCount !== null && req.query.ddCount !== undefined) ? req.query.ddCount : "NA";
            this._arapService.getAllArapDetails(req.query.id, req.query.type, req.query.acct, discountType,
                function (accountPayableData: any, error: any) {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        res.status(200).json(accountPayableData);
                    }
                });
        }.bind(this));

        this.app.post("/accounts/payable-receivable/upload", upload.single("file"), function (req: express.Request, res: express.Response) {
            // console.log("STEP1:GET ARAP Module data\n" + req.query.sentInd);
            // console.log("STEP1:ARAP Module data\n" + req.body);
            // console.log(JSON.stringify(req));
            // res.status(200).json({ message: "Succesfull" });
            let arapObj: any = JSON.parse(req.body.jsonObj).data;
            let file: any = req.file;
            arapObj.iqp_file_name = file.key;
            this._arapService.createEditInvoice(arapObj, req.query.sentInd,
                function (isSuccessfull: boolean, iqpId: number, error: any) {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        res.status(200).json({ message: "Successful", iqpId: iqpId });
                    }
                });
        }.bind(this));

        this.app.get("/accounts/payable-receivable/getSignedUrl", function (req: express.Request, res: express.Response) {
            let params: any = {
                Bucket: "drgrep-staging",
                Key: req.query.docName,
            };
            s3.getSignedUrl("getObject", params, (err, url) => {
                res.status(200).json({
                    method: "get",
                    url,
                    fields: {},
                });
            });
        }.bind(this));
    }

}
