"use strict";
import * as express from "express";
import routerModule = require("./router");
import * as uuid from "uuid";
import updateUserServiceModule = require("../service/updateUserService");
import userModule = require("../models/user");
import UpdateUserDaoModule = require("../dao/updateUserDao");
import configModule = require("../dao/databaseConfig");
import typedjson = require("typedjson");

/**
 * Router class responsible to register all store routes
 */
export class UpdateUserRouter extends routerModule.Router {

  private _updateUserService: updateUserServiceModule.UpdateUserService;

  /**
  * Constructor.
  *
  * @class UpdateUserRouter
  * @constructor
  */
  constructor(app: express.Application, updateUserService: updateUserServiceModule.UpdateUserService) {
    super(app);
    this._updateUserService = updateUserService;
  }

  public registerRoutes() {
    this.app.post("/update/username", function (req: express.Request, res: express.Response) {
      let updatedName: string = req.body.data["new-userName"];
      if (updatedName != null) {

        let email: string = req.body.data.email;

        this._updateUserService.getUserByEmail(email, function (user: userModule.User, error: any) {

          if (user != null) {
            user.setProperties(user.id, email, updatedName, null, null, user.roles);
            this._updateUserService.saveUser(user);
          } else {
            res.status(404).json({ "message": "user not found" });
          }
        }.bind(this));
      } else {
        res.status(404).json({ "message": "new name not sent" });
      }


    }.bind(this));
  }
}
