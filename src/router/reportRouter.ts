/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import reportServiceModule = require("../service/reportService");
import typedjson = require("typedjson");
/**
 * Router class responsible to register all store routes
 */
export class ReportRouter extends routerModule.Router {

    private _reportService: reportServiceModule.ReportService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: reportServiceModule.ReportService) {
        super(app);
        this._reportService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.get("/report/gstr1", function (req: express.Request, res: express.Response) {
            console.log("STEP1: \n" + "BrandId:" + req.query.brandId + "\n" + "month: " + req.query.month + "\n" + "Year: " + req.query.year);
            this._reportService.getGstr1Rpt(req.query.brandId, req.query.month, req.query.year, function (reportData: any, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(reportData);
                }
            });
        }.bind(this));
    }

}
