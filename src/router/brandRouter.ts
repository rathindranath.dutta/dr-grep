/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import brandModule = require("../models/brand");
import brandServiceModule = require("../service/brandService");
import productModule = require("../models/product");
/**
 * Router class responsible to register all store routes
 */
export class BrandRouter extends routerModule.Router {

  private _brandService: brandServiceModule.BrandService;

  /**
  * Constructor.
  *
  * @class BrandRouter
  * @constructor
  */
  constructor(app: express.Application, service: brandServiceModule.BrandService) {
    super(app);
    this._brandService = service;
  }

  /**
   * Overridden
   */
  public registerRoutes() {

    this.app.post("/brands", function (req: express.Request, res: express.Response) {
      this._brandService.create(req.body.store, function (id: number, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.header("Location", "/brands/{{id}}");
          res.status(201).json({ message: "Brand created" });
        }
      });
    }.bind(this));

    this.app.delete("/brands/:id", function (req: express.Request, res: express.Response) {
      this._brandService.delete(req.params.id, function (deleted: boolean, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else if (deleted) {
          res.status(204).json({ message: "Deleted a brand" });
        } else {
          res.status(500).json({ message: "Couldn't delete" });
        }
      });
    }.bind(this));

    this.app.get("/brands", function (req: express.Request, res: express.Response) {
      this._brandService.getBrandByUserEmail(req.query.userEmail, function (brand: brandModule.Brand, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(brand);
        }
      }.bind(this));
    }.bind(this));

    this.app.put("/brands/:id", function (req: express.Request, res: express.Response) {
      this._brandService.updateBrand(req.body.brand, function (data: any, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json({ message: "Brand updated" });
        }
      });
    }.bind(this));

  }

}
