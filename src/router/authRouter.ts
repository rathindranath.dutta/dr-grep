"use strict";
import * as express from "express";
import routerModule = require("./router");
import * as uuid from "uuid";
import tokenModule = require("../models/token");
import authServiceModule = require("../service/authService");
import userModule = require("../models/user");
import authDaoModule = require("../dao/authDao");
import configModule = require("../dao/databaseConfig");
import typedjson = require("typedjson");
import crypto = require("crypto");
import LoggerModule = require("../service/logger");
import nodemailer = require("nodemailer");
var conf = require("../config/config.js");
var algorithm = "aes-256-ctr", password = "goallll";

/**
 * Router class responsible to register all store routes
 */
export class AuthRouter extends routerModule.Router {

  private _authService: authServiceModule.AuthService;

  /**
  * Constructor.
  *
  * @class StoreRouter
  * @constructor
  */
  constructor(app: express.Application, authService: authServiceModule.AuthService) {
    super(app);
    this._authService = authService;
  }

  public registerRoutes() {
    this.app.post("/oauth/token", function (req: express.Request, res: express.Response) {
      let idToken: string = req.body.data["id-token"];
      let authProvider: string = req.body.data["auth-provider"];
      let grantType: string = req.body.data["grant-type"];
      if (grantType === "password") {
        let tokenStr: string = uuid.v1().toString();
        let refreshTokenStr: string = uuid.v1().toString();
        let expiresOn: Date = new Date();
        expiresOn.setMinutes(expiresOn.getMinutes() + 20);
        let userPassword: string = req.body.data.password;
        let userEmail: string = req.body.data.email;

        this._authService.getUserByEmail(userEmail, (user: userModule.User, error: any) => {
          LoggerModule.Logger.getLogger().log("info", "User Info: ", user);
          let inputSaltedPassword = this.getSaltedPassword(userPassword, user.salt);
          if (user.active !== "Y") {
            return res.status(403).json({});
          }
          if (inputSaltedPassword === user.password) {
            let token: tokenModule.Token = new tokenModule.Token();
            token.setProperties(tokenStr, refreshTokenStr, expiresOn, user);
            this._authService.saveToken(token, (savedToken: tokenModule.Token, error: any) => {
              res.status(200).json(
                {
                  accessToken: tokenStr,
                  type: "bearer",
                  username: user["_name"],
                  expiresIn: expiresOn,
                  "refresh-token": refreshTokenStr,
                  "brandId": user["_brandId"]
                });
            });
          } else {
            res.status(401).json({});
          }
        });

      } else if (grantType === "authorization_code") {
        this._authService.validateToken(idToken, authProvider, function (resp: boolean) {
          if (!resp) {
            return res.status(401).json({});
          }
          let email: string = req.body.data.email;
          let name: string = req.body.data.name;
          let tokenStr: string = uuid.v1().toString();
          let refreshTokenStr: string = uuid.v1().toString();
          let expiresOn: Date = new Date();
          expiresOn.setMinutes(expiresOn.getMinutes() + 20);

          this._authService.getUserByEmail(email, function (user: any, error: any) {
            if (user != null) {
              let token: tokenModule.Token = new tokenModule.Token();
              token.setProperties(tokenStr, refreshTokenStr, expiresOn, user);
              this._authService.saveToken(token, function (savedToken: tokenModule.Token, error: any) {
                res.status(200).json(
                  {
                    accessToken: tokenStr,
                    type: "bearer",
                    expiresIn: expiresOn,
                    "refresh-token": refreshTokenStr,
                    "brandId": user._brandId
                  });
              });
            } else {
              let user: userModule.User = new userModule.User();
              user.setProperties(null, email, name, null, null, []);
              this._authService.saveUser(user, function (savedUser: userModule.User, error: any) {
                let token: tokenModule.Token = new tokenModule.Token();
                token.setProperties(tokenStr, refreshTokenStr, expiresOn, savedUser);
                this._authService.saveToken(token, function (savedToken: tokenModule.Token, error: any) {
                  res.status(200).json(
                    {
                      accessToken: tokenStr,
                      type: "bearer",
                      expiresIn: expiresOn,
                      "refresh-token": refreshTokenStr,
                      "brandId": -1 //brandId will be -1 for newly created user
                    });
                });
              }.bind(this));
            }
          }.bind(this));
        }.bind(this));
      } else if (grantType === "refresh_token") {
        let email: string = req.body.data.email;
        let refreshToken: string = req.body.data["refresh-token"];
        this._authService.refreshToken(refreshToken,
          function (isUpdated: boolean, updatedToken: tokenModule.Token, error: any) {
            if (isUpdated) {
              res.status(200).json(
                {
                  accessToken: updatedToken.id,
                  type: "bearer",
                  expiresIn: updatedToken.expiresOn,
                  "refresh-token": updatedToken.refreshToken
                });
            } else {
              res.status(404).json({ "message": "refresh token not found" });
            }
          });
      }
    }.bind(this));

    this.app.delete("/tokens/:id", function (req: express.Request, res: express.Response) {
      this._authService.deleteToken(req.params.id, function (isDeleted: boolean, error: any) {
        if (isDeleted) {
          res.status(200).json({});
        }
      });
    }.bind(this));

    this.app.post("/users", function (req: express.Request, res: express.Response) {
      let userPassword: string = req.body.data.password;
      let userEmail: string = req.body.data.email;
      let userName: string = req.body.data.name;

      let user: userModule.User = new userModule.User();
      let salt = this.getSalt();
      let saltedPassword = this.getSaltedPassword(userPassword, salt);
      user.setProperties(null, userEmail, userName, saltedPassword, salt, []);
      this._authService.saveUser(user, function (savedUser: userModule.User, error: any) {
        let transporter = nodemailer.createTransport({
          service: "Gmail",
          auth: {
            type: "OAuth2",
            clientId: "1063147513998-76o538kg4fcksp7fq7q2j8fomd2l266a.apps.googleusercontent.com",
            clientSecret: "7MyuYsg32SwTLP_OSKjB516L"
          }
        });

        transporter.sendMail({
          from: conf.get("smtp.from"),
          to: userEmail,
          subject: conf.get("smtp.subject"),
          text: conf.get("activation_link") + "?user=" + userEmail,
          auth: {
            user: conf.get("smtp.auth.user"),
            refreshToken: conf.get("smtp.auth.refreshToken")
          }
        });
        res.status(201).json({});
      }.bind(this));
    }.bind(this));


    this.app.post("/users/:email/activate", function (req: express.Request, res: express.Response) {
      let email: string = req.params.email;
      this._authService.getUserByEmail(email, (user: userModule.User, error: any) => {
        let updatedUser = { "user_id": user["_id"], "user_is_active": "Y" };
        this._authService.updateUser(updatedUser, (isUpdated: boolean, error: any) => {
          if (isUpdated) {
            res.status(200).json({});
          }
        });
      });
    }.bind(this));

    this.app.post("/users/:email/resetPassword", function (req: express.Request, res: express.Response) {
      let email: string = req.params.email;
      let userPassword: string = req.body.data.password;
      let decryptedEmail: string = null;
      try {
        decryptedEmail = this.decrypt(email);
      } catch (e) {
        return res.status(404).json({"errorMessage": "user invalid"});
      }
      this._authService.getUserByEmail(decryptedEmail, (user: userModule.User, error: any) => {
        if (user == null) {
          return res.status(404).json({"errorMessage": "user invalid"});
        }
        let salt = this.getSalt();
        let saltedPassword = this.getSaltedPassword(userPassword, salt);
        let updatedUser = { "user_id": user["_id"], "password": saltedPassword, "salt": salt };
        this._authService.updateUser(updatedUser, (isUpdated: boolean, error: any) => {
          if (isUpdated) {
            res.status(200).json({});
          }
        });
      });
    }.bind(this));

    this.app.post("/users/:email/forgetPassword", function (req: express.Request, res: express.Response) {
      let email: string = req.params.email;
      this._authService.getUserByEmail(email, (user: userModule.User, error: any) => {
        let hash = this.encrypt(email);
        let body: string = conf.get("reset_password_link").replace(/:user_email/i, hash);
        let sent: boolean = this._authService.sendEmail(email, body, "Forgot Password");
        if (sent) {
          res.status(200).json({});
        }
      });
    }.bind(this));

    this.app.get("/hello", function (req: express.Request, res: express.Response) {
      res.status(200).json({ message: "hello world !!" });
    });
  }

  getSalt(): string {
    return crypto.randomBytes(10).toString("hex");
  }

  getSaltedPassword(password: string, salt: string): string {
    let hash = crypto.createHmac("sha512", salt);
    return hash.update(password).digest("hex");
  }

  encrypt(text: string) {
    var cipher = crypto.createCipher(algorithm, password);
    var crypted = cipher.update(text, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
  }

  decrypt(text: string) {
    var decipher = crypto.createDecipher(algorithm, password);
    var dec = decipher.update(text, "hex", "utf8");
    dec += decipher.final("utf8");
    return dec;
  }

}
