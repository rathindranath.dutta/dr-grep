/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import authServiceModule = require("../service/authService");
import teamServiceModule = require("../service/teamService");
import userModule = require("../models/user");
import teamModule = require("../models/team");
import crypto = require("crypto");
/**
 * Router class responsible to register all store routes
 */
export class TeamRouter extends routerModule.Router {

    private _authService: authServiceModule.AuthService;
    private _teamService: teamServiceModule.TeamService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application,
        authService: authServiceModule.AuthService,
        teamService: teamServiceModule.TeamService) {
        super(app);
        this._authService = authService;
        this._teamService = teamService;
    }

    /**
     * Overridden
     */
    public registerRoutes() {

        this.app.post("/team/user", function (req: express.Request, res: express.Response) {
            let userEmail: string = req.body.data.email;
            let brandId: number = req.body.data.brandId;
            let userName: string = req.body.data.name;
            this._authService.getUserByEmail(userEmail, function (user: any, error: any) {
                if (user != null) {
                    let team: teamModule.Team = new teamModule.Team();
                    team.setProperties(null, brandId, user._id, null, null, 1);
                    this._teamService.addUserToTeam(team, function (teamMemberId: number, error: any) {
                        //TO DO: Send Email
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            res.status(200).json({
                                message: "User Added"
                            });
                        }
                    });
                } else {
                    let userPassword: string = "password"; //default password
                    let user: userModule.User = new userModule.User();
                    let salt = this.getSalt();
                    let saltedPassword = this.getSaltedPassword(userPassword, salt);
                    user.setProperties(null, userEmail, userName, saltedPassword, salt, []);
                    this._authService.saveUser(user, function (savedUser: userModule.User, error: any) {
                        if (error) {
                            res.status(500).json({ message: error });
                        } else {
                            let team: teamModule.Team = new teamModule.Team();
                            team.setProperties(null, brandId, savedUser.id, null, null, 1);
                            this._teamService.addUserToTeam(team, function (teamMemberId: number, error: any) {
                                //TO DO: Send Email
                                if (error) {
                                    res.status(500).json({ message: error });
                                } else {
                                    res.status(200).json({
                                        message: "User Added"
                                    });
                                }
                            });
                        }
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this));

        this.app.get("/team/users", function (req: express.Request, res: express.Response) {
            let brandId: number = parseInt(req.query.brandId);
            this._teamService.getTeamMembers(brandId, function (users: userModule.User[], error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(users);
                }
            });
        }.bind(this));

        this.app.post("/team/rules", function (req: express.Request, res: express.Response) {
            let ruleObject: any = req.body;
            this._teamService.insertRule(ruleObject, function (isCreated: boolean, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "successfull" });
                }
            });
        }.bind(this));

        this.app.get("/team/rules", function (req: express.Request, res: express.Response) {
            let brandId: number = parseInt(req.query.brandId);
            this._teamService.getRuleSets(brandId, function (ruleSets: any[], error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(ruleSets);
                }
            });
        }.bind(this));
    }

    getSalt(): string {
        return crypto.randomBytes(10).toString("hex");
    }

    getSaltedPassword(password: string, salt: string): string {
        let hash = crypto.createHmac("sha512", salt);
        return hash.update(password).digest("hex");
    }
}
