"use strict";
import * as express from "express";
import routerModule = require("./router");
import * as uuid from "uuid";
import readUserServiceModule = require("../service/readUserService");
import userModule = require("../models/user");
import ReadUserDaoModule = require("../dao/readUserDao");
import configModule = require("../dao/databaseConfig");
import typedjson = require("typedjson");

/**
 * Router class responsible to register all store routes
 */
 export class ReadUserRouter extends routerModule.Router {

   private _readUserService: readUserServiceModule.ReadUserService;

   /**
   * Constructor.
   *
   * @class ReadUserRouter
   * @constructor
   */
   constructor(app: express.Application, readUserService: readUserServiceModule.ReadUserService) {
     super(app);
     this._readUserService = readUserService;
   }

   public registerRoutes() {

    this.app.get("/username", function(req: express.Request, res: express.Response) {
      let email: string = req.body.data.email;
      this._readUserService.getUserByEmail(email, function(user: userModule.User, error: any)
        {
          if (user != null) {
            res.status(200).json(user.name);
            } else {
              res.status(404).json({ message: error});
              }
              });
      });

    this.app.get("/id", function(req: express.Request, res: express.Response) {
      let email: string = req.body.data.email;
      this._readUserService.getUserByEmail(email, function(user: userModule.User, error: any)
        {
          if (user != null) {
            res.status(200).json(user.id);
            } else {
              res.status(404).json({ message: error});
              }
              });
      });

    this.app.get("/roles", function(req: express.Request, res: express.Response) {
      let email: string = req.body.data.email;
      this._readUserService.getUserByEmail(email, function(user: userModule.User, error: any)
        {
          if (user != null) {
            res.status(200).json(user.roles);
            } else {
              res.status(404).json({ message: error});
              }
              });
      });



      }
    }
