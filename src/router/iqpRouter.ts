/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import iqpModule = require("../models/iqp");
import iqpServiceModule = require("../service/iqpService");
/**
 * Router class responsible to register all store routes
 */
export class IQPRouter extends routerModule.Router {

    private _iqpService: iqpServiceModule.IQPService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: iqpServiceModule.IQPService) {
        super(app);
        this._iqpService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.get("/iqp", function (req: express.Request, res: express.Response) {
            console.log("STEP1:iqp called" + req.query.brandId);
            this._iqpService.getIQPDetailsByBrand(req.query.brandId, function (iqp: iqpModule.IQP, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(iqp);
                }
            });
        }.bind(this));

        this.app.patch("/iqp/:id", function (req: express.Request, res: express.Response) {
            this._iqpService.updateIQPStatus(req.params.id, req.query.brandId, req.query.indicator, function (isSuccessfull: boolean, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "Successful" });
                }
            });
        }.bind(this));
    }

}
