"use strict";
import * as express from "express";
import routerModule = require("./router");
import * as uuid from "uuid";
import tokenModule = require("../models/token");
import bulkuploadServiceModule = require("../service/bulkuploadService");
import userModule = require("../models/user");
import authDaoModule = require("../dao/authDao");
import configModule = require("../dao/databaseConfig");
import typedjson = require("typedjson");
import multer = require("multer");
import parse = require("csv-parse");
import fs = require("fs");
var unzip = require("unzip");
import appRootPath = require("app-root-path");
import s3Uploader = require("s3-uploader");
import del = require("del");
import async = require("async");
import LoggerModule = require("../service/logger");


/**
 * Router class responsible to register all store routes
 */
 export class BulkuploadRouter extends routerModule.Router {

   private _bulkuploadService: bulkuploadServiceModule.BulkuploadService;
   /**
   * Constructor.
   *
   * @class StoreRouter
   * @constructor
   */
   constructor(app: express.Application, bulkuploadService: bulkuploadServiceModule.BulkuploadService) {
     super(app);
     this._bulkuploadService = bulkuploadService;
   }

   public registerRoutes() {
    var upload = multer({ dest: "uploads/" });

    this.app.post("/upload", upload.single("datafile") , (req: express.Request, res: express.Response) => {
      var filePath = appRootPath.path + "/uploads/" + req.file.filename;
      var parser = parse({delimiter: ","});
      var brands = new Array();
      var products = new Array();
      fs.createReadStream(filePath).pipe(parser).on("data", (data: any) => {
        brands.push({brandName: data[1], brandDesc: data[2], guid: data[3]});
        products.push({
          title: data[4], description: data[5],
          technicalSpec: data[6], price: data[7],
          sku: data[8], webpageLink: data[9],
          productLink: data[10], otherLink: data[11],
          category: data[12], brand: data[13],
          size: data[14], colour: data[15],
          weigth: data[16], ingredients: data[17], upc: Number(data[18]),
          prod_purchase_price: Number(data[19]), prod_discount: Number(data[20]),
          prod_unit_measure: data[21], prod_margin_perc: Number(data[22]),
          prod_HSN_cd: data[23]
        });
      }).on("end", (data: any) => {
        //we only upload the first brand
        this._bulkuploadService.uploadBrand([brands[1]], global.currentUserId, (addedBrands) => {
          this._bulkuploadService.uploadProducts(products, brands[1].guid, addedBrands[brands[1].guid],
            global.currentUserId, (successful: Boolean, error: any) => {
            LoggerModule.Logger.getLogger().log("info", "Upload brand is successful");
            res.status(200).json({});
          });
        });
        del([appRootPath.path + "/uploads/" + "*"]).then(paths => {
            LoggerModule.Logger.getLogger().log("info", "Deleted files and folders : ", paths);
        });
      }).on("headers", (headerList: any) => {
        LoggerModule.Logger.getLogger().log("info", "headers here");
      }).on("error", (err: any) => {
        LoggerModule.Logger.getLogger().log("error", err.message);
        res.status(400).json({error: err.message});
      });
    });

    this.app.post("/uploadImages", upload.single("images") ,
      (req: express.Request, res: express.Response) => {

        let options = {
          aws: {
            path: "bulk-upload/",
            region: "ap-south-1",
            acl: "public-read",
            accessKeyId: "AKIAIN4LVSTYH53ZGPJA",
            secretAccessKey: "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"
          },
          cleanup: {
            versions: true,
            original: true
          },
          versions: [{
            maxHeight: 1040,
            maxWidth: 1040,
            format: "jpg",
            suffix: "-large",
            quality: 80,
            awsImageExpires: 31536000,
            awsImageMaxAge: 31536000
          }]
        };
        let client = new s3Uploader("drgrep-staging", options);

        let filePath = appRootPath.path + "/uploads/" + req.file.filename;
        let outputPath = appRootPath.path + "/uploads/output";
        fs.createReadStream(filePath).pipe(unzip.Extract({ path: outputPath }))
        .on("close", () => {
          fs.readdir(outputPath, (err, items) => {
            let productImages = new Array();
            let prodUpcImages = new Array<Object>();
            async.series([(callback: any) => {
              async.eachSeries(items, (anItem, callback) => {
                client.upload(outputPath + "/" + anItem, {}, (err, versions, meta) => {
                  let prodUpcImageNoArray = anItem.split(".")[0].split("-");
                  prodUpcImageNoArray.pop();
                  prodUpcImages.push({productUpc: prodUpcImageNoArray.join("-"), url: versions[0].url});
                  callback();
                });
              }, (err: any) => {
                callback();
              });
            }, (callback: any) => {
              this._bulkuploadService.updateProductImages(prodUpcImages, global.currentUserId , (data: any, error: any) => {
                callback();
              });
            }], (err: any, results: any) => {
              del([appRootPath.path + "/uploads/" + "*"]).then(paths => {
                  LoggerModule.Logger.getLogger().log("info", "Deleted files and folders:", paths);
                  res.status(200).json({});
              });
            });
          });
        }).on("error", (error: any) => {
          res.status(400).json({error: error.message});
        });
    });
  }
}