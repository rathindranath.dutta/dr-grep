/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import workflowServiceModule = require("../service/workflowService");
/**
 * Router class responsible to register all store routes
 */
export class WorkflowRouter extends routerModule.Router {

    private _workflowService: workflowServiceModule.WorkflowService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application,
        workflowService: workflowServiceModule.WorkflowService) {
        super(app);
        this._workflowService = workflowService;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.post("/workflow/feeds", function (req: express.Request, res: express.Response) {
            let action: string = req.query.action;
            let workflowFeedData: any = req.body;
            this._workflowService.insertUpdateWorkflowFeedData(action, workflowFeedData, function (isCreated: boolean, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "successfull" });
                }
            });
        }.bind(this));

        this.app.get("/workflow/todolist", function (req: express.Request, res: express.Response) {
            let brandId: number = parseInt(req.query.brandId);
            let userId: number = global.currentUserId;
            this._workflowService.getToDoList(brandId, userId, function (todoListObj: boolean, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(todoListObj);
                }
            });
        }.bind(this));
    }
}
