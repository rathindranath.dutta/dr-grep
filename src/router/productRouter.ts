/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import prodModule = require("../models/product");
import variationModule = require("../models/variation");
import prodServiceModule = require("../service/productService");
import typedjson = require("typedjson");
import aws = require("aws-sdk");
import multer = require("multer");
import multerS3 = require("multer-s3");
import async = require("async");

/**
 * Router class responsible to register all product & variation routes
 */
export class ProductRouter extends routerModule.Router {

  private _productService: prodServiceModule.ProductService;
  private _upload: any;

  /**
  * Constructor.
  *
  * @class StoreRouter
  * @constructor
  */
  constructor(app: express.Application, service: prodServiceModule.ProductService) {
    super(app);
    this._productService = service;
  }

  /**
   * Overridden
   */
  public registerRoutes() {
    aws.config.update({
      signatureVersion: "v4",
      credentials: new aws.Credentials("AKIAIN4LVSTYH53ZGPJA", "RxtQ3qIYRPRwO3gs5SYwjVIYu89GhQEI3OaotIVk")
    });
    let s3 = new aws.S3();
    let upload = multer({
      storage: multerS3({
        s3: s3,
        bucket: "drgrep-staging",
        acl: "public-read",
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req: express.Request, file: any, cb: any) {
          cb(null, Date.now().toString() + ".jpg");
        }
      })
    });

    this.app.post("/products", upload.array("images", 3), function (req: express.Request, res: express.Response) {
      let reqProduct: any = req.body.product;
      Object.keys(reqProduct).forEach((key) => {
        let parsedVal: number = null;
        switch (key) {
          case "user_id":
          case "brand_id":
          case "upc":
          case "is_active":
          case "item_ledger_account_cogs_code":
          case "item_ledger_account_inventory_code":
          case "item_ledger_account_purchase_code":
          case "item_ledger_account_sales_code":
          case "purchase_unit_rate":
          case "prod_purchase_price":
          case "prod_discount":
          case "prod_msrp":
          case "prod_margin_perc":
            parsedVal = parseInt(reqProduct[key]);
            reqProduct[key] = parsedVal;
            break;
          default:
            break;
        }
      });

      reqProduct["user_id"] = global.currentUserId;
      let product: prodModule.Product = typedjson.TypedJSON.parse(JSON.stringify(reqProduct), prodModule.Product);
      product.addTime = new Date();
      product.updateTime = new Date();
      //let uploa1dedFiles = req.files as Array<File>;
      let imagesArray = new Array();
      for (let index in req.files) {
        if (req.files.hasOwnProperty(index)) {
          console.log(req.files[index]["location"]);
          imagesArray.push({ "prod_img_link": req.files[index]["location"] });
        }
      };
      product.images = imagesArray;
      this._productService.create(product, function (id: number, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.header("Location", "/products/{{id}}");
          res.status(201).json({ message: "Product created" });
        }
      });
    }.bind(this));

    this.app.get("/products", function (req: express.Request, res: express.Response) {
      if (req.query.userEmail !== null && req.query.userEmail !== undefined) {
        this._productService.getProductsByUserEmail(req.query.userEmail, function (products: Array<prodModule.Product>, error: any) {
          if (error) {
            res.status(500).json({ message: error });
          } else {
            res.status(200).json(products);
          }
        });
      } else if (req.query.brandId !== null && req.query.brandId !== undefined) {
        this._productService.getProductsByBrand(req.query.brandId, function (products: Array<prodModule.Product>, error: any) {
          if (error) {
            res.status(500).json({ message: error });
          } else {
            res.status(200).json(products);
          }
        });
      } else {
        this._productService.list(req.query.page, function (products: Array<prodModule.Product>, error: any) {
          if (error) {
            res.status(500).json({ message: error });
          } else {
            res.status(200).json(products);
          }
        });
      }
    }.bind(this));

    this.app.patch("/products/attach-to-brand", function (req: express.Request, res: express.Response) {
      console.log("STEP1: Attach brand called");
      this._productService.attachProductsToBrand(req.body.ParamArray, function (ids: Array<number>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(201).json({ message: "Successful" });
        }
      });
    }.bind(this));
    this.app.get("/products/:id", function (req: express.Request, res: express.Response) {
      this._productService.get(req.params.id, function (product: prodModule.Product, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(product);
        }
      });
    }.bind(this));

    this.app.put("/products/:id", function (req: express.Request, res: express.Response) {
      let reqProduct: any = req.body.product;
      Object.keys(reqProduct).forEach((key) => {
        let parsedVal: number = null;
        switch (key) {
          case "user_id":
          case "brand_id":
          case "upc":
          case "is_active":
          case "item_ledger_account_cogs_code":
          case "item_ledger_account_inventory_code":
          case "item_ledger_account_purchase_code":
          case "item_ledger_account_sales_code":
          case "purchase_unit_rate":
          case "prod_purchase_price":
          case "prod_discount":
          case "prod_msrp":
          case "prod_margin_perc":
            parsedVal = parseInt(reqProduct[key]);
            reqProduct[key] = parsedVal;
            break;
          default:
            break;
        }
      });
      reqProduct["product_id"] = parseInt(req.params.id);
      let product: prodModule.Product = typedjson.TypedJSON.parse(JSON.stringify(reqProduct), prodModule.Product);
      this._productService.update(product, function (data: any, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(product);
        }
      });
    }.bind(this));

    this.app.delete("/products/:id", function (req: express.Request, res: express.Response) {
      this._productService.delete(req.params.id, function (isDeleted: boolean, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(204).json({ message: "Deleted a store" });
        }
      });
    }.bind(this));

    this.app.get("/products/:id/variations", function (req: express.Request, res: express.Response) {
      this._productService.getVariations(req.params.id, function (variations: Array<variationModule.Variation>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(variations);
        }
      });
    }.bind(this));

    this.app.get("/products/:product_id/variations/:id", function (req: express.Request, res: express.Response) {
      this._productService.getVariation(req.params.product_id, req.params.id, function (variation: variationModule.Variation, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(variation);
        }
      });
    }.bind(this));

    this.app.post("/products/:id/variations", function (req: express.Request, res: express.Response) {
      this._productService.addVariations(req.params.id, req.body.variations, function (ids: Array<number>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(201).json({ message: "Variations created" });
        }
      });
    }.bind(this));

    this.app.delete("/products/:id/variations", function (req: express.Request, res: express.Response) {
      this._productService.removeVariations(req.params.id, req.body.variations, function (ids: Array<number>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(204).json({ message: "Variations deleted" });
        }
      });
    }.bind(this));

    this.app.put("/products/:id/variations", function (req: express.Request, res: express.Response) {
      this._productService.updateVariations(req.body.variations, function (ids: Array<number>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json({ message: "Variations updated" });
        }
      });
    }.bind(this));

  }

}

