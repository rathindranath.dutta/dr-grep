/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
//import iqpModule = require("../models/iqp");
import connectionServiceModule = require("../service/connectionService");
import productModule = require("../models/product");
import connectionModule = require("../models/connection");
import typedjson = require("typedjson");
/**
 * Router class responsible to register all store routes
 */
export class ConnectionRouter extends routerModule.Router {

    private _connectionService: connectionServiceModule.ConnectionService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: connectionServiceModule.ConnectionService) {
        super(app);
        this._connectionService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.get("/connections", function (req: express.Request, res: express.Response) {
            this._connectionService.getConnectedEntities(req.query.brandId, function (connections: any, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json(connections);
                }
            });
        }.bind(this));

        this.app.post("/connections", function (req: express.Request, res: express.Response) {
            let connection: connectionModule.Connection = typedjson.TypedJSON.parse(JSON.stringify(req.body.connection), connectionModule.Connection);
            this._connectionService.create(connection, function (connections: any, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(201).json({ message: "Connection created" });
                }
            });
        }.bind(this));

        this.app.put("/connections/:id", function (req: express.Request, res: express.Response) {
          let reqConnection: any = req.body.connection;
          reqConnection["ctu_connection_id"] = parseInt(req.params.id);
          let connection: connectionModule.Connection = typedjson.TypedJSON.parse(JSON.stringify(reqConnection), connectionModule.Connection);
          this._connectionService.update(connection, function (data: any, error: any) {
            if (error) {
              res.status(500).json({ message: error });
            } else {
              res.status(200).json({ message: "Connection updated" });
            }
          });
        }.bind(this));
    }

}
