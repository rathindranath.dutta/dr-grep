/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import ddServiceModule = require("../service/dynamicDiscountingService");
/**
 * Router class responsible to register all store routes
 */
export class DynamicDiscountingRouter extends routerModule.Router {

    private _ddService: ddServiceModule.DynamicDiscountingService;

    /**
    * Constructor.
    *
    * @class DynamicDiscountingRouter
    * @constructor
    */
    constructor(app: express.Application, service: ddServiceModule.DynamicDiscountingService) {
        super(app);
        this._ddService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.post("/discountManager/earlypay", function (req: express.Request, res: express.Response) {
            //console.log("STEP1:ARAP Module data" + JSON.stringify(req.body.data));
            this._ddService.earlyPayment(req.body.data, function (isSuccessfull: boolean, iqpId: number, error: any) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "Successful", iqpId: iqpId });
                }
            });
        }.bind(this));

        this.app.post("/discountManager/update", function (req: express.Request, res: express.Response) {
            //console.log("STEP1:ARAP Module data" + JSON.stringify(req.body.data));
            this._ddService.actionOnDiscount(req.body.data, function (error: any, isSuccessfull: boolean) {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(200).json({ message: "Successful" });
                }
            });
        }.bind(this));
    }

}
