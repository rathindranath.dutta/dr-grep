/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";

/**
 * Router class responsible to register all routes
 */
export class Router {
  protected app: express.Application;

  /**
   * Constructor.
   *
   * @class Router
   * @constructor
   */
  constructor(app: express.Application) {
    this.app = app;
    this.registerRoutes();
  }

  /**
   * Override this method in Child classes
   */
  public registerRoutes(): void {
    console.log("registering default routes");
  }

}
