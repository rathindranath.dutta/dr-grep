/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import storeModule = require("../models/store");
import storeServiceModule = require("../service/storeService");
/**
 * Router class responsible to register all store routes
 */
export class StoreRouter extends routerModule.Router {

  private _storeService: storeServiceModule.StoreService;

  /**
  * Constructor.
  *
  * @class StoreRouter
  * @constructor
  */
  constructor(app: express.Application, service: storeServiceModule.StoreService) {
    super(app);
    this._storeService = service;
  }

  /**
   * Overridden
   */
  public registerRoutes() {
    this.app.post("/stores", function (req: express.Request, res: express.Response) {
      this._storeService.create(req.body.store, function (id: number, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.header("Location", "/stores/{{id}}");
          res.status(201).json({ message: "Store created" });
        }
      });
    }.bind(this));

    this.app.get("/stores", function (req: express.Request, res: express.Response) {
      this._storeService.list(function (stores: Array<storeModule.Store>, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(stores);
        }
      });
    }.bind(this));

    this.app.get("/stores/:id", function (req: express.Request, res: express.Response) {
      this._storeService.get(req.params.id, function (store: storeModule.Store, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(store);
        }
      });
    }.bind(this));

    this.app.put("/stores/:id", function (req: express.Request, res: express.Response) {
      this._storeService.update(req.body.store, function (store: storeModule.Store, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else {
          res.status(200).json(store);
        }
      });
    }.bind(this));

    this.app.delete("/stores/:id", function (req: express.Request, res: express.Response) {
      this._storeService.delete(req.params.id, function (deleted: boolean, error: any) {
        if (error) {
          res.status(500).json({ message: error });
        } else if (deleted) {
          res.status(204).json({ message: "Deleted a store" });
        } else {
          res.status(500).json({ message: "Couldn't delete" });
        }
      });
    }.bind(this));
  }

}
