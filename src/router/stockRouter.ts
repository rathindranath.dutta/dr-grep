/// <reference path="../_all.d.ts" />
"use strict";
import * as express from "express";
import routerModule = require("./router");
import StockServiceModule = require("../service/stockService");
/**
 * Router class responsible to register all store routes
 */
export class StockRouter extends routerModule.Router {

    private _stockService: StockServiceModule.StockService;

    /**
    * Constructor.
    *
    * @class IQPRouter
    * @constructor
    */
    constructor(app: express.Application, service: StockServiceModule.StockService) {
        super(app);
        this._stockService = service;
    }

    /**
     * Overridden
     */
    public registerRoutes() {
        this.app.get("/stock", function (req: express.Request, res: express.Response) {
            //console.log("STEP1:ARAP Module data" + JSON.stringify(req.body.data));
            if (req.query.brandId !== null && req.query.brandId !== undefined) {
                this._stockService.getStockDetails(req.query.brandId, function (stockDetails: any, error: any) {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        res.status(200).json(stockDetails);
                    }
                });
            } else if (req.query.productId !== null && req.query.productId !== undefined) {
                this._stockService.getInOutStockByProduct(req.query.productId, function (stockInOutDetails: any, error: any) {
                    if (error) {
                        res.status(500).json({ message: error });
                    } else {
                        res.status(200).json(stockInOutDetails);
                    }
                });
            }
        }.bind(this));

        this.app.post("/stock", function (req: express.Request, res: express.Response) {
            this._stockService.addStock(req.body.stock, parseInt(req.query.isAddProductsToStock), (data: any, error: any) => {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(201).json({ message: "Stock added" });
                }
            });
        }.bind(this));

        this.app.put("/stock", function (req: express.Request, res: express.Response) {
            this._stockService.updateStock(req.body.stock, (data: any, error: any) => {
                if (error) {
                    res.status(500).json({ message: error });
                } else {
                    res.status(201).json({ message: "Stock modified" });
                }
            });
        }.bind(this));
    }

}
