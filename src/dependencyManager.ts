/// <reference path="_all.d.ts" />
"use strict";

import * as express from "express";
import dbConfigModule = require("./dao/databaseConfig");
import storeDaoModule = require("./dao/storeDao");
import mysqlStoreDaoModule = require("./dao/mysqlStoreDao");
import storeServiceModule = require("./service/storeService");
import routerModule = require("./router/router");
import storeRouterModule = require("./router/storeRouter");
import authDaoModule = require("./dao/mysqlAuthDao");
import authServiceModule = require("./service/authService");
import authRouteModule = require("./router/authRouter");
import bulkuploadServiceModule = require("./service/bulkuploadService");
import bulkuploadRouteModule = require("./router/bulkuploadRouter");

import productDaoModule = require("./dao/productDao");
import mysqlProductDaoModule = require("./dao/mysqlProductDao");
import productServiceModule = require("./service/productService");
import prodRouteModule = require("./router/productRouter");
import brandDaoModule = require("./dao/brandDao");
import mysqlBrandDaoModule = require("./dao/mysqlBrandDao");
import brandServiceModule = require("./service/brandService");
import brandRouteModule = require("./router/brandRouter");
import iqpRouteModule = require("./router/iqpRouter");
import iqpServiceModule = require("./service/iqpService");
import iqpDaoModule = require("./dao/iqpDao");
import mysqlIQPDaoModule = require("./dao/mysqlIqpDao");
import imageDaoModule = require("./dao/imageDao");
import mysqlImageDaoModule = require("./dao/mysqlImageDao");
import connectionServiceModule = require("./service/connectionService");
import connectionRouteModule = require("./router/connectionRouter");
import connectionDaoModule = require("./dao/connectionDao");
import mysqlConnectionDaoModule = require("./dao/mysqlConnectionDao");
import contactServiceModule = require("./service/contactService");
import contactRouteModule = require("./router/contactRouter");
import contactDaoModule = require("./dao/contactDao");
import mysqlContactDaoModule = require("./dao/mysqlContactDao");

import arapRouteModule = require("./router/arapRouter");
import arapServiceModule = require("./service/arapService");
import arapDaoModule = require("./dao/arapDao");
import mysqlArApDaoModule = require("./dao/mysqlArApDao");

import stockRouteModule = require("./router/stockRouter");
import stockServiceModule = require("./service/stockService");
import stockDaoModule = require("./dao/stockDao");
import mysqlStockDaoModule = require("./dao/mysqlStockDao");
import reportServiceModule = require("./service/reportService");
import reportRouteModule = require("./router/reportRouter");
import mysqlReportDaoModule = require("./dao/mysqlReportDao");

import dynamicDiscountingRouterModule = require("./router/dynamicDiscountingRouter");
import dynamicDiscountingServiceModule = require("./service/dynamicDiscountingService");
import dynamicDiscountigDaoModule = require("./dao/dynamicDiscountigDao");
import mysqlDynamicDiscountingDaoModule = require("./dao/mysqlDynamicDiscountingDao");

import teamRouterModule = require("./router/teamRouter");
import teamServiceModule = require("./service/teamService");
import teamDaoModule = require("./dao/teamDao");
import mysqlTeamDaoModule = require("./dao/mysqlTeamDao");

import workflowRouterModule = require("./router/workflowRouter");
import workflowServiceModule = require("./service/workflowService");
import workflowDaoModule = require("./dao/workflowDao");
import mysqlworkflowDaoModule = require("./dao/mysqlWorkflowDao");
/**
 * Service class
 */
export class DependencyManager {

  private _routers: Array<routerModule.Router>;
  private _app: express.Application;
  private _dbConfig: dbConfigModule.DatabaseConfig;
  /**
   * Constructor.
   *
   * @class DependencyManager
   * @constructor
   */
  constructor(app: express.Application) {
    this._app = app;
    this._dbConfig = {}; // TODO: Instantiate config
    this._routers = [];
    this.manageStoreDependencies();
    this.manageAuthDependencies();
    this.manageBulkuploadDependencies();
    this.manageProductDependencies();
    this.manageNBrandDependencies();
    this.manageIQPDependencies();
    this.manageConnectionDependencies();
    this.manageARAPDependencies();
    this.manageStockDependencies();
    this.manageContactDependencies();
    this.manageReportDependencies();
    this.manageDynamicDiscountingDependencies();
    this.manageTeamDependencies();
    this.manageWorkflowDependencies();
  }

  private getBulkuploadService(): bulkuploadServiceModule.BulkuploadService {
    return new bulkuploadServiceModule.BulkuploadService(this.getProductService(),
      this.getBrandService(), this.getStoreService());
  }

  private getStoreService(): storeServiceModule.StoreService {
    let storeDao: storeDaoModule.StoreDao = new mysqlStoreDaoModule.MySqlStoreDao();
    return new storeServiceModule.StoreService(storeDao);
  }

  private getProductService(): productServiceModule.ProductService {
    let productDao: productDaoModule.ProductDao = new mysqlProductDaoModule.MySqlProductDao();
    let imageDao: imageDaoModule.ImageDao = new mysqlImageDaoModule.MySqlImageDao();
    return new productServiceModule.ProductService(productDao, imageDao);
  }

  private getAuthService(): authServiceModule.AuthService {
    let authDao: authDaoModule.MySqlAuthDao = new authDaoModule.MySqlAuthDao();
    return new authServiceModule.AuthService(authDao);
  }

  private getBrandService(): brandServiceModule.BrandService {
    let brandDao: brandDaoModule.BrandDao = new mysqlBrandDaoModule.MySqlBrandDao();
    return new brandServiceModule.BrandService(brandDao);
  }

  private getIQPService(): iqpServiceModule.IQPService {
    let iqpDao: mysqlIQPDaoModule.MySqlIqpDao = new mysqlIQPDaoModule.MySqlIqpDao();
    return new iqpServiceModule.IQPService(iqpDao);
  }
  private getConnectionService(): connectionServiceModule.ConnectionService {
    let connectionDao: mysqlConnectionDaoModule.MySqlConnectionDao = new mysqlConnectionDaoModule.MySqlConnectionDao();
    return new connectionServiceModule.ConnectionService(connectionDao);
  }

  private getContactService(): contactServiceModule.ContactService {
    let contactDao: mysqlContactDaoModule.MySqlContactDao = new mysqlContactDaoModule.MySqlContactDao();
    return new contactServiceModule.ContactService(contactDao);
  }

  private getARAPService(): arapServiceModule.ARAPService {
    let workflowDao: mysqlworkflowDaoModule.MySqlWorkflowDao = new mysqlworkflowDaoModule.MySqlWorkflowDao();
    let arapDao: mysqlArApDaoModule.MySqlArAPDao = new mysqlArApDaoModule.MySqlArAPDao(workflowDao);
    return new arapServiceModule.ARAPService(arapDao);
  }
  private getStockService(): stockServiceModule.StockService {
    let stockDao: mysqlStockDaoModule.MySqlStockDao = new mysqlStockDaoModule.MySqlStockDao();
    return new stockServiceModule.StockService(stockDao);
  }

  private getReportService(): reportServiceModule.ReportService {
    let reportDao = new mysqlReportDaoModule.MySqlReportDao();
    return new reportServiceModule.ReportService(reportDao);
  }
  private getDynamicDiscountingService(): dynamicDiscountingServiceModule.DynamicDiscountingService {
    let ddDao: mysqlDynamicDiscountingDaoModule.MySqlDynamicDiscountingDao = new mysqlDynamicDiscountingDaoModule.MySqlDynamicDiscountingDao();
    return new dynamicDiscountingServiceModule.DynamicDiscountingService(ddDao);
  }
  private getTeamService(): teamServiceModule.TeamService {
    let teamDao: mysqlTeamDaoModule.MySqlTeamDao = new mysqlTeamDaoModule.MySqlTeamDao();
    return new teamServiceModule.TeamService(teamDao);
  }

  private getWorkflowService(): workflowServiceModule.WorkflowService {
    let workflowDao: mysqlworkflowDaoModule.MySqlWorkflowDao = new mysqlworkflowDaoModule.MySqlWorkflowDao();
    return new workflowServiceModule.WorkflowService(workflowDao);
  }
  private manageStoreDependencies() {
    let storeRouter: routerModule.Router = new storeRouterModule.StoreRouter(this._app, this.getStoreService());
    this._routers.push(storeRouter);
  }

  private manageAuthDependencies() {
    let authRouter: authRouteModule.AuthRouter = new authRouteModule.AuthRouter(this._app, this.getAuthService());
    this._routers.push(authRouter);
  }

  private manageBulkuploadDependencies() {
    let bulkuploadRouter: bulkuploadRouteModule.BulkuploadRouter =
      new bulkuploadRouteModule.BulkuploadRouter(this._app, this.getBulkuploadService());
    this._routers.push(bulkuploadRouter);
  }

  private manageProductDependencies() {
    let productRouter: routerModule.Router = new prodRouteModule.ProductRouter(this._app, this.getProductService());
    this._routers.push(productRouter);
  }

  private manageNBrandDependencies() {
    let brandRouter: routerModule.Router = new brandRouteModule.BrandRouter(this._app, this.getBrandService());
    this._routers.push(brandRouter);
  }

  private manageIQPDependencies() {
    let iqpRouter: routerModule.Router = new iqpRouteModule.IQPRouter(this._app, this.getIQPService());
    this._routers.push(iqpRouter);
  }
  private manageConnectionDependencies() {
    let connectionRouter: routerModule.Router = new connectionRouteModule.ConnectionRouter(this._app, this.getConnectionService());
    this._routers.push(connectionRouter);
  }

  private manageContactDependencies() {
    let contactRouter: routerModule.Router = new contactRouteModule.ContactRouter(this._app, this.getContactService());
    this._routers.push(contactRouter);
  }

  private manageARAPDependencies() {
    let arapRouter: routerModule.Router = new arapRouteModule.ARAPRouter(this._app, this.getARAPService());
    this._routers.push(arapRouter);
  }

  private manageStockDependencies() {
    let stockRouter: routerModule.Router = new stockRouteModule.StockRouter(this._app, this.getStockService());
    this._routers.push(stockRouter);
  }
  manageReportDependencies() {
    let reportRouter = new reportRouteModule.ReportRouter(this._app, this.getReportService());
    this._routers.push(reportRouter);
  }

  manageDynamicDiscountingDependencies() {
    let ddRouter = new dynamicDiscountingRouterModule.DynamicDiscountingRouter(this._app, this.getDynamicDiscountingService());
    this._routers.push(ddRouter);
  }

  manageTeamDependencies() {
    let teamRouter = new teamRouterModule.TeamRouter(this._app, this.getAuthService(), this.getTeamService());
    this._routers.push(teamRouter);
  }

  manageWorkflowDependencies() {
    let workflowRouter = new workflowRouterModule.WorkflowRouter(this._app, this.getWorkflowService());
    this._routers.push(workflowRouter);
  }
  public get routers(): Array<routerModule.Router> {
    return this._routers;
  }

}
