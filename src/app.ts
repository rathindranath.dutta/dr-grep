/// <reference path="_all.d.ts" />

"use strict";

import * as bodyParser from "body-parser";
import * as express from "express";
import * as path from "path";
import dependencyManagerModule = require("./dependencyManager");
import routerModule = require("./router/router");
import serviceModule = require("./service/service");
import authServiceModule = require("./service/authService");
import authDaoModule = require("./dao/mysqlAuthDao");
import tokenModule = require("./models/token");
import userModule = require("./models/user");
import LoggerModule = require("./service/logger");

/**
 * The server.
 *
 * @class Server
 */
class Server {

  private app: express.Application;
  private router: routerModule.Router;
  private authService: authServiceModule.AuthService;
  private unauthenticatedRoutes: RegExp[] = [/\/oauth\/token/, /\/users/, /\/forgetPassword/,
  /\/users\/[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)\/activate/, /\/hello/];

  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
   */
  public static bootstrap(): Server {
    return new Server();
  }

  /**
   * Constructor.
   *
   * @class Server
   * @constructor
   */
  constructor() {
    // Instantiate required dependencies
    this.app = express();
    //configure application
    this.config();
    // Inject dependencies and register routes
    let dependencyManager: dependencyManagerModule.DependencyManager = new dependencyManagerModule.DependencyManager(this.app);
    dependencyManager.routers.forEach(function (router: routerModule.Router) {
      router.registerRoutes();
    });

    let authDao: authDaoModule.MySqlAuthDao = new authDaoModule.MySqlAuthDao();
    this.authService = new authServiceModule.AuthService(authDao);
  }

  /**
   * Configure the application
   */
  private config() {
    // request body and form parser
    this.app.use(bodyParser.json());

    // query string parser
    this.app.use(bodyParser.urlencoded({ extended: true }));

    // add static paths (host angular here)
    this.app.use(express.static(path.join(__dirname, "public")));
    // Serve swagger documentation
    this.app.use("/api", express.static(path.join(__dirname, "api")));

    // Handle CORS
    this.app.use(function (req: express.Request, res: express.Response, next: express.NextFunction) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
      res.header("Access-Control-Allow-Headers", "Content-Type,Authorization");
      next();
    });

    // TODO: Middleware to check bearer token in Authorization header
    this.app.use(function (req: express.Request, res: express.Response, next: express.NextFunction) {
      //check the accept header
      let accept: string = req.header("Accept");
      if (accept != null && accept.indexOf("*/*") < 0 && accept !== "application/json") {
        res.json(406, { "code": 406, "message": "Not Acceptable" });
      } else {
        if (this.shouldAllowRoute(req.path) || req.method === "OPTIONS") {
          next();
        } else {
          //Check authorization header
          let bearerToken: string = req.header("Authorization");
          if (bearerToken != null && bearerToken.indexOf("Bearer ") === 0) {
            let token: string = bearerToken.substring(7);
            let tokenObj: tokenModule.Token = this.authService.getTokenInfo(token,
              (tokenObj: tokenModule.Token, error: any) => {
                /*this.authService.getUserById(tokenObj.userId, (currentUser: userModule.User,
                  error: any) => {
                  global.currentUser = currentUser;
                });*/
                if (tokenObj == null) {
                  res.status(401).json({ message: "Unauthorized" });
                } else {
                  global.currentUserId = tokenObj.userId;
                  next();
                }
              });
          } else {
            res.status(401).json({ message: "Unauthorized" });
          }
        }
      }
    }.bind(this));

    // Middleware to add logs
    this.app.use(function (req: express.Request, res: express.Response, next: express.NextFunction) {
      if (req.path !== "/hello") {
        LoggerModule.Logger.getLogger().log("info", "Going to invoke: ", req.path);
      }
      next();
    });
  }

  private shouldAllowRoute(route: string) {
    var allowed = false;
    this.unauthenticatedRoutes.forEach((exp) => {
      if (exp.test(route)) {
        allowed = true;
      }
    });
    return allowed;
  }

  /**
   * Get the express app
   */
  public getApp(): express.Application {
    return this.app;
  }
}
export = Server.bootstrap().getApp();
