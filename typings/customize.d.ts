declare module NodeJS  {
    interface Global {
        currentUser: any,
        currentUserId: any
    }
}

interface Array<T> {
   includes(o: T): boolean;
}

interface Object {
	keys: any;
}