FROM centos:centos6

ADD *.sh /build/

RUN chmod +x /build/install.sh /build/startup.sh /build/configure.sh && /build/install.sh && useradd deploy

WORKDIR /home/deploy

RUN /build/configure.sh && chown -R deploy:deploy /home/deploy/dr-grep-nodejs

EXPOSE 8080

ENTRYPOINT /build/startup.sh