# dr-grep-web

## Prerequisites

1. Install nvm to manage multiple node version
2. nvm install v6.3.0
3. Install any of your favourite editor (Sublime Text 3 with typescript plugin)

## To install typescript package

1. Install the package control (https://packagecontrol.io/installation)
2. In Sublime editor open the Command Pallete (ctrl+shift+P or cmd+shift+P).
3. Type “Install Package” and hit return.
4. Type “TypeScript” and hit return.

## Build

1. npm install

## Run the server

npm start

## Run the application

Goto browser and hit http://localhost:3000


