import { Directive, ElementRef, Input, Renderer } from '@angular/core';

@Directive({ selector: '[login-style]' })
export class LoginDirective {
    constructor(el: ElementRef, renderer: Renderer) {
    	if (localStorage.getItem("signedIn") == "true") {
    		renderer.setElementStyle(el.nativeElement, 'display', 'none');
    	}
    }
}