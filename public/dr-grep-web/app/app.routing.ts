import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { HomeComponent }      from './home.component';
import { AuthDetailComponent }      from './auth.component';
import { AppComponent }      from './app.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { 
  	path: 'app', 
  	component: AppComponent
  },
  { 
  	path: 'auth', 
  	component: AuthDetailComponent
  },	
  {
    path: 'home',
    component: HomeComponent
  }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);