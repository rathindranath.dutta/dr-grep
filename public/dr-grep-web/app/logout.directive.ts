import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({ selector: '[logout-style]' })
export class LogoutDirective {
    constructor(el: ElementRef, renderer: Renderer) {
    	if (localStorage.getItem("signedIn") == "false") {
    		renderer.setElementStyle(el.nativeElement, 'display', 'none');
    	}
    }
}