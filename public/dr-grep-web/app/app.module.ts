import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { AuthDetailComponent } from './auth.component';
import { HomeComponent } from './home.component';
import { AuthService }  from './auth.service';
import { Routing } from './app.routing';
import { LoginDirective } from './login.directive';
import { LogoutDirective } from './logout.directive';
//import { LocalStorageService } from "angular2-localstorage/LocalStorageEmitter";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    Routing
  ],
  declarations: [
    AppComponent,
    AuthDetailComponent,
    HomeComponent,
    LoginDirective,
    LogoutDirective
  ],
  providers: [ AuthService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }