import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

declare var gapi:any;

@Component({
  selector: 'auth-detail',
  templateUrl: 'auth.html'
})

export class AuthDetailComponent implements OnInit {
  googleLoginButtonId = "google-login-button";
  userDisplayName = "empty";

  constructor(private authService: AuthService, private zone: NgZone, private router: Router) {
  }

  ngOnInit() {
    var jsElm = document.createElement("script");
    jsElm.type = "application/javascript";
    jsElm.src = "https://apis.google.com/js/platform.js";
    jsElm.onload = function(){
      gapi.signin2.render(
      this.googleLoginButtonId,
      {
        "onSuccess": this.onSignIn.bind(this),
        "scope": "profile",
        "theme": "dark"
      });
    }.bind(this);
    document.body.appendChild(jsElm);
    this.authService.validateToken();
  }

  onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var id_token = googleUser.getAuthResponse().id_token;
    localStorage.setItem('name', profile.getName());
    localStorage.setItem('signedIn', "true");
    this.zone.run(() => {
      this.userDisplayName = profile.getName();
    });
    console.log('get Name: ' + localStorage.getItem('name'));
    console.log('id_token: ' + id_token);
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail());
    let link = ['/home'];
    this.router.navigate(link);
  }

  resetStoredVariables() {
    localStorage.setItem('signedIn', "false");
    localStorage.setItem('name', "");
  }

  signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      this.resetStoredVariables();
      this.zone.run(() => {
        this.userDisplayName = "empty";
      });
    }.bind(this));
  }
}