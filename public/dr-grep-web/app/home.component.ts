import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'my-home',
  template: `
     <h3>{{title}}</h3>
     <auth-detail></auth-detail>
     <div class="main-application">
  		<p>Hello, {{userDisplayName}}!</p>
	</div>`
})
export class HomeComponent implements OnInit {
  title = 'My Home';
  userDisplayName = "";

  constructor(private zone: NgZone, private router: Router) {
  }

  ngOnInit() {
  	this.userDisplayName =  localStorage.getItem('name');
  	if (localStorage.getItem("signedIn") != "true") {
  		let link = ['/auth'];
  		this.router.navigate(link);
  	}
  }
}