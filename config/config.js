var convict = require("convict");

// Define a schema
var conf = convict({
  env: {
    doc: "The applicaton environment.",
    format: ["production", "development", "test"],
    default: "development",
    env: "NODE_ENV"
  },
  ip: {
    doc: "The IP address to bind.",
    format: "ipaddress",
    default: "127.0.0.1",
    env: "IP_ADDRESS",
  },
  port: {
    doc: "The port to bind.",
    format: "port",
    default: 8080,
    env: "PORT"
  },
  database: {
    host: {
      doc: "Database host name/IP",
      format: String,
      default: 'localhost'
    },
    user: {
      doc: "Database user",
      format: String,
      default: 'root'
    },
    password: {
      doc: "Database user password",
      format: String,
      default: 'root'
    },
    dbname: {
      doc: "Database name",
      format: String,
      default: 'pin_drgrep_db'
    },
    connectionLimit: {
      doc: "Database connection limit",
      format: 'int',
      default: 5
    },
    connectionTimeout: {
      doc: "Database connection timeout",
      format: 'int',
      default: 10000
    }
  },
  activation_link: {
    doc: "Activation link for user registration",
    format: String,
    default: 'http://localhost:3000/activate'
  },
  reset_password_link: {
    doc: "reset link for password",
    format: String,
    default: 'http://localhost:8080/users/:user_email/resetPassword'
  },
  smtp: {
    from: {
      doc: "From user email address",
      format: String,
      default: "drgrep123@gmail.com"
    },
    subject: {
      doc: "Email Subject",
      format: String,
      default: "Welcome to dr-grep"
    },
    auth: {
      user: {
        doc: "auth user",
        format: String,
        default: "drgrep123@gmail.com"
      },
      refreshToken: {
        doc: "refresh token of user",
        format: String,
        default: "1/XhofYiWhsjmAJvmk7CTTtQQHo6UtH1c65-88_-v7dWg"
      }
    }
  },
  logging: {
    level: {
      doc: "set the logging level",
      format: String,
      default: "debug"
    }
  }
});

// Load environment dependent configuration
var env = conf.get('env');
conf.loadFile('./config/' + env + '.json');

// Perform validation
conf.validate({strict: true});

module.exports = conf;